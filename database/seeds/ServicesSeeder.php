<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use WezomCms\Services\Models\ServiceGroup;
use WezomCms\Services\Models\ServiceGroupTranslation;

class ServicesSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('services')->truncate();
        \DB::table('service_translations')->truncate();
        \DB::table('service_groups')->truncate();
        \DB::table('service_group_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = $this->getData();
        $groups = $this->getCategories();

        try {
            \DB::transaction(function () use ($data, $groups) {

                // категории
                foreach ($groups as $cat){
                    $category = new ServiceGroup();
                    $category->sort = $cat['sort'];
                    $category->icon = $this->randomIcon();
                    $category->is_top = $cat['is_top'];

                    $category->save();
                    foreach ($cat['translates'] as $lang => $t){
                        $translations = new ServiceGroupTranslation();
                        $translations->locale = $lang;
                        $translations->name = $t['name'];
                        $translations->slug = $t['slug'];
                        $translations->text = $t['text'];

                        $translations->service_group_id = $category->id;
                        $translations->save();
                    }

                    foreach ($cat['children'] as $c){
                        $child = new ServiceGroup();
                        $child->sort = $c['sort'];
                        $child->icon = $this->randomIcon();
                        $child->is_top = $c['is_top'];
                        $child->parent_id = $category->id;
                        $child->save();

                        foreach ($c['translates'] as $l => $ct){
                            $translations_child = new ServiceGroupTranslation();
                            $translations_child->locale = $l;
                            $translations_child->name = $ct['name'];
                            $translations_child->slug = $ct['slug'];
                            $translations_child->text = $ct['text'];

                            $translations_child->service_group_id = $child->id;
                            $translations_child->save();
                        }
                    }
                }

                foreach ($data as $item){
                    $model = new \WezomCms\Services\Models\Service();
                    $model->sort = $item['sort'];
                    $model->icon = $this->randomIcon();
                    $model->service_group_id = $item['service_group_id'];

                    $model->save();
                    foreach ($item['translates'] as $lang => $trans){
                        $translations = new \WezomCms\Services\Models\ServiceTranslation();
                        $translations->locale = $lang;
                        $translations->name = $trans['name'];
                        $translations->slug = $trans['slug'];
                        $translations->text = $trans['text'];
                        $translations->service_id = $model->id;
                        $translations->save();
                    }

                    // risks
                    $someRisks = \WezomCms\Services\Models\Risk::orderBy(DB::raw('RAND()'))->take(random_int(2, 3))->pluck('id')->toArray();
                    $model->risks()->sync($someRisks);
                }
            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }

    protected function getCategories()
    {
        return [
            [
                'sort' => 0,
                'is_top' => false,
                'translates' => [
                    'ru' => [
                        'name' => 'Частным лицам',
                        'slug' => 'chastnym-litsam',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                    ],
                    'uk' => [
                        'name' => 'Приватним особам',
                        'slug' => 'privatnim-osobam',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                    ]
                ],
                'children' => [
                    [
                        'sort' => 0,
                        'is_top' => true,
                        'translates' => [
                            'ru' => [
                                'name' => 'Автомобиль',
                                'slug' => 'avto',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                            'uk' => [
                                'name' => 'Автомобіль',
                                'slug' => 'avto',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                        ]
                    ],
                    [
                        'sort' => 1,
                        'is_top' => true,
                        'translates' => [
                            'ru' => [
                                'name' => 'Имущество',
                                'slug' => 'imuschestvo',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                            'uk' => [
                                'name' => 'Майно',
                                'slug' => 'imuschestvo',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                        ]
                    ],
                    [
                        'sort' => 2,
                        'is_top' => true,
                        'translates' => [
                            'ru' => [
                                'name' => 'Здоровье',
                                'slug' => 'zdorovya',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                            'uk' => [
                                'name' => 'Здоров\'я',
                                'slug' => 'zdorovya',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                        ]
                    ],
                    [
                        'sort' => 3,
                        'is_top' => true,
                        'translates' => [
                            'ru' => [
                                'name' => 'Ответственность',
                                'slug' => 'vidpovidalnist',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                            'uk' => [
                                'name' => 'Відповідальність',
                                'slug' => 'vidpovidalnist',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                        ]
                    ],
                    [
                        'sort' => 4,
                        'is_top' => true,
                        'translates' => [
                            'ru' => [
                                'name' => 'Туризм',
                                'slug' => 'turizm',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                            'uk' => [
                                'name' => 'Туризм',
                                'slug' => 'turizm',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                        ]
                    ],
                    [
                        'sort' => 5,
                        'is_top' => false,
                        'translates' => [
                            'ru' => [
                                'name' => 'Яхты,лодки,катера',
                                'slug' => 'yahti-chovni-kateri',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                            'uk' => [
                                'name' => 'Яхти,човни,катери',
                                'slug' => 'yahti-chovni-kateri',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                        ]
                    ],
                    [
                        'sort' => 6,
                        'is_top' => false,
                        'translates' => [
                            'ru' => [
                                'name' => 'Малая авиация',
                                'slug' => 'mala-aviatsiya',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                            'uk' => [
                                'name' => 'Мала авіація',
                                'slug' => 'mala-aviatsiya',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                        ]
                    ],
                ]
            ],
            [
                'sort' => 1,
                'is_top' => false,
                'translates' => [
                    'ru' => [
                        'name' => 'Корпоративным клиентам',
                        'slug' => 'korporativnym-klientam',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                    ],
                    'uk' => [
                        'name' => 'Корпоративним кліентам',
                        'slug' => 'korporativnim-klientam',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                    ],
                ],
                'children' => [
                    [
                        'sort' => 0,
                        'is_top' => false,
                        'translates' => [
                            'ru' => [
                                'name' => 'Имущество',
                                'slug' => 'imuschestvo',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                            'uk' => [
                                'name' => 'Майно',
                                'slug' => 'imuschestvo',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                        ]
                    ],
                    [
                        'sort' => 1,
                        'is_top' => false,
                        'translates' => [
                            'ru' => [
                                'name' => 'Транспорт',
                                'slug' => 'transport',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                            'uk' => [
                                'name' => 'Транспорт',
                                'slug' => 'transport',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                        ]
                    ],
                    [
                        'sort' => 2,
                        'is_top' => false,
                        'translates' => [
                            'ru' => [
                                'name' => 'Сотрудники',
                                'slug' => 'sotrudniki',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                            'uk' => [
                                'name' => 'Співробітники',
                                'slug' => 'sotrudniki',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                        ]
                    ],
                    [
                        'sort' => 3,
                        'is_top' => false,
                        'translates' => [
                            'ru' => [
                                'name' => 'Ответственность',
                                'slug' => 'vidpovidalnist',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                            'uk' => [
                                'name' => 'Відповідальність',
                                'slug' => 'vidpovidalnist',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                        ]
                    ],
                    [
                        'sort' => 4,
                        'is_top' => false,
                        'translates' => [
                            'ru' => [
                                'name' => 'Грузы',
                                'slug' => 'gruzy',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                            'uk' => [
                                'name' => 'Вантажі',
                                'slug' => 'gruzy',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                        ]
                    ],
                    [
                        'sort' => 5,
                        'is_top' => false,
                        'translates' => [
                            'ru' => [
                                'name' => 'Агрострахование',
                                'slug' => 'agrostrahuvannya',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                            'uk' => [
                                'name' => 'Агрострахування',
                                'slug' => 'agrostrahuvannya',
                                'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(80, 100)),
                            ],
                        ]
                    ],
                ]
            ]
        ];
    }

    protected function getData()
    {

        return [
            [
                'sort' => 0,
                'service_group_id' => 2,
                'translates' => [
                    'ru' => [
                        'name' => 'Каско',
                        'slug' => 'kasko',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                    'uk' => [
                        'name' => 'Каско',
                        'slug' => 'kasko',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                ]
            ],
            [
                'sort' => 1,
                'service_group_id' => 2,
                'translates' => [
                    'ru' => [
                        'name' => 'Страхование жизни',
                        'slug' => 'strahovanie-zhizni',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                    'uk' => [
                        'name' => 'Страхування життя',
                        'slug' => 'strahuvannja-zhittja',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                ]
            ],
            [
                'sort' => 2,
                'service_group_id' => 3,
                'translates' => [
                    'ru' => [
                        'name' => 'Страхование имущества',
                        'slug' => 'strahovanie-imushhestva',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                    'uk' => [
                        'name' => 'Страхування майна',
                        'slug' => 'strahuvannja-majna',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                ]
            ],
            [
                'sort' => 3,
                'service_group_id' => 3,
                'translates' => [
                    'ru' => [
                        'name' => 'Туристическая страховка',
                        'slug' => 'turisticheskaja-strahovka',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                    'uk' => [
                        'name' => 'Туристична страховка',
                        'slug' => 'turistichna-strahovka',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                ]
            ],
            [
                'sort' => 4,
                'service_group_id' => 2,
                'translates' => [
                    'ru' => [
                        'name' => 'Страховка авто',
                        'slug' => 'strahovka-avto',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                    'uk' => [
                        'name' => 'Страховка авто',
                        'slug' => 'strahovka-avto',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                ]
            ],
            [
                'sort' => 5,
                'service_group_id' => 2,
                'translates' => [
                    'ru' => [
                        'name' => 'Страхование груза',
                        'slug' => 'strahovanie-gruza',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                    'uk' => [
                        'name' => 'Страхування вантажу',
                        'slug' => 'strahuvannja-vantazhu',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                ]
            ],
            [
                'sort' => 6,
                'service_group_id' => 3,
                'translates' => [
                    'ru' => [
                        'name' => 'Медицинская страховка',
                        'slug' => 'medicinskaja-strahovka',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                    'uk' => [
                        'name' => 'Медичне страхування',
                        'slug' => 'medichne-strahuvannja',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                ]
            ],
            [
                'sort' => 7,
                'service_group_id' => 4,
                'translates' => [
                    'ru' => [
                        'name' => 'Страховка от стихийных бедствий',
                        'slug' => 'strahovka-ot-stihijnyh-bedstvij',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                    'uk' => [
                        'name' => 'Страховка від стихійних лих',
                        'slug' => 'strahovka-vid-stihijnih-lih',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                ]
            ],
            [
                'sort' => 8,
                'service_group_id' => 4,
                'translates' => [
                    'ru' => [
                        'name' => 'Страховка смерти',
                        'slug' => 'strahovka-smerti',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                    'uk' => [
                        'name' => 'Страховка смерті',
                        'slug' => 'strahovka-smerti',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)),
                    ],
                ]
            ],
        ];
    }
}




