<?php

use WezomCms\Partners\Models\Category;
use WezomCms\Partners\Models\CategoryTranslation;
use WezomCms\Partners\Models\Partner;
use WezomCms\Partners\Models\PartnerTranslation;

class PartnerSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('partner_categories')->truncate();
        \DB::table('partner_category_translations')->truncate();
        \DB::table('partner_translations')->truncate();
        \DB::table('partners')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            \DB::transaction(function (){

                for($i = 1; $i != 5; $i++){

                    factory(Category::class)->create([
                        'sort' => $i
                    ]);

                    factory(CategoryTranslation::class)->create(['locale' => 'ru', 'category_id' => $i]);
                    factory(CategoryTranslation::class)->create(['locale' => 'uk', 'category_id' => $i]);

                    factory(Partner::class,random_int(1, 4))->create(['category_id' => $i])
                        ->each(function (Partner $partner) {
                            factory(PartnerTranslation::class)->create(['locale' => 'uk', 'partner_id' => $partner->id]);
                            factory(PartnerTranslation::class)->create(['locale' => 'ru', 'partner_id' => $partner->id]);
                        });
                }

            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }
}


