<?php

use Faker\Generator as Faker;

class MenuSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('menu')->truncate();
        \DB::table('menu_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = $this->getData();

        try {
            \DB::transaction(function () use ($data) {

                foreach ($data as $item){
                    $menu = new \WezomCms\Menu\Models\Menu();
                    $menu->sort = $item['sort'];
                    $menu->group = $item['group'];

                    $menu->save();
                    foreach ($item['translates'] as $lang => $t){
                        $translations = new \WezomCms\Menu\Models\MenuTranslation();
                        $translations->locale = $lang;
                        $translations->name = $t['name'];
                        $translations->url = $t['url'];

                        $translations->menu_id = $menu->id;
                        $translations->save();
                    }

                    foreach ($item['children'] ?? [] as $c){
                        $child = new \WezomCms\Menu\Models\Menu();
                        $child->sort = $c['sort'];
                        $child->group = $c['group'];
                        $child->parent_id = $menu->id;
                        $child->save();

                        foreach ($c['translates'] as $l => $ct){
                            $translations_child = new \WezomCms\Menu\Models\MenuTranslation();
                            $translations_child->locale = $l;
                            $translations_child->name = $ct['name'];
                            $translations_child->url = $ct['url'];

                            $translations_child->menu_id = $child->id;
                            $translations_child->save();
                        }
                    }
                }
            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }

    protected function getData()
    {
        return [
            [
                'sort' => 0,
                'group' => 'header',
                'translates' => [
                    'ru' => [
                        'name' => 'Перестрахование',
                        'url' => '/perestrahovanie',
                    ],
                    'uk' => [
                        'name' => 'Перестрахування',
                        'url' => '/perestrahuvannya',
                    ]
                ],
            ],
            [
                'sort' => 1,
                'group' => 'header',
                'translates' => [
                    'ru' => [
                        'name' => 'Агентам и брокерам',
                        'url' => '/agentam-i-brokeram',
                    ],
                    'uk' => [
                        'name' => 'Агентам і брокерам',
                        'url' => '/agentam-i-brokeram',
                    ]
                ],
            ],
            [
                'sort' => 2,
                'group' => 'header',
                'translates' => [
                    'ru' => [
                        'name' => 'Партнерские программы',
                        'url' => '/partnerskie-programmy',
                    ],
                    'uk' => [
                        'name' => 'Партнерські програми',
                        'url' => '/partnerski-programi',
                    ]
                ],
            ],
            [
                'sort' => 3,
                'group' => 'header',
                'translates' => [
                    'ru' => [
                        'name' => 'Служба контроля качества',
                        'url' => '/sluzhba-kontrolya-kachestva',
                    ],
                    'uk' => [
                        'name' => 'Служба контролю якості',
                        'url' => '/sluzhba-kontrolyu-yakosti',
                    ]
                ],
            ],
            [
                'sort' => 4,
                'group' => 'header',
                'translates' => [
                    'ru' => [
                        'name' => 'COVID-19',
                        'url' => '/covid-19',
                    ],
                    'uk' => [
                        'name' => 'COVID-19',
                        'url' => '/covid-19',
                    ]
                ],
            ],
            [
                'sort' => 5,
                'group' => 'header',
                'translates' => [
                    'ru' => [
                        'name' => 'Блог',
                        'url' => '/blog',
                    ],
                    'uk' => [
                        'name' => 'Блог',
                        'url' => '/blog',
                    ]
                ],
            ],
            [
                'sort' => 7,
                'group' => 'header',
                'translates' => [
                    'ru' => [
                        'name' => 'Оплата',
                        'url' => '/payments',
                    ],
                    'uk' => [
                        'name' => 'Оплата',
                        'url' => '/payments',
                    ]
                ],
            ],
            [
                'sort' => 8,
                'group' => 'header',
                'translates' => [
                    'ru' => [
                        'name' => 'Отчетность',
                        'url' => '/reports',
                    ],
                    'uk' => [
                        'name' => 'Звітність',
                        'url' => '/reports',
                    ]
                ],
            ],
            [
                'sort' => 6,
                'group' => 'header',
                'translates' => [
                    'ru' => [
                        'name' => 'О компании',
                        'url' => '/o-kompanii',
                    ],
                    'uk' => [
                        'name' => 'Про компанію',
                        'url' => '/pro-kompaniyu',
                    ]
                ],
                'children' => [
                    [
                        'sort' => 0,
                        'group' => 'header',
                        'translates' => [
                            'ru' => [
                                'name' => 'О нас',
                                'url' => '/o-nas',
                            ],
                            'uk' => [
                                'name' => 'Про нас',
                                'url' => '/pro-nas',
                            ]
                        ],
                    ],
                    [
                        'sort' => 1,
                        'group' => 'header',
                        'translates' => [
                            'ru' => [
                                'name' => 'Наши партнеры',
                                'url' => '/nashi-partnery',
                            ],
                            'uk' => [
                                'name' => 'Наші партнери',
                                'url' => '/nashi-partneri',
                            ]
                        ],
                    ],
                    [
                        'sort' => 2,
                        'group' => 'header',
                        'translates' => [
                            'ru' => [
                                'name' => 'Отчетность',
                                'url' => '/otchetnost',
                            ],
                            'uk' => [
                                'name' => 'Звітність',
                                'url' => '/zvitnist',
                            ]
                        ],
                    ],
                    [
                        'sort' => 3,
                        'group' => 'header',
                        'translates' => [
                            'ru' => [
                                'name' => 'Документы',
                                'url' => '/dokumenti',
                            ],
                            'uk' => [
                                'name' => 'Документи',
                                'url' => '/dokumenti',
                            ]
                        ],
                    ],
                ],
            ],
            [
                'sort' => 0,
                'group' => 'mobile',
                'translates' => [
                    'ru' => [
                        'name' => 'Перестрахование',
                        'url' => '/perestrahovanie',
                    ],
                    'uk' => [
                        'name' => 'Перестрахування',
                        'url' => '/perestrahuvannya',
                    ]
                ],
            ],
            [
                'sort' => 1,
                'group' => 'mobile',
                'translates' => [
                    'ru' => [
                        'name' => 'Агентам и брокерам',
                        'url' => '/agentam-i-brokeram',
                    ],
                    'uk' => [
                        'name' => 'Агентам і брокерам',
                        'url' => '/agentam-i-brokeram',
                    ]
                ],
            ],
            [
                'sort' => 2,
                'group' => 'mobile',
                'translates' => [
                    'ru' => [
                        'name' => 'Партнерские программы',
                        'url' => '/partnerskie-programmy',
                    ],
                    'uk' => [
                        'name' => 'Партнерські програми',
                        'url' => '/partnerski-programi',
                    ]
                ],
            ],
            [
                'sort' => 3,
                'group' => 'mobile',
                'translates' => [
                    'ru' => [
                        'name' => 'Служба контроля качества',
                        'url' => '/sluzhba-kontrolya-kachestva',
                    ],
                    'uk' => [
                        'name' => 'Служба контролю якості',
                        'url' => '/sluzhba-kontrolyu-yakosti',
                    ]
                ],
            ],
            [
                'sort' => 4,
                'group' => 'mobile',
                'translates' => [
                    'ru' => [
                        'name' => 'COVID-19',
                        'url' => '/covid-19',
                    ],
                    'uk' => [
                        'name' => 'COVID-19',
                        'url' => '/covid-19',
                    ]
                ],
            ],
            [
                'sort' => 5,
                'group' => 'mobile',
                'translates' => [
                    'ru' => [
                        'name' => 'Блог',
                        'url' => '/blog',
                    ],
                    'uk' => [
                        'name' => 'Блог',
                        'url' => '/blog',
                    ]
                ],
            ],
            [
                'sort' => 6,
                'group' => 'mobile',
                'translates' => [
                    'ru' => [
                        'name' => 'О компании',
                        'url' => '/o-kompanii',
                    ],
                    'uk' => [
                        'name' => 'Про компанію',
                        'url' => '/pro-kompaniyu',
                    ]
                ],
                'children' => [
                    [
                        'sort' => 0,
                        'group' => 'mobile',
                        'translates' => [
                            'ru' => [
                                'name' => 'О нас',
                                'url' => '/o-nas',
                            ],
                            'uk' => [
                                'name' => 'Про нас',
                                'url' => '/pro-nas',
                            ]
                        ],
                    ],
                    [
                        'sort' => 1,
                        'group' => 'mobile',
                        'translates' => [
                            'ru' => [
                                'name' => 'Наши партнеры',
                                'url' => '/nashi-partnery',
                            ],
                            'uk' => [
                                'name' => 'Наші партнери',
                                'url' => '/nashi-partneri',
                            ]
                        ],
                    ],
                    [
                        'sort' => 2,
                        'group' => 'mobile',
                        'translates' => [
                            'ru' => [
                                'name' => 'Отчетность',
                                'url' => '/otchetnost',
                            ],
                            'uk' => [
                                'name' => 'Звітність',
                                'url' => '/zvitnist',
                            ]
                        ],
                    ],
                    [
                        'sort' => 3,
                        'group' => 'mobile',
                        'translates' => [
                            'ru' => [
                                'name' => 'Документы',
                                'url' => '/dokumenti',
                            ],
                            'uk' => [
                                'name' => 'Документи',
                                'url' => '/dokumenti',
                            ]
                        ],
                    ],
                ],
            ],
            [
                'sort' => 0,
                'group' => 'footer',
                'translates' => [
                    'ru' => [
                        'name' => 'Услуги',
                        'url' => '/yslugi',
                    ],
                    'uk' => [
                        'name' => 'Послуги',
                        'url' => '/poslugi',
                    ]
                ],
                'children' => [
                    [
                        'sort' => 0,
                        'group' => 'footer',
                        'translates' => [
                            'ru' => [
                                'name' => 'Страхование имущества',
                                'url' => '/strahovanie-imuschestva',
                            ],
                            'uk' => [
                                'name' => 'Страхування майна',
                                'url' => '/strahuvannya-mayna',
                            ]
                        ],
                    ],
                    [
                        'sort' => 1,
                        'group' => 'footer',
                        'translates' => [
                            'ru' => [
                                'name' => 'Страхование наземного транспорта',
                                'url' => '/strahuvannya-nazemnogo-transportu',
                            ],
                            'uk' => [
                                'name' => 'Страхування наземного транспорту',
                                'url' => '/strahuvannya-nazemnogo-transportu',
                            ]
                        ],
                    ],
                    [
                        'sort' => 2,
                        'group' => 'footer',
                        'translates' => [
                            'ru' => [
                                'name' => 'Личное страхование',
                                'url' => '/osobiste-strahuvannya',
                            ],
                            'uk' => [
                                'name' => 'Особисте страхування',
                                'url' => '/osobiste-strahuvannya',
                            ]
                        ],
                    ],
                    [
                        'sort' => 3,
                        'group' => 'footer',
                        'translates' => [
                            'ru' => [
                                'name' => 'Каско',
                                'url' => '/kasko',
                            ],
                            'uk' => [
                                'name' => 'Каско',
                                'url' => '/kasko',
                            ]
                        ],
                    ],
                ],
            ],
            [
                'sort' => 1,
                'group' => 'footer',
                'translates' => [
                    'ru' => [
                        'name' => 'О компании',
                        'url' => '/o-kompanii',
                    ],
                    'uk' => [
                        'name' => 'Про компанію',
                        'url' => '/pro-kompaniyu',
                    ]
                ],
                'children' => [
                    [
                        'sort' => 0,
                        'group' => 'footer',
                        'translates' => [
                            'ru' => [
                                'name' => 'О нас',
                                'url' => '/o-nas',
                            ],
                            'uk' => [
                                'name' => 'Про нас',
                                'url' => '/pro-nas',
                            ]
                        ],
                    ],
                    [
                        'sort' => 1,
                        'group' => 'footer',
                        'translates' => [
                            'ru' => [
                                'name' => 'Партнеры',
                                'url' => '/partneri',
                            ],
                            'uk' => [
                                'name' => 'Партнери',
                                'url' => '/spartneri',
                            ]
                        ],
                    ],
                    [
                        'sort' => 2,
                        'group' => 'footer',
                        'translates' => [
                            'ru' => [
                                'name' => 'История',
                                'url' => '/istoriya',
                            ],
                            'uk' => [
                                'name' => 'Історія',
                                'url' => '/istoriya',
                            ]
                        ],
                    ],
                    [
                        'sort' => 3,
                        'group' => 'footer',
                        'translates' => [
                            'ru' => [
                                'name' => 'Финансовые отчеты',
                                'url' => '/finansovi-zviti',
                            ],
                            'uk' => [
                                'name' => 'Фінансові звіти',
                                'url' => '/finansovi-zviti',
                            ]
                        ],
                    ],
                    [
                        'sort' => 4,
                        'group' => 'footer',
                        'translates' => [
                            'ru' => [
                                'name' => 'Филиалы',
                                'url' => '/filiyi',
                            ],
                            'uk' => [
                                'name' => 'Філії',
                                'url' => '/filiyi',
                            ]
                        ],
                    ],
                ],
            ],
            [
                'sort' => 1,
                'group' => 'footer',
                'translates' => [
                    'ru' => [
                        'name' => 'Информация',
                        'url' => '/informatsiya',
                    ],
                    'uk' => [
                        'name' => 'Інформація',
                        'url' => '/informatsiya',
                    ]
                ],
                'children' => [
                    [
                        'sort' => 0,
                        'group' => 'footer',
                        'translates' => [
                            'ru' => [
                                'name' => 'Блог',
                                'url' => '/blog',
                            ],
                            'uk' => [
                                'name' => 'Блог',
                                'url' => '/blog',
                            ]
                        ],
                    ],
                    [
                        'sort' => 1,
                        'group' => 'footer',
                        'translates' => [
                            'ru' => [
                                'name' => 'Контакты',
                                'url' => '/kontakti',
                            ],
                            'uk' => [
                                'name' => 'Контакти',
                                'url' => '/kontakti',
                            ]
                        ],
                    ],
                    [
                        'sort' => 2,
                        'group' => 'footer',
                        'translates' => [
                            'ru' => [
                                'name' => 'История',
                                'url' => '/istoriya',
                            ],
                            'uk' => [
                                'name' => 'Історія',
                                'url' => '/istoriya',
                            ]
                        ],
                    ],
                    [
                        'sort' => 3,
                        'group' => 'footer',
                        'translates' => [
                            'ru' => [
                                'name' => 'Финансовые отчеты',
                                'url' => '/finansovi-zviti',
                            ],
                            'uk' => [
                                'name' => 'Фінансові звіти',
                                'url' => '/finansovi-zviti',
                            ]
                        ],
                    ],
                    [
                        'sort' => 4,
                        'group' => 'footer',
                        'translates' => [
                            'ru' => [
                                'name' => 'Филиалы',
                                'url' => '/filiyi',
                            ],
                            'uk' => [
                                'name' => 'Філії',
                                'url' => '/filiyi',
                            ]
                        ],
                    ],
                ],
            ],
        ];
    }

}





