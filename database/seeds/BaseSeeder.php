<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class BaseSeeder extends Seeder
{
    private $faker;

    public function __construct()
    {
        $this->faker = resolve(Faker::class);
    }

    protected function randomIcon()
    {
        $icons = [];
        $iconsDirectory = scandir(__DIR__ . '/../../modules/ui/resources/svg/icons');

        foreach ($iconsDirectory as $item){
            if($item != '.' && $item != '..'){
                $icons[] = current(explode('.', $item));
            }
        }

        return $icons[random_int(0, count($icons) - 1)];
    }

    protected function getFaker()
    {
        return $this->faker;
    }
}



