<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // most
//        $this->call(RegionsSeeder::class);
//        $this->call(SettingsSeeder::class);
//
//         $this->call(EmployeeSeeder::class);
//         $this->call(RisksSeeder::class);
//         $this->call(FaqSeeder::class);
//         $this->call(AdditionalServiceSeeder::class);
//         $this->call(StepServiceSeeder::class);
//         $this->call(DocumentServiceSeeder::class);
//         $this->call(ObjectInsuranceSeeder::class);
//         $this->call(PartnerSeeder::class);
//         $this->call(CounterSeeder::class);
//         $this->call(BenefitsSeeder::class);
//         $this->call(ServicesSeeder::class);
//         $this->call(MenuSeeder::class);
//         $this->call(TextPageSeeder::class);
//         $this->call(BranchSeeder::class);
//         $this->call(SliderSeeder::class);
//         $this->call(ArticlesSeeder::class);
//         $this->call(PaymentSeeder::class);
//         $this->call(ReportsSeeder::class);
         $this->call(AboutSeeder::class);
//         $this->call(AboutMenuSeeder::class);
    }
}
