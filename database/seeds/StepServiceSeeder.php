<?php

class StepServiceSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('service_steps')->truncate();
        \DB::table('service_step_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            \DB::transaction(function () {

                for($i = 1; $i != 15; $i++){

                    factory(\WezomCms\Services\Models\Step::class)->create([
                        'sort' => $i,
                        'icon' => $this->randomIcon()
                    ]);

                    factory(\WezomCms\Services\Models\StepTranslation::class)->create(['locale' => 'ru', 'step_id' => $i]);
                    factory(\WezomCms\Services\Models\StepTranslation::class)->create(['locale' => 'uk', 'step_id' => $i]);
                }

            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }
}

