<?php

use WezomCms\Payments\Models\Link;
use WezomCms\Payments\Models\LinkTranslation;
use WezomCms\Payments\Models\PaymentTranslation;

class PaymentSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('payment_links')->truncate();
        \DB::table('payment_link_translations')->truncate();
        \DB::table('payments')->truncate();
        \DB::table('payment_translations')->truncate();
        \DB::table('payments_links_relation')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            \DB::transaction(function (){

                // link for payment
                foreach ($this->getDataLink() as $item){
                    $l = new Link();
                    $l->sort = $item['sort'];
                    $l->type = $item['type'];
                    $l->link = $item['link'];
                    $l->save();

                    foreach ($item['translations'] as $lang => $tran){
                        $t = new LinkTranslation();
                        $t->locale = $lang;
                        $t->link_id = $l->id;
                        $t->name = $tran['name'];
                        $t->save();
                    }
                }

                //payment
                foreach ($this->getDataPayment() as $item){

                    $p = new WezomCms\Payments\Models\Payment();
                    $p->sort = $item['sort'];
                    $p->save();

                    foreach ($item['translations'] as $lang => $tran){
                        $pt = new PaymentTranslation();
                        $pt->payment_id = $p->id;
                        $pt->locale = $lang;
                        $pt->name = $tran['name'];
                        $pt->text = $tran['text'];
                        $pt->save();
                    }
                }

                //relation
                DB::table('payments_links_relation')->insert([
                    ['payment_id' => 2, 'link_id' => 1],
                    ['payment_id' => 4, 'link_id' => 2],
                    ['payment_id' => 3, 'link_id' => 3],
                    ['payment_id' => 4, 'link_id' => 4],
                ]);
            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }

    private function getDataLink()
    {
        return [
            [
                'sort' => 0,
                'type' => Link::TYPE_BUTTON,
                'link' => 'http://some_link',
                'translations' => [
                    'ru' => [
                        'name' => 'Оплатить'
                    ],
                    'uk' => [
                        'name' => 'Оплатити'
                    ]
                ]
            ],
            [
                'sort' => 1,
                'type' => Link::TYPE_BUTTON,
                'link' => 'http://some_link',
                'translations' => [
                    'ru' => [
                        'name' => 'Подробнее'
                    ],
                    'uk' => [
                        'name' => 'Докладніше'
                    ]
                ]
            ],
            [
                'sort' => 2,
                'type' => Link::TYPE_LINK,
                'link' => 'http://some_link',
                'translations' => [
                    'ru' => [
                        'name' => 'Оплата других видов страхования'
                    ],
                    'uk' => [
                        'name' => 'Оплата інших видів страхування'
                    ]
                ]
            ],
            [
                'sort' => 3,
                'type' => Link::TYPE_LINK,
                'link' => 'http://some_link',
                'translations' => [
                    'ru' => [
                        'name' => '"Медичний захист" - Добровольное медицинское страхование'
                    ],
                    'uk' => [
                        'name' => '"Медичний захист" - Добровільне медичне страхування'
                    ]
                ]
            ]
        ];
    }

    private function getDataPayment()
    {
        return [
            [
                'sort' => 0,
                'translations' => [
                    'ru' => [
                        'name' => 'Банк Півленний',
                        'text' => '<p><strong>Наименование:</strong> ЧАО "СК Теком"</p>
<p><strong>Код ЕГРПОУ:</strong> 201187676</p>
<p><strong>IBAN (текущий счет компании):</strong> UA898670989788976980000007</p>
<p><strong>МФО:</strong> 2345455</p>
<p><strong>Инд. налоговой №:</strong> 3453456434</p>
<p><strong>Номер выписки из реестра плательщиков НДС:</strong> 23452343</p>'
                    ],
                    'uk' => [
                        'name' => 'Банк Півленний',
                        'text' => '<p><strong>Наименование:</strong> ЧАО "СК Теком"</p>
<p><strong>Код ЕГРПОУ:</strong> 201187676</p>
<p><strong>IBAN (текущий счет компании):</strong> UA898670989788976980000007</p>
<p><strong>МФО:</strong> 2345455</p>
<p><strong>Инд. налоговой №:</strong> 3453456434</p>
<p><strong>Номер выписки из реестра плательщиков НДС:</strong> 23452343</p>'
                    ]
                ]
            ],
            [
                'sort' => 1,
                'translations' => [
                    'ru' => [
                        'name' => 'Приват24',
                        'text' => '<p>На нашем сайте Вы можете быстро и удобно произвести оплату наших страховых услуг при помощи сервиса Portmone</p>'
                    ],
                    'uk' => [
                        'name' => 'Приват24',
                        'text' => '<p>На нашому сайті Ви можете швидко і зручно зробити оплату наших страхових послуг за допомогою сервісу Portmone</p>'
                    ]
                ]
            ],
            [
                'sort' => 2,
                'translations' => [
                    'ru' => [
                        'name' => 'Portmone',
                        'text' => '<p>На нашем сайте Вы можете быстро и удобно произвести оплату наших страховых услуг при помощи сервиса Portmone</p>'
                    ],
                    'uk' => [
                        'name' => 'Portmone',
                        'text' => '<p>На нашому сайті Ви можете швидко і зручно зробити оплату наших страхових послуг за допомогою сервісу Portmone</p>'
                    ]
                ]
            ],
            [
                'sort' => 3,
                'translations' => [
                    'ru' => [
                        'name' => 'Ibox',
                        'text' => '<p>Вы можете быстро и удобно найти ближайший к вам терминал</p>'
                    ],
                    'uk' => [
                        'name' => 'Ibox',
                        'text' => '<p>Ви можете швидко і зручно знайти найближчий до вас термінал</p>',
                    ]
                ]
            ]
        ];
    }
}
