<?php

use WezomCms\Benefits\Models\Benefits;
use WezomCms\Benefits\Models\BenefitsTranslation;

class AboutSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('about_awards')->truncate();
        \DB::table('about_award_translations')->truncate();
        \DB::table('about_events')->truncate();
        \DB::table('about_event_translations')->truncate();
        \DB::table('about_agents')->truncate();
        \DB::table('about_agent_translations')->truncate();
        \DB::table('about_licenses')->truncate();
        \DB::table('about_license_translations')->truncate();
        \DB::table('about_license_groups')->truncate();
        \DB::table('about_license_group_translations')->truncate();
        \DB::table('about_rule_groups')->truncate();
        \DB::table('about_rule_group_translations')->truncate();
        \DB::table('about_rules')->truncate();
        \DB::table('about_rule_translations')->truncate();
        \DB::table('about_contract_groups')->truncate();
        \DB::table('about_contract_group_translations')->truncate();
        \DB::table('about_contracts')->truncate();
        \DB::table('about_contracts_translations')->truncate();
        \DB::table('about_informations')->truncate();
        \DB::table('about_information_translations')->truncate();
        \DB::table('about_details')->truncate();
        \DB::table('about_details_translations')->truncate();
        \DB::table('about_detail_files')->truncate();
        \DB::table('about_detail_file_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            \DB::transaction(function () {

                for($i = 1; $i != 8; $i++){
                    factory(\WezomCms\About\Models\AboutUs\Award::class)->create([
                        'sort' => $i
                    ]);

                    factory(\WezomCms\About\Models\AboutUs\AwardTranslation::class)->create(['locale' => 'ru', 'award_id' => $i]);
                    factory(\WezomCms\About\Models\AboutUs\AwardTranslation::class)->create(['locale' => 'uk', 'award_id' => $i]);
                }

                for($i = 1; $i != 10; $i++){
                    factory(\WezomCms\About\Models\AboutUs\Event::class)->create([
                        'year' =>  \WezomCms\About\Models\AboutUs\Event::yearsForSelect(30, false)[$i]
                    ]);

                    factory(\WezomCms\About\Models\AboutUs\EventTranslation::class)->create(['locale' => 'ru', 'event_id' => $i]);
                    factory(\WezomCms\About\Models\AboutUs\EventTranslation::class)->create(['locale' => 'uk', 'event_id' => $i]);
                }

                for($i = 1; $i != 11; $i++){
                    factory(\WezomCms\About\Models\Agent::class)->create([
                        'sort' => $i
                    ]);

                    factory(\WezomCms\About\Models\AgentTranslation::class)->create(['locale' => 'ru', 'agent_id' => $i]);
                    factory(\WezomCms\About\Models\AgentTranslation::class)->create(['locale' => 'uk', 'agent_id' => $i]);
                }


                for($i = 1; $i != 6; $i++){
                    factory(\WezomCms\About\Models\Information::class)->create([
                        'sort' => $i
                    ]);

                    factory(\WezomCms\About\Models\InformationTranslation::class)->create(['locale' => 'ru', 'information_id' => $i]);
                    factory(\WezomCms\About\Models\InformationTranslation::class)->create(['locale' => 'uk', 'information_id' => $i]);
                }

                for($i = 1; $i != 9; $i++){
                    factory(\WezomCms\About\Models\Details\Detail::class)->create([
                        'sort' => $i
                    ]);

                    factory(\WezomCms\About\Models\Details\DetailTranslation::class)->create(['locale' => 'ru', 'detail_id' => $i]);
                    factory(\WezomCms\About\Models\Details\DetailTranslation::class)->create(['locale' => 'uk', 'detail_id' => $i]);
                }

                for($i = 1; $i != 9; $i++){
                    factory(\WezomCms\About\Models\Details\DetailFile::class)->create([
                        'sort' => $i
                    ]);

                    factory(\WezomCms\About\Models\Details\DetailFileTranslation::class)->create(['locale' => 'ru', 'detail_file_id' => $i]);
                    factory(\WezomCms\About\Models\Details\DetailFileTranslation::class)->create(['locale' => 'uk', 'detail_file_id' => $i]);
                }

                for($i = 1; $i != 4; $i++){
                    factory(\WezomCms\About\Models\Rules\Group::class)->create([
                        'sort' => $i
                    ]);

                    factory(\WezomCms\About\Models\Rules\GroupTranslation::class)->create(['locale' => 'ru', 'group_id' => $i]);
                    factory(\WezomCms\About\Models\Rules\GroupTranslation::class)->create(['locale' => 'uk', 'group_id' => $i]);

                    $count_j = 1;
                    $limit_count_j = 8;
                    if($i == 2){
                        $count_j = 8;
                        $limit_count_j = 16;
                    }
                    if($i == 3){
                        $count_j = 16;
                        $limit_count_j = 24;
                    }

                    for($j = $count_j; $j != $limit_count_j; $j++){
                        factory(\WezomCms\About\Models\Rules\Rule::class)->create([
                            'sort' => $j,
                            'rule_group_id' => $i
                        ]);

                        factory(\WezomCms\About\Models\Rules\RuleTranslation::class)->create(['locale' => 'ru', 'rule_id' => $j]);
                        factory(\WezomCms\About\Models\Rules\RuleTranslation::class)->create(['locale' => 'uk', 'rule_id' => $j]);
                    }
                }

                for($i = 1; $i != 4; $i++){
                    factory(\WezomCms\About\Models\License\Group::class)->create([
                        'sort' => $i
                    ]);

                    factory(\WezomCms\About\Models\License\GroupTranslation::class)->create(['locale' => 'ru', 'group_id' => $i]);
                    factory(\WezomCms\About\Models\License\GroupTranslation::class)->create(['locale' => 'uk', 'group_id' => $i]);

                    $count_j = 1;
                    $limit_count_j = 8;
                    if($i == 2){
                        $count_j = 8;
                        $limit_count_j = 16;
                    }
                    if($i == 3){
                        $count_j = 16;
                        $limit_count_j = 24;
                    }

                    for($j = $count_j; $j != $limit_count_j; $j++){
                        factory(\WezomCms\About\Models\License\License::class)->create([
                            'sort' => $j,
                            'group_id' => $i
                        ]);

                        factory(\WezomCms\About\Models\License\LicenseTranslation::class)->create(['locale' => 'ru', 'license_id' => $j]);
                        factory(\WezomCms\About\Models\License\LicenseTranslation::class)->create(['locale' => 'uk', 'license_id' => $j]);
                    }
                }

                for($i = 1; $i != 4; $i++){
                    factory(\WezomCms\About\Models\Contracts\Group::class)->create([
                        'sort' => $i
                    ]);

                    factory(\WezomCms\About\Models\Contracts\GroupTranslation::class)->create(['locale' => 'ru', 'group_id' => $i]);
                    factory(\WezomCms\About\Models\Contracts\GroupTranslation::class)->create(['locale' => 'uk', 'group_id' => $i]);

                    $count_j = 1;
                    $limit_count_j = 8;
                    if($i == 2){
                        $count_j = 8;
                        $limit_count_j = 16;
                    }
                    if($i == 3){
                        $count_j = 16;
                        $limit_count_j = 24;
                    }

                    for($j = $count_j; $j != $limit_count_j; $j++){
                        factory(\WezomCms\About\Models\Contracts\Contract::class)->create([
                            'sort' => $j,
                            'contract_group_id' => $i
                        ]);

                        factory(\WezomCms\About\Models\Contracts\ContractTranslation::class)->create(['locale' => 'ru', 'contract_id' => $j]);
                        factory(\WezomCms\About\Models\Contracts\ContractTranslation::class)->create(['locale' => 'uk', 'contract_id' => $j]);
                    }
                }
//
//                //settings
//                $exist = \WezomCms\Core\Models\Setting::where([
//                    'module' => 'benefits',
//                    'group' => 'page'
//                ])->exists();
//
//                if(!$exist){
//                    foreach ($settings as $item){
//                        $p = new \WezomCms\Core\Models\Setting();
//                        $p->module = $item->module;
//                        $p->group = $item->group;
//                        $p->key = $item->key;
//                        $p->type = $item->type;
//
//                        $p->save();
//                        foreach ($item->translates as $lang => $value){
//                            $translations = new \WezomCms\Core\Models\SettingTranslation();
//                            $translations->setting_id = $p->id;
//                            $translations->locale = $lang;
//                            $translations->value = $value;
//
//                            $translations->save();
//                        }
//                    }
//                }

            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }
}
