<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use WezomCms\Orders\Enums\PayedModes;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('delivery_type', 20)->nullable();
            $table->foreignId('delivery_id')->nullable()->constrained()->nullOnDelete();
            $table->string('delivery_address')->nullable();
            $table->foreignId('payment_id')->nullable()->constrained()->nullOnDelete();
            $table->foreignId('status_id')->nullable()->constrained('order_statuses')->nullOnDelete();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->boolean('payed')->default(false);
            $table->boolean('dont_call_back')->default(false);
            $table->enum('payed_mode', PayedModes::getValues())->nullable();
            $table->dateTime('payed_at')->nullable();
            $table->string('ttn')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
