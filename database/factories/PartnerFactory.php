<?php

use Faker\Generator as Faker;

$factory->define(WezomCms\Partners\Models\Category::class, function (Faker $faker) {
    return [
        'published' => $faker->boolean(90),
    ];
});

$factory->define(WezomCms\Partners\Models\CategoryTranslation::class, function (Faker $faker) {
    $name = $faker->city;
    return [
        'name' => $name,
        'slug' => \Illuminate\Support\Str::slug($name),
    ];
});

$factory->define(WezomCms\Partners\Models\Partner::class, function (Faker $faker) {
    return [
        'published' => $faker->boolean(80),
    ];
});

$factory->define(WezomCms\Partners\Models\PartnerTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->realText(50),
        'description' => $faker->realText($faker->numberBetween(100, 250)),
    ];
});
