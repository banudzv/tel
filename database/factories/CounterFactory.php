<?php

use Faker\Generator as Faker;
use WezomCms\Counter\Models\Counter;
use WezomCms\Counter\Models\CounterTranslation;

$factory->define(Counter::class, function (Faker $faker) {
    return [
        'published' => true,
        'start' => $faker->numberBetween(0, 4),
        'end' => $faker->numberBetween(100, 1000),
        'step' => $faker->numberBetween(1, 10),
        'duration' => $faker->numberBetween(1, 5),
    ];
});

$factory->define(CounterTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->realText(50),
    ];
});
