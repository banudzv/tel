<?php

use Faker\Generator as Faker;
use WezomCms\Articles\Models\Article;
use WezomCms\Articles\Models\ArticleTranslation;
use WezomCms\News\Models\News;
use WezomCms\News\Models\NewsTranslation;
use Illuminate\Support\Carbon;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'published_at' =>  $faker->dateTimeBetween('-10 days', 'now'),
        'published' => $faker->boolean(80),
        'for_main' => $faker->boolean(50),
    ];
});

$factory->define(ArticleTranslation::class, function (Faker $faker) {

    $name = $faker->realText(20);
    return [
        'locale' => 'us',
        'name' => $name,
        'slug' => \Illuminate\Support\Str::slug($name),
        'text' => $faker->realText($faker->numberBetween(100, 250)),
        'article_id' => 1
    ];
});

$factory->define(\WezomCms\Articles\Models\ArticleTag::class, function (Faker $faker) {

    return [
        'published' => $faker->boolean(80),
        'slug' => $faker->slug,
    ];
});

$factory->define(\WezomCms\Articles\Models\ArticleTagTranslation::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
    ];
});
