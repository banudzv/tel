<?php

use Faker\Generator as Faker;

$factory->define(\WezomCms\About\Models\AboutUs\Award::class, function (Faker $faker) {
    return [
        'published' => true,
    ];
});

$factory->define(\WezomCms\About\Models\AboutUs\AwardTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->userName,
        'text' => $faker->realText($faker->numberBetween(50, 80))
    ];
});
