<?php

use Faker\Generator as Faker;

$factory->define(\WezomCms\Services\Models\Risk::class, function (Faker $faker) {
    return [
        'published' => true,
    ];
});

$factory->define(\WezomCms\Services\Models\RiskTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->realText($faker->numberBetween(10, 50)),
    ];
});
