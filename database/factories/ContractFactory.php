<?php

use Faker\Generator as Faker;

$factory->define(\WezomCms\About\Models\Contracts\Contract::class, function (Faker $faker) {
    return [
        'published' => true,
    ];
});

$factory->define(\WezomCms\About\Models\Contracts\ContractTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
    ];
});
