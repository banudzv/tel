@extends('cms-ui::layouts.main')

@php
    /**
     * @var $email null|string
     * @var $token null|string
     */
@endphp

@section('content')
    <div class="container">
        <h1>@lang('cms-users::site.Восстановление пароля')</h1>
        {!! Form::open(['url' => route('auth.password.update', compact('token')), 'class' => 'js-import']) !!}
            <div>
                <label class="form-group__label" for="l-email"></label>
                <input type="email"
                       name="email"
                       inputmode="email"
                       placeholder="@lang('cms-users::site.E-mail') *"
                       value="{{ $email ?? old('email') }}"
                       required="required">
            </div>
            <div>
                <label class="form-group__label" for="l-password"></label>
                <input type="password"
                       name="password"
                       minlength="{{ config('cms.users.users.password_min_length') }}"
                       placeholder="@lang('cms-users::site.Новый пароль') *"
                       required="required">
            </div>
            <div>
                <label class="form-group__caption" for="l-password-confirmation"></label>
                <input type="password"
                       name="password_confirmation"
                       minlength="{{ config('cms.users.users.password_min_length') }}"
                       placeholder="@lang('cms-users::site.Повторите пароль') *"
                       required="required">
            </div>
            <button type="submit">@lang('cms-users::site.Восстановить')</button>
        {!! Form::close() !!}
    </div>
@endsection
