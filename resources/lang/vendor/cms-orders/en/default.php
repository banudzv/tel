<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
        'deliveries' => [
            'pickup' => 'Pickup',
            'postal' => 'Postal office',
            'courier' => 'Courier',
        ],
        'payments' => [
            'payed_modes' => [
                'manual' => 'Manual',
                'auto' => 'Auto',
            ],
            'Payment order' => 'Payment order :order',
        ],
        'delivery_types' => [
            'postal' => 'Postal',
            'courier' => 'Courier',
        ],
        'payed_modes' => [
            'manual' => 'Manual',
            'auto' => 'Auto',
        ],
    ],
];
