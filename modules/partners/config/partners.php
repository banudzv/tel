<?php

use WezomCms\Partners\Widgets;

return [
    'index_page' => false, // Enable/disable single page with list
    'images' => [
        'directory' => 'partners',
        'placeholder' => 'no-avatar.png',
        'default' => 'medium', // For admin image preview
        'sizes' => [
            'medium' => [
                'width' => 170,
                'height' => 170,
                'mode' => 'fit',
            ],
        ],
    ],
    'widgets' => [
        'partners' => Widgets\PartnersWidget::class,
    ]
];
