<?php

namespace WezomCms\Partners\ModelFilters;

use EloquentFilter\ModelFilter;
use WezomCms\Core\Contracts\Filter\FilterListFieldsInterface;
use WezomCms\Core\Filter\FilterField;
use WezomCms\Partners\Models\Partner;
use WezomCms\Partners\Repositories\PartnerCategoryRepository;

/**
 * Class PartnerFilter
 * @package WezomCms\Partners\ModelFilters
 * @mixin Partner
 */
class PartnerFilter extends ModelFilter implements FilterListFieldsInterface
{
    /**
     * Generate array with fields
     * @return iterable|FilterField[]
     */
    public function getFields(): iterable
    {
        $categoryRepository = resolve(PartnerCategoryRepository::class);

        return [
            FilterField::makeName()->size(2),
            FilterField::published(),
            FilterField::make()
                ->name('country')
                ->label(__('cms-partners::admin.Category'))
                ->class('js-select2')
                ->type(FilterField::TYPE_SELECT)
                ->options($categoryRepository->getBySelect())
            ,
        ];
    }

    public function country($id)
    {
        $this->where('category_id', $id);
    }

    public function published($published)
    {
        $this->where('published', $published);
    }

    public function name($name)
    {
        $this->related('translations', 'name', 'LIKE', '%' . $name . '%');
    }
}
