<?php

namespace WezomCms\Partners\Widgets;

use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\Partners\Models\Category;
use WezomCms\Partners\Models\Partner;
use WezomCms\Partners\Repositories\PartnerCategoryRepository;

class PartnersWidget extends AbstractWidget
{
    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        /** @var $models Category[] */
        $models = resolve(PartnerCategoryRepository::class)->getByFront();

        if($models->isEmpty()){
            return null;
        }

        return compact('models');
    }
}
