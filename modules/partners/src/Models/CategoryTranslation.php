<?php

namespace WezomCms\Partners\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * \WezomCms\Partners\Models\CategoryTranslation
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $slug
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation wherePartnerId($value)
 * @mixin \Eloquent
 */
class CategoryTranslation extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $table = 'partner_category_translations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug'];
}
