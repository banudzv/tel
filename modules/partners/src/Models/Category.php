<?php

namespace WezomCms\Partners\Models;

use Illuminate\Database\Eloquent\Model;
use WezomCms\Core\ExtendPackage\Translatable;
use WezomCms\Core\Traits\Model\PublishedTrait;

/**
 * \WezomCms\Partners\Models\Category
 *
 * @property int $id
 * @property bool $published
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \WezomCms\Partners\Models\PartnerTranslation $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Partners\Models\PartnerTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner published()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner publishedWithSlug($slug, $slugField = 'slug')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\Partner withTranslation()
 * @mixin \Eloquent
 * @mixin CategoryTranslation
 */

class Category extends Model
{
    use Translatable;
    use PublishedTrait;

    protected $table = 'partner_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['published', 'sort'];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected $translatedAttributes = ['name'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['published' => 'bool'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    public function partners()
    {
        return $this->hasMany(Partner::class);
    }
}

