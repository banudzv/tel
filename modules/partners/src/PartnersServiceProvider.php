<?php

namespace WezomCms\Partners;

use WezomCms\Core\BaseServiceProvider;
use WezomCms\Core\Contracts\PermissionsContainerInterface;
use WezomCms\Core\Contracts\SitemapXmlGeneratorInterface;
use WezomCms\Core\Traits\SidebarMenuGroupsTrait;

class PartnersServiceProvider extends BaseServiceProvider
{
    use SidebarMenuGroupsTrait;

    /**
     * All module widgets.
     *
     * @var array|string|null
     */
    protected $widgets = 'cms.partners.partners.widgets';

    /**
     * @param  PermissionsContainerInterface  $permissions
     */
    public function permissions(PermissionsContainerInterface $permissions)
    {
        $permissions->add('partners', __('cms-partners::admin.Partners'))->withEditSettings();
        $permissions->add('partner-categories', __('cms-partners::admin.Category'));
    }

    public function adminMenu()
    {
        $partners = $this->contentGroup()
            ->add(__('cms-partners::admin.Partners'), route('admin.partners.index'))
            ->data('icon', 'fa-handshake-o')
            ->data('position', 13)
            ->nickname('partners');

        $partners->add(__('cms-partners::admin.Category'), route('admin.partner-categories.index'))
            ->data('permission', 'partner-categories.view')
            ->data('icon', 'fa-list')
            ->data('position', 1)
        ;

        $partners->add(__('cms-partners::admin.Partners'), route('admin.partners.index'))
            ->data('permission', 'partners.view')
            ->data('icon', 'fa-handshake-o')
            ->data('position', 2)
        ;
    }

    /**
     * @return array
     */
    public function sitemap()
    {
        if (config('cms.partners.partners.index_page')) {
            return [
                [
                    'id' => 'partners',
                    'sort' => 16,
                    'parent_id' => 0,
                    'name' => settings('partners.site.name'),
                    'url' => route('partners'),
                ],
            ];
        }

        return [];
    }

    /**
     * @param  SitemapXmlGeneratorInterface  $sitemap
     */
    public function sitemapXml(SitemapXmlGeneratorInterface $sitemap)
    {
        if (config('cms.partners.partners.index_page')) {
            $sitemap->addLocalizedRoute('partners');
        }
    }
}
