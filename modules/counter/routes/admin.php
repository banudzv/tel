<?php

use WezomCms\Counter\Http\Controllers\Admin\CounterController;

Route::adminResource('counters', CounterController::class);
