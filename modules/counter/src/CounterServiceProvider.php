<?php

namespace WezomCms\Counter;

use WezomCms\Core\BaseServiceProvider;
use WezomCms\Core\Contracts\PermissionsContainerInterface;
use WezomCms\Core\Traits\SidebarMenuGroupsTrait;

class CounterServiceProvider extends BaseServiceProvider
{
	use SidebarMenuGroupsTrait;

	/**
	 * All module widgets.
	 *
	 * @var array|string|null
	 */
	protected $widgets = 'cms.counter.counter.widgets';

	/**
	 * @param  PermissionsContainerInterface  $permissions
	 */
	public function permissions(PermissionsContainerInterface $permissions)
	{
		$permissions->add('counters', __('cms-counter::admin.Counter'));
	}

	public function adminMenu()
	{
		$this->contentGroup()
			->add(__('cms-counter::admin.Counter'), route('admin.counters.index'))
			->data('permission', 'counters.view')
			->data('icon', 'fa-newspaper-o')
			->nickname('counter')
		;
	}
}
