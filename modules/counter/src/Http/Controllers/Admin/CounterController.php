<?php

namespace WezomCms\Counter\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\CatalogDelivery\Http\Requests\Admin\DeliveryTypeRequest;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Counter\Http\Requests\Admin\CounterRequest;
use WezomCms\Counter\Models\Counter;

class CounterController extends AbstractCRUDController
{
	/**
	 * Model name.
	 *
	 * @var string
	 */
	protected $model = Counter::class;

	/**
	 * Indicates whether to use pagination.
	 *
	 * @var bool
	 */
	protected $paginate = false;

	/**
	 * Base view path name.
	 *
	 * @var string
	 */
	protected $view = 'cms-counter::admin.counter';

	/**
	 * Resource route name.
	 *
	 * @var string
	 */
	protected $routeName = 'admin.counters';

	/**
	 * Form request class name.
	 *
	 * @var string
	 */
	protected $request = CounterRequest::class;

	public function __construct()
	{
		parent::__construct();

	}

	/**
	 * Resource name for breadcrumbs and title.
	 *
	 * @return string
	 */
	protected function title(): string
	{
		return __('cms-counter::admin.Counter');
	}

	/**
	 * @param  Builder  $query
	 * @param  Request  $request
	 */
	protected function selectionIndexResult($query, Request $request)
	{
		$query->orderBy('sort');
	}
}

