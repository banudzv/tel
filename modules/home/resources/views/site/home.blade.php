@extends('cms-ui::layouts.base')

@php
/**
 * @var $seo_text string
 */
@endphp

@section('main')

    @widget('slider')
    @widget('services:for-main')
    @widget('counter', ['modification' => 'counter'])
    @widget('benefits')
    <div class="section section--home-reports">
        <div class="container">
            <a href="{{route('auth.login-form')}}">enter</a>
            <a href="{{route('auth.register-form')}}">reg</a>
            <a href="{{route('cabinet')}}">cabinet</a>
            <div class="_grid _justify-center _spacer _spacer--md">
                <div class="_cell _cell--12 _ms:cell--10 _df:cell--8 _lg:cell--6">
                    <div class="reports-block">
                        @widget('reports:report-for-main')
                    </div>
                </div>
                <div class="_cell _cell--12 _ms:cell--10 _df:cell--8 _lg:cell--6">
                    <div class="reports-block">
                        @widget('callbacks:consultation')
                    </div>
                </div>
            </div>
        </div>
    </div>

    @widget('branches:list')
    @widget('callbacks:quality-control')
    @widget('articles:blog')
    @widget('partners')

    @include('cms-ui::partials.seo-text', ['hideH1' => true])
@endsection
