<?php

Route::get('/', 'WezomCms\\Home\\Http\\Controllers\\Site\\HomeController@index')->name('home');
Route::get('/search','WezomCms\\Home\\Http\\Controllers\\Site\\SearchController@result')->name('search-results');
Route::post('/search','WezomCms\\Home\\Http\\Controllers\\Site\\SearchController@search')->name('search');
