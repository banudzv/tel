<?php

namespace WezomCms\Core\Foundation;

class Icon
{
    public static function getAllIconAsName():array
    {
        $symbols = [];
        $content = file_get_contents(config('cms.ui.ui.svg.accident_insurance'));
        if ($content) {
            preg_match_all('/<symbol\s((?!<).)*>/', $content, $_symbols);
            if ($_symbols) {
                foreach ($_symbols[0] as $_symbol) {
                    preg_match_all('/id="(((?!").)*)"/', $_symbol, $_ids);
                    if ($_ids) {
                        array_push($symbols, $_ids[1][0]);
                    }
                }
            }
        }

        return $symbols;
    }

    public static function getAllIconGroups():array
    {
        $data = [];
        foreach(config('cms.ui.ui.svg') as $key => $path){
            if(!empty($path)){
                $symbols = [];
                $content = file_get_contents(config('cms.ui.ui.svg.'.$key));
                if ($content) {
                    preg_match_all('/<symbol\s((?!<).)*>/', $content, $_symbols);
                    if ($_symbols) {
                        foreach ($_symbols[0] as $_symbol) {
                            preg_match_all('/id="(((?!").)*)"/', $_symbol, $_ids);
                            if ($_ids) {
                                array_push($symbols, $_ids[1][0]);
                            }
                        }
                    }
                }

                $data[$key] = $symbols;
            }
        }

        return $data;
    }

    public static function getForSelect(): array
    {
        $data = [];

        foreach (self::getAllIconGroups() as $k => $items){
            foreach ($items as $item) {
                $data[__('cms-core::site.icon.' . $k)][$item] = r2d2()->svgSymbol($item, [
                    'class' => '_svg-spritemap__icon',
                    'width' => 50,
                    'height' => 50
                ], asset(config('cms.ui.ui.svg.'.$k)));
            }
        }

        return $data;
    }

    public static function renderForAdmin($icon)
    {
        return r2d2()->svgSymbol($icon, [
            'class' => '_svg-spritemap__icon',
            'width' => 50,
            'height' => 50
        ], asset(config('cms.ui.ui.svg.adminSprite')));
    }

    public static function renderForAdminAllIcon($icon)
    {
        return r2d2()->svgSymbol($icon, [
            'class' => '_svg-spritemap__icon',
            'width' => 50,
            'height' => 50
        ], asset(config('cms.ui.ui.all_icons')));
    }
}
