<?php

namespace WezomCms\Core\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Http\FormRequest;;

/**
 * @method array indexViewData($result, array $viewData): array
 * @method array formData($obj, array $viewData): array
 * @method array createViewData($obj, array $viewData): array
 * @method array editViewData($obj, array $viewData): array
 * @method array showViewData($obj, array $viewData): array
 *
 *
 * @method afterSuccessfulStore($obj, FormRequest $request)
 * @method afterSuccessfulUpdate($obj, FormRequest $request)
 *
 * @method beforeDelete($obj, bool $force = false)
 * @method title($obj, bool $force = false)
 */
abstract class AbstractRepository
{
    protected $modelQuery;

    public function __construct()
    {
        $this->modelQuery = resolve($this->model())::query();
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    abstract protected function model();


    protected function getModelQuery() : Builder
    {
        return $this->modelQuery;
    }

    public function getByFront()
    {
        return $this->getModelQuery()
            ->published()
            ->with(['translations'])
            ->orderBy('sort')
            ->get();
    }

    public function getByFrontWithRelation(array $relation, string $sort='sort')
    {
        return $this->getModelQuery()
            ->published()
            ->with($relation)
            ->orderBy($sort)
            ->get();
    }

    public function getBySelect()
    {
        return $this->getModelQuery()
            ->published()
            ->with([
                'translations' => function ($query) {
                    $query->orderBy('name');
                }
            ])
            ->orderBy('sort')
            ->get()
            ->pluck('name', 'id')
            ->toArray();
    }

    public function getBySlugForFront($slug, array $relations = ['translations'])
    {
        return $this->getModelQuery()
            ->publishedWithSlug($slug)
            ->with($relations)
            ->firstOrFail();
    }
}
