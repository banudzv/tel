<?php

namespace WezomCms\Core\Traits;

use WezomCms\Core\Traits\Model\ImageAttachable;

trait ImageMultiUploaderAttachable
{
    use ImageAttachable;

    /**
     * @return string
     */
    abstract public function getMainColumn(): string;

    /**
     * Determines the presence of the "name" field in the database.
     *
     * @return bool
     */
    public function hasNameField()
    {
        return true;
    }

    /**
     * Determines the presence of the "alt" & "title" fields in the database.
     *
     * @return bool
     */
    public function hasAltAndTitleFields()
    {
        return true;
    }
}
