@extends('cms-ui::layouts.main')
@php
    /**
     * @var $result \WezomCms\OurTeam\Models\Employee[]|\Illuminate\Pagination\LengthAwarePaginator
     */
@endphp
@section('content')
    <div class="container">
        @widget('ui:breadcrumbs')

        <div>
            @forelse($result as $item)
                <div>
                    <img class="lozad js-import" src="{{ url('assets/images/loader.gif') }}"
                         data-lozad="{{ $item->getImageUrl() }}" alt="{{ $item->name }}">
                    <div>{{ $item->name }}</div>
                    <div>{{ $item->position }}</div>
                    @if($item->address)
                        <ul>
                            <li>{{ $item->address }}</li>
                        </ul>
                    @endif
                    @if($item->phones)
                        <ul>
                            @foreach($item->phones as $phone)
                                <li>
                                    <a href="tel:{{ str_replace(' ', '', $phone) }}"> {{ $phone }} </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                    @if($item->email)
                        <a href="mailto:{{ $item->email }}">{{ $item->email }}</a>
                    @endif
                </div>
            @empty
                <div>
                    @emptyResult
                </div>
            @endforelse
        </div>

        @if($result->hasPages())
            <div>{!! $result->links() !!}</div>
        @endif

        <div class="wysiwyg js-import" data-wrap-media data-draggable-table>
            {!! SEO::getSeoText() !!}
        </div>
    </div>
@endsection
