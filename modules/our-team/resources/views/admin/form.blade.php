<div class="row">
    <div class="col-lg-7">
        <div class="card mb-3">
            <div class="card-body">
                @langTabs
                <div class="form-group">
                    {!! Form::label($locale . '[name]', __('cms-our-team::admin.Name')) !!}
                    {!! Form::text($locale . '[name]', old($locale . '.name', $obj->translateOrNew($locale)->name)) !!}
                </div>
                <div class="form-group">
                    {!! Form::label($locale . '[position]', __('cms-our-team::admin.Position')) !!}
                    {!! Form::text($locale . '[position]', old($locale . '.position', $obj->translateOrNew($locale)->position)) !!}
                </div>
                <div class="form-group">
                    {!! Form::label($locale . '[address]', __('cms-our-team::admin.Address')) !!}
                    {!! Form::text($locale . '[address]', old($locale . '.address', $obj->translateOrNew($locale)->address)) !!}
                </div>
                <div class="form-group">
                    {!! Form::label($locale . '[experience_description]', __('cms-our-team::admin.Experience description')) !!}
                    {!! Form::textarea($locale . '[experience_description]', old($locale . '.experience_description', $obj->translateOrNew($locale)->experience_description)) !!}
                </div>
                <div class="form-group">
                    {!! Form::label($locale . '[education]', __('cms-our-team::admin.Education')) !!}
                    {!! Form::textarea($locale . '[education]', old($locale . '.education', $obj->translateOrNew($locale)->education)) !!}
                </div>
                <div class="form-group">
                    {!! Form::label($locale . '[work_description]', __('cms-our-team::admin.Work description')) !!}
                    {!! Form::textarea($locale . '[work_description]', old($locale . '.work_description', $obj->translateOrNew($locale)->work_description)) !!}
                </div>
                @endLangTabs
            </div>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="card mb-3">
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label('published', __('cms-core::admin.layout.Published')) !!}
                    {!! Form::status('published') !!}
                </div>
                <div class="form-group">
                    {!! Form::label('birthday', __('cms-our-team::admin.Birthday')) !!}
                    {!! Form::text('birthday', old('birthday', $obj->birthday ? $obj->birthday->format('d.m.Y') : null), ['class' => 'js-datepicker', 'placeholder' => __('cms-our-team::admin.Birthday')]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('for_about_page', __('cms-our-team::admin.For about page')) !!}
                    {!! Form::status('for_about_page') !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', __('cms-our-team::admin.Email')) !!}
                    {!! Form::text('email') !!}
                </div>
                <div class="form-group">
                    {!! Form::multipleInputs('phones[]', old('phones', $obj->phones), __('cms-our-team::admin.Phones')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('image', __('cms-our-team::admin.Image')) !!}
                    {!! Form::imageUploader('image', $obj, route($routeName . '.delete-image', $obj->id)) !!}
                </div>
            </div>
        </div>
    </div>
</div>
