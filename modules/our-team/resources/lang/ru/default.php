<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
        'Our team' => 'Наша команда',
        'Employees list' => 'Список сотрудников',
        'Employees' => 'Сотрудники',
        'Name' => 'ФИО',
        'Image' => 'Изображение',
        'No image' => 'Нет изображения',
        'Employees limit at page' => 'Кол-во отображаемых элементов на странице',
        'Position' => 'Должность',
        'Address' => 'Адрес',
        'Phones' => 'Телефоны',
        'Email' => 'Email',
        'Text' => 'Текстовое описание страницы',
        'Choice' => 'Выберите сотрудника',
        'Do not can delete employ' => 'Перед удалением, отвяжите сотрудника от филиала',
        'Branch' => 'Филиал',
        'Experience description' => 'Описание стажа',
        'For about page' => 'Выводить на стр. "О нас"',
        'Education' => 'Образование',
        'Work description' => 'Описание работы',
        'Birthday' => 'Дата рождение',
    ],
    TranslationSide::SITE => [
        'Our team' => 'Наша команда',
    ]
];
