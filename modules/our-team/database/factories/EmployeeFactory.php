<?php

use Faker\Generator as Faker;

$factory->define(WezomCms\OurTeam\Models\Employee::class, function (Faker $faker) {
    return [
        'published' => $faker->boolean(80),
        'name' => $faker->name,
        'email' => $faker->companyEmail,
        'position' => $faker->jobTitle,
        'address' => $faker->address,
        'phones' => [$faker->phoneNumber, $faker->phoneNumber],
    ];
});
