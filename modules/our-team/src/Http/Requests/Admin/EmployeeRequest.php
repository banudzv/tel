<?php

namespace WezomCms\OurTeam\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Traits\LocalizedRequestTrait;

class EmployeeRequest extends FormRequest
{
    use LocalizedRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->localizeRules(
            [
                'name' => 'required|string|max:255',
                'position' => 'nullable|string|max:255',
                'address' => 'nullable|string|max:255',
                'experience_description' => 'nullable|string|max:255',
                'education' => 'nullable|string',
                'work_description' => 'nullable|string',
            ],
            [
                'published' => 'required',
                'image' => 'nullable|image',
                'phones.*' => 'nullable|string|max:255|distinct|regex:/^\+?[\d\s\(\)-]+$/',
                'email' => 'nullable|email|max:255',
                'for_about_page' => 'required',
                'birthday' => 'nullable|date',
            ]
        );
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->localizeAttributes(
            [
                'name' => __('cms-our-team::admin.Name'),
                'position' => __('cms-our-team::admin.Position'),
                'address' => __('cms-our-team::admin.Address'),
                'experience_description' => __('cms-our-team::admin.Experience description'),
                'education' => __('cms-our-team::admin.Education'),
                'work_description' => __('cms-our-team::admin.Work description'),
            ],
            [
                'published' => __('cms-core::admin.layout.Published'),
                'image' => __('cms-our-team::admin.Image'),
                'phones.*' => __('cms-our-team::admin.Phone'),
                'email' => __('cms-our-team::admin.Email'),
                'for_about_page' => __('cms-our-team::admin.For about page'),
                'birthday' => __('cms-our-team::admin.Birthday'),
            ]
        );
    }
}
