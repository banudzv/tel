<?php

namespace WezomCms\Faq\Http\Controllers\Site;

use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Faq\Models\FaqGroup;
use WezomCms\Faq\Models\FaqQuestion;

class FaqController extends SiteController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $settings = settings('faq.site', []);

        $pageName = array_get($settings, 'name');

        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('faq'));

        // SEO
        $this->seo()
            ->setPageName($pageName)
            ->setTitle(array_get($settings, 'title'))
            ->setH1(array_get($settings, 'h1'))
            ->setDescription(array_get($settings, 'description'))
            ->metatags()
            ->setKeywords(array_get($settings, 'keywords'));

        // Selection
        if (config('cms.faq.faq.use_groups')) {
            $result = FaqGroup::published()
                ->with([
                    'faqQuestions' => function ($query) {
                        $query->published()
                            ->orderBy('sort')
                            ->latest('id');
                    }
                ])
                ->whereHas('faqQuestions', function ($query) {
                    $query->published();
                })
                ->orderBy('sort')
                ->latest('id')
                ->get();
        } else {
            $result = FaqQuestion::published()
                ->orderBy('sort')
                ->latest('id')
                ->get();
        }

        // Render
        return view('cms-faq::site.index', compact('result'));
    }
}
