<?php

namespace WezomCms\Faq\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Faq\Models\FaqQuestion;

class FaqRepository extends AbstractRepository
{
    protected function model()
    {
        return FaqQuestion::class;
    }

    public function getBySelect()
    {
        return $this->getModelQuery()
            ->published()
            ->with([
                'translations' => function ($query) {
                    $query->orderBy('question');
                }
            ])
            ->orderBy('sort')
            ->get()
            ->pluck('question', 'id')
            ->toArray();
    }
}
