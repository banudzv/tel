<?php

Route::namespace('WezomCms\\Faq\\Http\\Controllers\\Admin')
    ->group(function () {
        Route::adminResource('faq', 'FaqController')->settings();

        if (config('cms.faq.faq.use_groups')) {
            Route::adminResource('faq-groups', 'FaqGroupsController');
        }
    });
