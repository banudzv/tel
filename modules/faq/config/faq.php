<?php

use WezomCms\Faq\Dashboards;

return [
    'use_groups' => false,
    'dashboards' => [
        Dashboards\FaqDashboard::class,
        //Dashboards\FaqPublishedDashboard::class,
    ],
];
