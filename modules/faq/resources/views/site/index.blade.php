@extends('cms-ui::layouts.main')

@php
    /**
     * @var $result \Illuminate\Database\Eloquent\Collection|\WezomCms\Faq\Models\FaqGroup[]|\WezomCms\Faq\Models\FaqQuestion[]
     */
@endphp

@section('content')
    <div class="container">
        <h1>{{ SEO::getH1() }}</h1>

        @if($result->isNotEmpty())
            <ul class="js-import" data-accordion='{"slideTime":150}'>
                @foreach($result as $key => $group)
                    <li>
                        <div data-wstabs-ns="faq" data-wstabs-button="{{ $key }}">
                            <span>{{ $group->name }}</span>
                        </div>
                        <div data-wstabs-ns="faq" data-wstabs-block="{{ $key }}" hidden>
                            @foreach($group->faqQuestions as $item)
                                <div>{{ $item->question }}</div>
                                <div class="wysiwyg js-import" data-wrap-media
                                     data-draggable-table>{!! $item->answer !!}</div>
                            @endforeach
                        </div>
                    </li>
                @endforeach
            </ul>
        @else
            @emptyResult
        @endif
    </div>
@endsection
