<?php

namespace WezomCms\About\Widgets;

use Illuminate\Support\Collection;
use WezomCms\About\Models\Menu;
use WezomCms\About\Repositories\MenuRepository;
use WezomCms\Core\Foundation\Widgets\AbstractWidget;

class AboutMenu extends AbstractWidget
{
    /**
     * A list of models that, when changed, will clear the cache of this widget.
     *
     * @var array
     */
    public static $models = [Menu::class];

    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        $repository = \App::make(MenuRepository::class);

        /** @var Collection $menu */
        $menu = $repository->getByFront();

        if ($menu->isEmpty()) {
            return null;
        }

        return compact('menu');
    }
}
