<?php

namespace WezomCms\About;

use SidebarMenu;
use WezomCms\Core\BaseServiceProvider;
use WezomCms\Core\Contracts\PermissionsContainerInterface;
use WezomCms\Core\Contracts\SitemapXmlGeneratorInterface;
use WezomCms\Core\Traits\SidebarMenuGroupsTrait;

class AboutServiceProvider extends BaseServiceProvider
{
	use SidebarMenuGroupsTrait;

    protected $widgets = 'cms.about.about.widgets';

	/**
	 * @param  PermissionsContainerInterface  $permissions
	 */
	public function permissions(PermissionsContainerInterface $permissions)
	{
        $permissions->editSettings('about-page', __('cms-about::admin.About company'));
        $permissions->add('about-menu', __('cms-about::admin.Menu'));
        $permissions->add('about-awards', __('cms-about::admin.About company'));
        $permissions->add('about-events', __('cms-about::admin.Events'));
        $permissions->add('about-agents', __('cms-about::admin.Agents'))->withEditSettings();
        $permissions->add('about-license-groups', __('cms-about::admin.License group'));
        $permissions->add('about-licenses', __('cms-about::admin.Licenses'));
        $permissions->add('about-rule-groups', __('cms-about::admin.Rule groups'));
        $permissions->add('about-rules', __('cms-about::admin.Rules'));
        $permissions->add('insurance-rates', __('cms-about::admin.InsuranceRates'));
        $permissions->add('about-contract-groups', __('cms-about::admin.Contract groups'));
        $permissions->add('about-contracts', __('cms-about::admin.Contracts'));

        $permissions->add('about-contract-ownership-groups', __('cms-about::admin.Contract groups'));
        $permissions->add('about-contracts-ownership', __('cms-about::admin.Contracts'));

        $permissions->add('about-informations', __('cms-about::admin.Information for review'))->withEditSettings();
        $permissions->add('about-details', __('cms-about::admin.Details'));
        $permissions->add('about-detail-files', __('cms-about::admin.Detail file'));
	}

    public function adminMenu()
    {
        $about = SidebarMenu::add(__('cms-about::admin.About company'), route('admin.about-awards.index'))
            ->data('icon', 'fa-building')
//            ->data('permission', 'awards.view')
            ->data('position', 20)
            ->nickname('about');

        $about_us = $about->add(__('cms-about::admin.About us'), route('admin.about-awards.index'))
            ->data('icon', 'fa-users')
            ->data('position', 1);

        $about_us->add(__('cms-about::admin.Awards'), route('admin.about-awards.index'))
            ->data('icon', 'fa-list')
            ->data('position', 1);
        $about_us->add(__('cms-about::admin.Events'), route('admin.about-events.index'))
            ->data('icon', 'fa-list')
            ->data('position', 2);

        // Правила страхования
        $about_rules = $about->add(__('cms-about::admin.Rules insurance'), route('admin.about-rule-groups.index'))
            ->data('icon', 'fa-list')
            ->data('position', 2);
        $about_rules->add(__('cms-about::admin.Rule groups'), route('admin.about-rule-groups.index'))
            ->data('icon', 'fa-th-large')
            ->data('position', 1);
        $about_rules->add(__('cms-about::admin.Rules'), route('admin.about-rules.index'))
            ->data('icon', 'fa-list')
            ->data('position', 2);

        // Договоры оферты
        $about_contracts = $about->add(__('cms-about::admin.Contract offers'), route('admin.about-contract-groups.index'))
            ->data('icon', 'fa-handshake-o')
            ->data('position', 3);
        $about_contracts->add(__('cms-about::admin.Contract groups'), route('admin.about-contract-groups.index'))
            ->data('icon', 'fa-th-large')
            ->data('position', 1);
        $about_contracts->add(__('cms-about::admin.Contracts'), route('admin.about-contracts.index'))
            ->data('icon', 'fa-list')
            ->data('position', 2);
        // Структура собственности
        $about_contracts_ownership = $about->add('Структура собственности', route('admin.about-contract-ownership-groups.index'))
            ->data('icon', 'fa-handshake-o')
            ->data('position', 3);
        $about_contracts_ownership->add(__('cms-about::admin.Contract groups'), route('admin.about-contract-ownership-groups.index'))
            ->data('icon', 'fa-th-large')
            ->data('position', 1);
        $about_contracts_ownership->add(__('cms-about::admin.Contracts'), route('admin.about-contracts-ownership.index'))
            ->data('icon', 'fa-list')
            ->data('position', 2);

        // Детально о компании
        $about_details = $about->add(__('cms-about::admin.Details about company'), route('admin.about-detail-files.index'))
            ->data('icon', 'fa-list')
            ->data('position', 3);
        $about_details->add(__('cms-about::admin.Detail file'), route('admin.about-detail-files.index'))
            ->data('icon', 'fa-file')
            ->data('position', 1);
        $about_details->add(__('cms-about::admin.Details'), route('admin.about-details.index'))
            ->data('icon', 'fa-th-large')
            ->data('position', 2);

        // Лицензии
        $about_details = $about->add(__('cms-about::admin.Licenses'), route('admin.about-licenses.index'))
            ->data('icon', 'fa-file-text-o')
            ->data('position', 3);
        $about_details->add(__('cms-about::admin.Licenses'), route('admin.about-licenses.index'))
            ->data('icon', 'fa-file-text-o')
            ->data('position', 1);
        $about_details->add(__('cms-about::admin.License groups'), route('admin.about-license-groups.index'))
            ->data('icon', 'fa-th-large')
            ->data('position', 2);

        $about->add(__('cms-about::admin.Agents'), route('admin.about-agents.index'))
            ->data('icon', 'fa-address-book-o')
            ->data('position', 4);
        $about->add(__('cms-about::admin.Information for review'), route('admin.about-informations.index'))
            ->data('icon', 'fa-file-image-o')
            ->data('position', 6);
        $about->add(__('cms-about::admin.About company'), route('admin.about-page.settings'))
            ->data('permission', 'about-page.edit-settings')
            ->data('icon', 'fa-user-secret')
            ->data('position', 7);
        $about->add(__('cms-about::admin.InsuranceRates'), route('admin.insurance-rates.index'))
            ->data('icon', 'fa-list')
            ->data('position', 8);
        $about->add(__('cms-about::admin.Menu'), route('admin.about-menu.index'))
            ->data('icon', 'fa-map-signs')
            ->data('position', 9);

    }

    /**
     * @return array
     */
    public function sitemap()
    {
        return [
            [
                'id' => 'about-us',
                'name' => settings('about.about_us.title'),
                'url' => route('about.about-us'),
            ],
            [
                'id' => 'about-information',
                'name' => settings('information.site.title'),
                'url' => route('about.information'),
            ],
            [
                'id' => 'about-agents',
                'name' => settings('agent.site.title'),
                'url' => route('about.agents'),
            ],
            [
                'id' => 'about-details',
                'name' => settings('about.about_details.title'),
                'url' => route('about.details'),
            ],
            [
                'id' => 'about-rule',
                'name' => settings('about.about_rule.title'),
                'url' => route('about.rule'),
            ],
            [
                'id' => 'about-license',
                'name' => settings('about.about_license.title'),
                'url' => route('about.licenses'),
            ],
            [
                'id' => 'about-contracts',
                'name' => settings('about.about_contracts.title'),
                'url' => route('about.contracts'),
            ],
            [
                'id' => 'insurance-rate',
                'name' => settings('about.insurance-rate.title'),
                'url' => route('about.insurance-rate'),
            ],
        ];
    }

    /**
     * @param  SitemapXmlGeneratorInterface  $sitemap
     */
    public function sitemapXml(SitemapXmlGeneratorInterface $sitemap)
    {
        $sitemap->addLocalizedRoute('about.about-us');
        $sitemap->addLocalizedRoute('about.information');
        $sitemap->addLocalizedRoute('about.agents');
        $sitemap->addLocalizedRoute('about.details');
        $sitemap->addLocalizedRoute('about.rule');
        $sitemap->addLocalizedRoute('about.licenses');
        $sitemap->addLocalizedRoute('about.contracts');
        $sitemap->addLocalizedRoute('about.insurance-rate');
    }
}
