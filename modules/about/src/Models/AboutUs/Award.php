<?php

namespace WezomCms\About\Models\AboutUs;

use Illuminate\Database\Eloquent\Model;
use LaravelLocalization;
use Request;
use WezomCms\Core\ExtendPackage\Translatable;
use WezomCms\Core\Traits\Model\ImageAttachable;
use WezomCms\Core\Traits\Model\PublishedTrait;

/**
 *
 * @property int $id
 * @property bool $published
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Menu\Models\Menu withTranslation()
 * @mixin \Eloquent
 * @mixin AwardTranslation
 */
class Award extends Model
{
    use Translatable;
    use PublishedTrait;
    use ImageAttachable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'about_awards';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['published'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'published' => 'bool',
    ];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected $translatedAttributes = ['name', 'text'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * @return array
     */
    public function imageSettings(): array
    {
        return ['image' => 'cms.about.about.images.awards'];
    }

    public function getImage()
    {
        return url($this->getImageUrl(null, 'image'));
    }
}
