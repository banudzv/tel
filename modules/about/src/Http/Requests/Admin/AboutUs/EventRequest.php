<?php

namespace WezomCms\About\Http\Requests\Admin\AboutUs;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Http\Requests\ChangeStatus\RequiredIfMessageTrait;
use WezomCms\Core\Traits\LocalizedRequestTrait;

class EventRequest extends FormRequest
{
    use LocalizedRequestTrait;
    use RequiredIfMessageTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->localizeRules(
            [
                'name' => 'required|string|max:255',
                'text' => 'required|string',
            ],
            [
                'published' => 'nullable',
                'year' => 'required|unique:about_events,id,' . $this->route('about_events'),
            ]
        );
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->localizeAttributes(
            [
                'name' => __('cms-core::admin.layout.Name'),
                'text' => __('cms-core::admin.layout.Text'),
            ],
            [
                'published' => __('cms-core::admin.layout.Published'),
                'year' => __('cms-about::admin.Year'),
            ]
        );
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'year.unique' => __('cms-about::admin.model with this year exist'),
        ];
    }
}

