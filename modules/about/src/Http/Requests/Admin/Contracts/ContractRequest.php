<?php

namespace WezomCms\About\Http\Requests\Admin\Contracts;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Http\Requests\ChangeStatus\RequiredIfMessageTrait;
use WezomCms\Core\Traits\LocalizedRequestTrait;

class ContractRequest extends FormRequest
{
    use LocalizedRequestTrait;
    use RequiredIfMessageTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->localizeRules(
            [
                'name' => 'required|string|max:1000',
            ],
            [
                'published' => 'nullable',
                'sort' => 'nullable|integer',
                'contract_group_id' => 'required',
                'file' => ['nullable', 'file', 'min:3', 'max:15000'],
            ]
        );
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->localizeAttributes(
            [
                'name' => __('cms-core::admin.layout.Name'),
            ],
            [
                'sort' => __('cms-core::admin.layout.Position'),
                'published' => __('cms-core::admin.layout.Published'),
                'file' => __('cms-about::admin.File'),
            ]
        );
    }
}


