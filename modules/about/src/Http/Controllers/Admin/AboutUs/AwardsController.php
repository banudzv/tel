<?php

namespace WezomCms\About\Http\Controllers\Admin\AboutUs;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\About\Http\Requests\Admin\AboutUs\AwardRequest;
use WezomCms\About\Models\AboutUs\Award;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;

class AwardsController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Award::class;

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-about::admin.awards';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.about-awards';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = AwardRequest::class;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-about::admin.Awards');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }
}

