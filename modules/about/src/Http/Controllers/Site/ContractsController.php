<?php

namespace WezomCms\About\Http\Controllers\Site;

use WezomCms\About\Repositories\ContractGroupRepository;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Core\Traits\ImageFromSettings;

class ContractsController extends SiteController
{
    use ImageFromSettings;

    private $settings;

    private ContractGroupRepository $contractGroupRepository;

    public function __construct(ContractGroupRepository $contractGroupRepository)
    {
        $this->contractGroupRepository = $contractGroupRepository;
        $this->settings = settings('about.site', []);
    }

    public function index()
    {
        $localSetting = settings('about.about_contracts', []);

        $pageName = array_get($this->settings, 'name');
        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('about.about-us'));
        $this->seo()
            ->setTitle(array_get($localSetting, 'title'))
            ->setH1(array_get($this->settings, 'h1'))
            ->setPageName($pageName)
            ->setDescription(array_get($localSetting, 'description'))
            ->metatags()
            ->setKeywords(array_get($localSetting, 'keywords'));

        $groups = $this->contractGroupRepository->getByFront();

        return view('cms-about::site.contracts', [
            'banner' => $this->getBannerUrl('about.page'),
            'description' => array_get($this->settings, 'description'),
            'groups' => $groups
        ]);
    }
}
