<?php

namespace WezomCms\About\Http\Controllers\Site;

use WezomCms\About\Repositories\InformationRepository;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Core\Traits\ImageFromSettings;

class InformationController extends SiteController
{
    use ImageFromSettings;

    private $settings;

    private InformationRepository $informationRepository;

    public function __construct(InformationRepository $informationRepository)
    {
        $this->settings = settings('about.site', []);

        $this->informationRepository = $informationRepository;
    }

    public function index()
    {
        $localSetting = settings('information.site', []);

        $pageName = array_get($this->settings, 'name');
        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('about.about-us'));
        $this->seo()
            ->setTitle(array_get($localSetting, 'title'))
            ->setH1(array_get($this->settings, 'h1'))
            ->setPageName($pageName)
            ->setDescription(array_get($localSetting, 'description'))
            ->metatags()
            ->setKeywords(array_get($localSetting, 'keywords'));

        $informations = $this->informationRepository->getByFront();

        return view('cms-about::site.information', [
            'banner' => $this->getBannerUrl('about.page'),
            'description' => array_get($this->settings, 'description'),
            'informations' => $informations
        ]);
    }
}
