<?php

namespace WezomCms\About\Http\Controllers\Site;

use WezomCms\About\Repositories\AwardsRepository;
use WezomCms\About\Repositories\EventRepository;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Core\Traits\ImageFromSettings;
use WezomCms\OurTeam\Repositories\EmployeeRepository;

class AboutController extends SiteController
{
    use ImageFromSettings;

    private $settings;

    private AwardsRepository $awardsRepository;
    private EventRepository $eventRepository;
    private EmployeeRepository $employeeRepository;

    public function __construct(
        AwardsRepository $awardsRepository,
        EventRepository $eventRepository,
        EmployeeRepository $employeeRepository
    )
    {
        $this->settings = settings('about.site', []);
        $this->awardsRepository = $awardsRepository;
        $this->eventRepository = $eventRepository;
        $this->employeeRepository = $employeeRepository;
    }

    public function index()
    {
        $localSetting = settings('about.about_us', []);
        $text = array_get($localSetting, 'text');

        $pageName = array_get($this->settings, 'name');
        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('about.about-us'));
        $this->seo()
            ->setTitle(array_get($localSetting, 'title'))
            ->setH1(array_get($this->settings, 'h1'))
            ->setPageName($pageName)
            ->setDescription(array_get($localSetting, 'description'))
            ->metatags()
            ->setKeywords(array_get($localSetting, 'keywords'));

        $awards = $this->awardsRepository->getByFront();
        $events = $this->eventRepository->getByFront();
        $employees = $this->employeeRepository->getByAbout();

        return view('cms-about::site.about-us', [
            'banner' => $this->getBannerUrl('about.page'),
            'description' => array_get($this->settings, 'description'),
            'awards' => $awards,
            'events' => $events,
            'employees' => $employees,
            'text' => $text
        ]);
    }
}
