<?php

namespace WezomCms\About\Repositories;

use WezomCms\About\Models\Rules\Group;
use WezomCms\Core\Repositories\AbstractRepository;

class RuleGroupRepository extends AbstractRepository
{
    protected function model()
    {
        return Group::class;
    }

    public function getByFront()
    {
        return $this->getModelQuery()
            ->published()
            ->with(['rules'])
            ->orderBy('sort')
            ->get();
    }
}
