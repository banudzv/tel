<?php

namespace WezomCms\About\Repositories;

use WezomCms\About\Models\AboutUs\Award;
use WezomCms\About\Models\Information;
use WezomCms\Core\Repositories\AbstractRepository;

class InformationRepository extends AbstractRepository
{
    protected function model()
    {
        return Information::class;
    }
}
