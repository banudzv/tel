<?php

namespace WezomCms\About\Repositories;

use WezomCms\About\Models\License\Group;
use WezomCms\Core\Repositories\AbstractRepository;

class LicenseGroupRepository extends AbstractRepository
{
    protected function model()
    {
        return Group::class;
    }

    public function getByFront()
    {
        return $this->getModelQuery()
            ->published()
            ->with(['licenses'])
            ->orderBy('sort')
            ->get();
    }
}

