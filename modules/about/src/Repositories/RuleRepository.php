<?php

namespace WezomCms\About\Repositories;

use WezomCms\About\Models\AboutUs\Event;
use WezomCms\About\Models\Rules\Rule;
use WezomCms\Core\Repositories\AbstractRepository;

class RuleRepository extends AbstractRepository
{
    protected function model()
    {
        return Rule::class;
    }
}
