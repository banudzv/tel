<?php

namespace WezomCms\About\Repositories;

use WezomCms\About\Models\AboutUs\Award;
use WezomCms\Core\Repositories\AbstractRepository;

class AwardsRepository extends AbstractRepository
{
    protected function model()
    {
        return Award::class;
    }
}
