@extends('cms-core::admin.crud.index')

@php
    /**
     * @var $result \WezomCms\About\Models\License[]
     */
@endphp

@section('content')

    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th class="sortable-column"></th>
                <th width="1%">@massControl($routeName)</th>
                <th>@lang('cms-core::admin.layout.Name')</th>
                <th>@lang('cms-about::admin.License groups')</th>
                <th>@lang('cms-reports::admin.File')</th>
                <th width="1%" class="text-center">@lang('cms-core::admin.layout.Manage')</th>
            </tr>
            </thead>
            <tbody class="js-sortable"
                   data-params="{{ json_encode(['model' => encrypt($model)]) }}">
            @foreach($result as $obj)
                <tr data-id="{{ $obj->id }}">
                    <td>
                        <div class="js-sortable-handle sortable-handle">
                            <i class="fa fa-arrows"></i>
                        </div>
                    </td>
                    <td>@massCheck($obj)</td>
                    <td>
                        @editResource($obj)
                    </td>
                    <td>
                        @editResource(['obj'=> $obj->group, 'text' => $obj->group->name, 'ability' => 'about-license-groups.edit', 'target' => '_blank'])
                    </td>
                    <td>
                        @if($obj->fileExists())
                            <a href="{{ url($obj->getFileUrl()) }}" target="_blank" >
                                {{ $obj->file }}
                            </a>
                        @else
                            <span class="d-block p-2 text-info">@lang('cms-callbacks::admin.No file')</span>
                        @endif
                    </td>
                    <td>
                        <div class="btn-group list-control-buttons" role="group">
                            @smallStatus($obj)
                            @editResource($obj, false)
                            @deleteResource($obj)
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
