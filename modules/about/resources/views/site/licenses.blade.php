@extends('cms-ui::layouts.main')

@php
    /**
     * @var $banner string|null
     * @var $description string|null
     * @var $groups \WezomCms\About\Models\License\Group[]
     * @var $licenses \WezomCms\About\Models\License\License[]
     */

@endphp

@section('content')
    @widget('ui:heading-section', [
        'bannerImage' => $banner,
        'title' => SEO::getH1(),
        'text' => $description,
        'classes' => 'heading-section--contacts'
    ])

    @widget('about:menu')

    <div class="section section--information section--bg-grey">
        <div class="container">
            <div class="_grid _justify-center">
                <div class="_cell _cell--12 _lg:cell--10">
                    <div class="text text--size-lg text--color-grey _mb-xl">
                        @lang('cms-about::site.about-license.title')
                    </div>
                    @foreach($groups as $group)
                        <div class="_mb-xl">
                            <div class="title title--size-h4 _mb-xl">{{ $group->name }}</div>
                            @foreach($group->licenses as $license)
                                @include('cms-about::site.partials.row-file', ['item' => $license])
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
