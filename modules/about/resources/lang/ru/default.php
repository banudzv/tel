<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
        'About company' => 'О компании',
        'About us' => 'О нас',
        'Awards' => 'Награды',
        'Award' => 'Награда',
        'Events' => 'События',
        'Event' => 'Событие',
        'Agent' => 'Агент',
        'Agent name' => 'ФИО агента',
        'Agent text' => 'Контакты агента',
        'Agents' => 'Агенты',
        'Licenses' => 'Лицензии',
        'License' => 'Лицензия',
        'Rule' => 'Правило',
        'Rules' => 'Правила',
        'Rule groups' => 'Группы правил',
        'Rule group' => 'Группа правил',
        'Rules insurance' => 'Правила страхования',
        'InsuranceRates' => 'Страховые тарифы',
        'Contract' => 'Контракт',
        'Contracts' => 'Контракты',
        'Contract group' => 'Группа контракта',
        'Contract groups' => 'Группы контрактов',
        'Contract offers' => 'Договоры публичной оферты',
        'Information for review' => 'Информация для ознакомления',
        'Information' => 'Информация',
        'License groups' => 'Группа для лицензий',
        'Details' => 'Детали',
        'Detail file' => 'Файлы для деталей',
        'Details about company' => 'Детально о ТЕКОМ',
        'Menu' => 'Внутренее меню',
        'Image' => 'Картинка',
        'Link' => 'Ссылка',
        'Year' => 'Год',
        'File' => 'Файл',
        'model with this year exist' => 'Запись с данным годом уже существует',
        'image-size' => 'Рекомендуемый размер :width x :height px',
        'Text fo description' => 'Текст на странице "О нас"',
        'tab about us page' => 'Страница "О нас"',
        'tab about details page' => 'Страница "Детали"',
        'tab about rule page' => 'Страница "Правила"',
        'tab about contracts page' => 'Страница "Контракты"',
        'tab about license page' => 'Страница "Лицензии"',
        'tab about insurance rate page' => 'Страница "Страховые тарифы"',
    ],
    TranslationSide::SITE => [
        'about-us' => [
            'our awards' => 'Наши награды',
            'our mission' => 'Наша миссия',
            'chronology of the company' => 'Хронология компании',
            'title for desc' => 'СК Теком - это уверенность в квадрате',
            'employee title' => 'Топ менеджмент',
            'employee description' => 'Мы благодорим всех, кто уже доверил нам свой покой и благосостояние, и приложим все усилия, чтобы оправдать Ваши ожидания и прийти на помощь в трудную минуту.',
            'our task' => [
                'title' => 'Наша задача',
                'sub-title' => 'Подзаголовок',
                'description' => 'Описание',
            ],
            'our target' => [
                'title' => 'Наша цель',
                'sub-title' => 'Подзаголовок',
                'description' => 'Описание',
            ]
        ],
        'about-information' => [
            'title' => 'В соответсвии с требованиями с Законом Украины бла-бла-бла'
        ],
        'about-rule' => [
            'title' => 'Здесь находяться правила страхования бла-бла-бла'
        ],
        'insurance-rate' => [
            'title' => 'Вы можете ознакомиться с нашими страховыми тарифами здесь'
        ],
        'about-license' => [
            'title' => 'Здесь находяться лицензии бла-бла-бла'
        ],
        'about-contracts' => [
            'title' => 'Здесь находяться договоры публичной оферты бла-бла-бла'
        ],
        'about-ownership' => [
            'title' => 'Структура собственности'
        ],
        'about-agent' => [
            'title' => 'Для нас каждый страховой агент это - бла-бла-бла',
            'name' => 'Наименование',
            'contact information' => 'Контактная информация'
        ]
    ],
];
