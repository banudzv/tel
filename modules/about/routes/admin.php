<?php

use WezomCms\About\Http\Controllers\Admin\AboutUs;
use WezomCms\About\Http\Controllers\Admin\Rules;
use WezomCms\About\Http\Controllers\Admin\InsuranceRates;
use WezomCms\About\Http\Controllers\Admin\Contracts;
use WezomCms\About\Http\Controllers\Admin\Ownership;
use WezomCms\About\Http\Controllers\Admin\Details;
use WezomCms\About\Http\Controllers\Admin\License;
use WezomCms\About\Http\Controllers\Admin;

Route::namespace('WezomCms\\About\\Http\\Controllers\\Admin')
    ->group(function () {
        Route::settings('about-page', 'AboutController');
    });

Route::adminResource('about-menu', Admin\MenuController::class);
Route::adminResource('about-awards', AboutUs\AwardsController::class);
Route::adminResource('about-events', AboutUs\EventController::class);
Route::adminResource('about-agents', Admin\AgentController::class)->settings();
Route::adminResource('about-licenses', License\LicenseController::class);
Route::adminResource('about-license-groups', License\GroupController::class);
Route::adminResource('about-rule-groups', Rules\GroupController::class);
Route::adminResource('about-rules', Rules\RuleController::class);
Route::adminResource('insurance-rates', InsuranceRates\InsuranceRateController::class);
Route::adminResource('about-contract-groups', Contracts\GroupController::class);
Route::adminResource('about-contracts', Contracts\ContractsController::class);

Route::adminResource('about-contract-ownership-groups', Ownership\GroupController::class);
Route::adminResource('about-contracts-ownership', Ownership\ContractsController::class);

Route::adminResource('about-informations', Admin\InformationController::class)->settings();
Route::adminResource('about-details', Details\DetailController::class);
Route::adminResource('about-detail-files', Details\DetailFileController::class);
