<?php

namespace WezomCms\Cli\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class CmsResourceMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:cms-resource
                            {name : Resource name}
                            {module : Module name}
                            {--m|multilingual : Model is multilingual}
                            {--f|filter : Need generate ModelFilter}
                            {--s|sort : Model must be sortable}
                            {--dir=modules : Directory name with modules}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new cms resource (CRUD)';

    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * Create a new command instance.
     *
     * @param  Filesystem  $files
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $module = $this->argument('module');
        $destination = app()->basePath($this->option('dir'));

        if (!$this->checkModule($module, $destination)) {
            return;
        }

        $modulePath = $destination . '/' . $module;

        $multilingual = (bool)$this->option('multilingual');
        $filter = (bool)$this->option('filter');
        $sort = (bool)$this->option('sort');

        // generate model
        $modelClass = $this->generateModel($modulePath, $multilingual, $filter);
        if ($multilingual) {
            $this->generateModelTranslation($modulePath, $modelClass);
        }
        if ($filter) {
            $this->generateModelFilter($modulePath, $modelClass);
        }
        $this->generateFactory($modulePath, $modelClass);

        // generate http
        $formRequestClass = $this->generateAdminRequest($modulePath, $modelClass, $multilingual);
        $this->generateAdminController($modulePath, $modelClass, $formRequestClass, $sort);
        $this->generateSiteController($modulePath, $modelClass);

        // generate views
        $this->generateAdminViews($modulePath, $modelClass, $multilingual, $sort);
        $this->generateSiteViews($modulePath);
    }

    /**
     * @param  string  $modulePath
     * @param  bool  $multilingual
     * @param  bool  $filter
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function generateModel(string $modulePath, bool $multilingual, bool $filter): string
    {
        $namespace = $this->ns('Models');
        $name = Str::singular(Str::studly($this->argument('name')));

        $dir = $modulePath . '/src/Models';

        $stub = 'model';
        if ($multilingual) {
            $stub .= '-translatable';
        }
        if ($filter) {
            $stub .= '-filterable';
        }

        $this->generate($dir, $name, 'model/' . $stub, ['DummyNamespace' => $namespace, 'DummyClass' => $name]);

        return $name;
    }

    /**
     * @param  string  $modulePath
     * @param  string  $modelClass
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function generateModelTranslation(string $modulePath, string $modelClass)
    {
        $name = $modelClass . 'Translation';

        $this->generate(
            $modulePath . '/src/Models',
            $name,
            'model/model-translation',
            [
                'DummyNamespace' => $this->ns('Models'),
                'DummyClass' => $name,
            ]
        );
    }

    /**
     * @param  string  $modulePath
     * @param  string  $modelClass
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function generateModelFilter(string $modulePath, string $modelClass)
    {
        $name = $modelClass . 'Filter';

        $this->generate(
            $modulePath . '/src/ModelFilters',
            $name,
            'model/model-filter',
            [
                'DummyNamespace' => $this->ns('ModelFilters'),
                'NamespacedDummyModel' => $this->ns('Models', $modelClass),
                'DummyModel' => $modelClass,
                'DummyClass' => $name,
            ]
        );
    }

    /**
     * @param  string  $modulePath
     * @param  string  $modelClass
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function generateFactory(string $modulePath, string $modelClass)
    {
        $this->generate(
            $modulePath . '/database/factories',
            $modelClass . 'Factory',
            'model/factory',
            [
                'NamespacedDummyModel' => $this->ns('Models', $modelClass),
                'DummyModel' => $modelClass,
            ]
        );
    }

    /**
     * @param  string  $modulePath
     * @param  string  $modelClass
     * @param  bool  $multilingual
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function generateAdminRequest(string $modulePath, string $modelClass, bool $multilingual): string
    {
        $name = $modelClass . 'Request';

        $stub = 'http/admin-request';
        if ($multilingual) {
            $stub .= '-localized';
        }

        $this->generate(
            $modulePath . '/src/Http/Requests/Admin',
            $name,
            $stub,
            [
                'DummyNamespace' => $this->ns('Http', 'Requests', 'Admin'),
                'DummyClass' => $name,
            ]
        );

        return $name;
    }

    /**
     * @param  string  $modulePath
     * @param  string  $modelClass
     * @param  string  $formRequestClass
     * @param  bool  $sort
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function generateAdminController(
        string $modulePath,
        string $modelClass,
        string $formRequestClass,
        bool $sort
    ) {
        $name = $modelClass . 'Controller';

        $stub = 'http/admin-controller';
        if ($sort) {
            $stub .= '-sortable';
        }

        $this->generate(
            $modulePath . '/src/Http/Controllers/Admin',
            $name,
            $stub,
            [
                'DummyNamespace' => $this->ns('Http', 'Controllers', 'Admin'),
                'NamespacedDummyFormRequest' => $this->ns('Http', 'Requests', 'Admin', $formRequestClass),
                'NamespacedDummyModel' => $this->ns('Models', $modelClass),
                'DummyClass' => $name,
                'DummyModel' => $modelClass,
                'DummyViewPath' => "cms-{$this->argument('module')}::admin",
                'DummyRouteName' => 'admin.' . $this->argument('name'),
                'DummyFormRequest' => $formRequestClass,
            ]
        );
    }

    /**
     * @param  string  $modulePath
     * @param  string  $modelClass
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function generateSiteController(string $modulePath, string $modelClass)
    {
        $name = $modelClass . 'Controller';

        $this->generate(
            $modulePath . '/src/Http/Controllers/Site',
            $name,
            'http/site-controller',
            [
                'DummyNamespace' => $this->ns('Htt', 'Controllers', 'Site'),
                'NamespacedDummyModel' => $this->ns('Models', $modelClass),
                'DummyClass' => $name,
            ]
        );
    }

    /**
     * @param  string  $modulePath
     * @param  string  $modelClass
     * @param  bool  $sort
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function generateAdminViews(
        string $modulePath,
        string $modelClass,
        bool $multilingual,
        bool $sort
    ) {
        $dir = $modulePath . '/resources/views/admin';

        // index
        $stub = 'views/admin/index';
        if ($sort) {
            $stub .= '-sortable';
        }

        $this->generate(
            $dir,
            'index.blade',
            $stub,
            ['NamespacedDummyModel' => $this->ns('Models', $modelClass)]
        );

        $this->generate(
            $dir,
            'form.blade',
            'views/admin/form' . ($multilingual ? '-translatable' : ''),
            ['NamespacedDummyModel' => $this->ns('Models', $modelClass)]
        );
    }

    /**
     * @param  string  $modulePath
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function generateSiteViews(string $modulePath)
    {
        $this->generate($modulePath . '/resources/views/site', 'index.blade', 'views/site/index');
    }

    /**e
     * @param  string  $dir
     * @param  string  $name
     * @param  string  $stub
     * @param  array  $replace
     * @return bool
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function generate(string $dir, string $name, string $stub, array $replace = []): bool
    {
        $file = "{$dir}/{$name}.php";
        if (!$this->checkExistPath($file)) {
            return false;
        }

        if (!empty($replace)) {
            $content = str_replace(
                array_keys($replace),
                array_values($replace),
                $this->files->get($this->stubs($stub))
            );
        } else {
            $content = $this->files->get($this->stubs($stub));
        }

        $this->prepareDestination($dir);

        $this->touch($file);

        $this->files->put($file, $content);

        return true;
    }

    /**
     * @param  string  $module
     * @param  string  $destination
     * @return bool
     */
    protected function checkModule(string $module, string $destination): bool
    {
        if (file_exists($destination . '/' . $module)) {
            return true;
        }

        if ($this->confirm("Module [{$module}] doesnt exists by path {$destination}. Create new module?", true)) {
            $this->call('make:module', ['module' => $module, '--dir' => $this->option('dir')]);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param  string  $path
     * @return bool
     */
    protected function checkExistPath(string $path): bool
    {
        if (!file_exists($path)) {
            return true;
        }

        return $this->confirm("File [{$path}] already exists. Override?", false);
    }

    /**
     * @param  string  $destination
     */
    protected function prepareDestination(string $destination)
    {
        if (!$this->files->isDirectory($destination)) {
            $this->files->makeDirectory($destination);
        }
    }

    /**
     * @param  string|null  $stub
     * @return string
     */
    protected function stubs(?string $stub = null): string
    {
        return __DIR__ . '/../../stubs/cms-resource/' . ltrim($stub, '/') . '.stub';
    }

    /**
     * Create empty file if doesnt exists.
     *
     * @param  string  $file
     */
    protected function touch(string $file)
    {
        if (!is_file($file)) {
            touch($file);
        }
    }

    /**
     * @param  mixed  $parts
     * @return string
     */
    protected function ns(...$parts)
    {
        return implode('\\', array_filter(array_merge(
            [
            'WezomCms',
            Str::studly($this->argument('module')),
            ],
            $parts
        )));
    }
}
