<?php

Route::namespace('WezomCms\\Services\\Http\\Controllers\\Site')
    ->group(function () {

        Route::get('services-group/{slug}', 'ServiceGroupsController@innerBase')
            ->name('service-group-base.inner');

        Route::get('services-group/{baseSlug}/{slug}', 'ServiceGroupsController@inner')
            ->name('service-group.inner');

        Route::get('services/{slug}', 'ServicesController@inner')->name('services.inner');

        Route::get('services', 'ServicesController@index')->name('services');

        Route::get('services-group-ajax/more', 'ServiceGroupsController@ajaxMoreServices')->name('service-group.more');
//
//
//        if (config('cms.services.services.use_groups')) {
//            Route::get('services-group/{slug}', 'ServiceGroupsController@inner')->name('service-groups.inner');
//        }
    });
