{{--@dd(\WezomCms\Core\Foundation\Icon::getForSelect())--}}

<div class="row">
    <div class="col-lg-7">
        <div class="card mb-3">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5 class="py-2"><strong>@lang('cms-core::admin.layout.Main data')</strong></h5>
            </div>
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label('name', __('cms-services::admin.risk.Group name')) !!}
                    {!! Form::text('name', old('name', $obj->name)) !!}
                </div>
            </div>
        </div>
    </div>
</div>

