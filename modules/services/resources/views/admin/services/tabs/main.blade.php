@langTabs
    <div class="form-group">
        {!! Form::label($locale . '[name]', __('cms-services::admin.Name')) !!}
        {!! Form::text($locale . '[name]', old($locale . '.name', $obj->translateOrNew($locale)->name)) !!}
    </div>
    <div class="form-group">
        {!! Form::label($locale . '[slug]', __('cms-core::admin.layout.Slug')) !!}
        {!! Form::slugInput($locale . '[slug]', old($locale . '.slug', $obj->translateOrNew($locale)->slug), ['source' => 'input[name="' . $locale . '[name]"']) !!}
    </div>
    <div class="form-group">
        {!! Form::label($locale . '[sub_title]', __('cms-services::admin.Sub title')) !!}
        {!! Form::text($locale . '[sub_title]', old($locale . '.sub_title', $obj->translateOrNew($locale)->sub_title)) !!}
    </div>
    <div class="form-group">
        {!! Form::label($locale . '[text]', __('cms-services::admin.Text')) !!}
        {!! Form::textarea($locale . '[text]', old($locale . '.text', $obj->translateOrNew($locale)->text), ['class' => 'js-wysiwyg', 'data-lang' => $locale]) !!}
    </div>
    <div class="form-group">
        {!! Form::label($locale . '[text_for_document]', __('cms-services::admin.Text for document')) !!}
        {!! Form::textarea($locale . '[text_for_document]', old($locale . '.text_for_document', $obj->translateOrNew($locale)->text_for_document), ['class' => 'js-wysiwyg', 'data-lang' => $locale]) !!}
    </div>
    <div class="form-group">
        {!! Form::label($locale . '[text_for_relation]', __('cms-services::admin.Text for relation')) !!}
        {!! Form::textarea($locale . '[text_for_relation]', old($locale . '.text_for_relation', $obj->translateOrNew($locale)->text_for_relation)) !!}
    </div>
    <div class="form-group">
        {!! Form::label($locale . '[parental_case_name]', __('cms-services::admin.Parental case name')) !!}
        {!! Form::text($locale . '[parental_case_name]', old($locale . '.parental_case_name', $obj->translateOrNew($locale)->parental_case_name)) !!}
    </div>
@endLangTabs
