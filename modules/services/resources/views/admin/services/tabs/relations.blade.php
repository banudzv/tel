
<div class="form-group">
    {!! Form::label('risks[]', __('cms-services::admin.Risks')) !!}
    <div class="input-group">
        {!! Form::select('risks[]', $risks, old('risks', $selectedRisks), ['multiple' => 'multiple', 'class' => 'js-select2']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('groups_relation[]', __('cms-services::admin.Groups relation')) !!}
    <div class="input-group">
        {!! Form::select('groups_relation[]', $groupsRelation, old('groups_relation', $selectedGroupsRelation), ['multiple' => 'multiple', 'class' => 'js-select2']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('services[]', __('cms-services::admin.Services relation')) !!}
    <div class="input-group">
        {!! Form::select('services[]', $services, old('services', $selectedServices), ['multiple' => 'multiple', 'class' => 'js-select2']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('faqs[]', __('cms-faq::admin.Questions')) !!}
    <div class="input-group">
        {!! Form::select('faqs[]', $faqs, old('faqs', $selectedFaqs), ['multiple' => 'multiple', 'class' => 'js-select2']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('additionals[]', __('cms-services::admin.Service additional')) !!}
    <div class="input-group">
        {!! Form::select('additionals[]', $additionals, old('additionals', $selectedAdditionals), ['multiple' => 'multiple', 'class' => 'js-select2']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('steps[]', __('cms-services::admin.Service step')) !!}
    <div class="input-group">
        {!! Form::select('steps[]', $steps, old('steps', $selectedSteps), ['multiple' => 'multiple', 'class' => 'js-select2']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('documents[]', __('cms-services::admin.Service document')) !!}
    <div class="input-group">
        {!! Form::select('documents[]', $documents, old('documents', $selectedDocuments), ['multiple' => 'multiple', 'class' => 'js-select2']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('object_insurances[]', __('cms-services::admin.Object insurances')) !!}
    <div class="input-group">
        {!! Form::select('object_insurances[]', $objectInsurances, old('object_insurances', $selectedObjectInsurances), ['multiple' => 'multiple', 'class' => 'js-select2']) !!}
    </div>
</div>
