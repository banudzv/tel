<div class="row">
    <div class="col-lg-7">
        <div class="card mb-3">
            <div class="card-body">
                @langTabs
                <div class="form-group">
                    {!! Form::label($locale . '[name]', __('cms-services::admin.Name')) !!}
                    {!! Form::text($locale . '[name]', old($locale . '.name', $obj->translateOrNew($locale)->name)) !!}
                </div>
                <div class="form-group">
                    {!! Form::label($locale . '[text]', __('cms-services::admin.Text')) !!}
                    {!! Form::textarea($locale . '[text]', old($locale . '.text', $obj->translateOrNew($locale)->text)) !!}
                </div>
                @endLangTabs
            </div>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="card mb-3">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5 class="py-2"><strong>@lang('cms-core::admin.layout.Main data')</strong></h5>
            </div>
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label('published', __('cms-core::admin.layout.Published')) !!}
                    {!! Form::status('published') !!}
                </div>
                <div class="form-group">
                    {!! Form::label(str_slug('icon'), __('cms-core::admin.layout.Icon')) !!}
                    <div class="input-group">
                        {!! Form::select('icon', \WezomCms\Core\Foundation\Icon::getForSelect(), old('icon', $obj->icon), ['class' => 'js-select2']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


