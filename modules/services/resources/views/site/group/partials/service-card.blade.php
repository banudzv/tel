<div class="program {{ !($loop->iteration % 2) ? 'program--inverse' : '' }}">
    <div class="program__content">
        <div class="program__title">
            {{ $child->name }}
        </div>
        @if($child->services && count($child->services))
            <ul class="program__list">
                @foreach($child->services ?? [] as $service)
                    <li class="program__list-item">{{ $service->name }}</li>
                @endforeach
            </ul>
        @endif
        <div class="program__link">
            @widget('ui:button', [
                'component' => 'a',
                'attrs' => [
                    'href' => route('service-group.inner', [
                        'baseSlug' => $groupSlug ?? null,
                        'slug' => $child->slug
                    ])
                ],
                'text' => __('cms-services::site.more details'),
                'modificators' => [
                    'color-accent',
                    'size-smd',
                    'uppercase',
                    'ls-def',
                    'bold'
                ]
            ])
        </div>
    </div>
    <div class="program__image">
        <img class="js-import lozad" src="{{ url('/images/empty.gif') }}" data-src="{{ $child->getImageBlock() }}" alt="">
    </div>
</div>
