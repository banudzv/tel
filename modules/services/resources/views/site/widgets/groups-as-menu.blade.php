@php
    /**
     * @var $models \Illuminate\Database\Eloquent\Collection|\WezomCms\Services\Models\ServiceGroup[]
     * @var $children \WezomCms\Services\Models\ServiceGroup
     */
@endphp
<ul class="header-menu">
    @foreach($models as $group)
        <li class="header-menu__item">
            <a class="header-menu__link"
               data-dropdown-link
               data-dropdown-selector="{{ $group->slug }}"
               href="{{ route('service-group-base.inner', ['slug' => $group->slug]) }}">
                {{ $group->name }}
            </a>
        </li>
    @endforeach
</ul>
@foreach($models as $group)
    @if(count($group->children))
        <div class="menu menu--theme-categories" data-dropdown-menu="{{ $group->slug }}">
            <div class="container">
                <ul class="menu__list _grid _spacer _spacer--lg">
                    @foreach($group->children as $children)
                        <li  class="_cell _cell--auto menu__item">
                            <a class="menu__link"
                               href="{{ route('service-group.inner', [
                                   'baseSlug' => $group->slug,
                                   'slug' => $children->slug
                               ]) }}">
                                @svg($children->icon, null, null, 'menu__link-icon')
                                <span>
                                    {{ $children->name }}
                                </span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
@endforeach
