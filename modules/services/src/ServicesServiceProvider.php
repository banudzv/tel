<?php

namespace WezomCms\Services;

use Illuminate\Database\Eloquent\Collection;
use SidebarMenu;
use WezomCms\Core\BaseServiceProvider;
use WezomCms\Core\Contracts\PermissionsContainerInterface;
use WezomCms\Core\Contracts\SitemapXmlGeneratorInterface;
use WezomCms\Core\Foundation\Helpers;
use WezomCms\Services\Models\Service;
use WezomCms\Services\Models\ServiceGroup;
use WezomCms\Services\Repositories\ServiceGroupRepository;

class ServicesServiceProvider extends BaseServiceProvider
{
    /**
     * All module widgets.
     *
     * @var array|string|null
     */
    protected $widgets = 'cms.services.services.widgets';

    /**
     * Dashboard widgets.
     *
     * @var array|string|null
     */
    protected $dashboard = 'cms.services.services.dashboards';

    protected $translationKeys = [
        'cms-services::admin.Choice',
    ];

    /**
     * @return \Lavary\Menu\Item
     */
    public static function makeAdminGroup()
    {
        $group = SidebarMenu::get('services');
        if (!$group) {
            $group = SidebarMenu::add(__('cms-services::admin.Services'), route('admin.services.index'))
                ->data('icon', 'fa-briefcase')
                ->data('position', 30)
                ->nickname('services');
        }

        return $group;
    }

    public function permissions(PermissionsContainerInterface $permissions)
    {
        $permissions->add('services', __('cms-services::admin.Services'))
            ->withEditSettings()
        ;
        $permissions->add('service-additionals', __('cms-services::admin.Service additional'));
        $permissions->add('risks', __('cms-services::admin.Risks'));
        $permissions->add('risk-groups', __('cms-services::admin.risk.Group name'));
        $permissions->add('service-groups', __('cms-services::admin.Service groups'));
        $permissions->add('service-steps', __('cms-services::admin.Service step'));
        $permissions->add('service-documents', __('cms-services::admin.Service document'));
        $permissions->add('object-insurances', __('cms-services::admin.Object insurances'));
    }

    public function adminMenu()
    {
        $useGroups = config('cms.services.services.use_groups');
        if ($useGroups || Helpers::providerLoaded('WezomCms\ServicesOrders\ServicesOrdersServiceProvider')) {
            $group = static::makeAdminGroup();

            if ($useGroups) {
                $group->add(__('cms-services::admin.Groups'), route('admin.service-groups.index'))
                    ->data('permission', 'service-groups.view')
                    ->data('icon', 'fa-th-large')
                    ->data('position', 1);
            }

            $group->add(__('cms-services::admin.Services'), route('admin.services.index'))
                ->data('permission', 'services.view')
                ->data('icon', 'fa-list')
                ->data('position', 2);

            $group->add(__('cms-services::admin.Risks'), route('admin.risks.index'))
                ->data('permission', 'risks.view')
                ->data('icon', 'fa-list')
                ->data('position', 3);
            $group->add(__('cms-services::admin.risk.Group name'), route('admin.risk-groups.index'))
                ->data('permission', 'risk-groups.view')
                ->data('icon', 'fa-list')
                ->data('position', 3);

            $group->add(__('cms-services::admin.Service additional'), route('admin.service-additionals.index'))
                ->data('permission', 'service-additionals.view')
                ->data('icon', 'fa-list')
                ->data('position', 4);

            $group->add(__('cms-services::admin.Service step'), route('admin.service-steps.index'))
                ->data('permission', 'service-steps.view')
                ->data('icon', 'fa-list')
                ->data('position', 5);

            $group->add(__('cms-services::admin.Service document'), route('admin.service-documents.index'))
                ->data('permission', 'service-documents.view')
                ->data('icon', 'fa-list')
                ->data('position', 6);

            $group->add(__('cms-services::admin.Object insurances'), route('admin.object-insurances.index'))
                ->data('permission', 'object-insurances.view')
                ->data('icon', 'fa-list')
                ->data('position', 7);

        } else {
            SidebarMenu::add(__('cms-services::admin.Services'), route('admin.services.index'))
                ->data('permission', 'services.view')
                ->data('icon', 'fa-briefcase')
                ->data('position', 30)
                ->nickname('services');
        }
    }

    /**
     * @return array
     */
    public function sitemap()
    {
        $data = [];
        $services = \App::make(ServiceGroupRepository::class)->getBySiteMap();

        foreach($services ?? [] as $k => $baseGroup){
            /** @var $baseGroup ServiceGroup */
            $data[$k]['id'] = $baseGroup->id;
            $data[$k]['sort'] = -60;
            $data[$k]['name'] = $baseGroup->name;
            $data[$k]['url'] = $baseGroup->getFrontUrl();

            foreach($baseGroup->children ?? [] as $ke => $group){
                /** @var $group ServiceGroup */
                $data[$k]['parent'][$ke]['id'] = $group->id;
                $data[$k]['parent'][$ke]['sort'] = $group->sort;
                $data[$k]['parent'][$ke]['name'] = $group->name;
                $data[$k]['parent'][$ke]['url'] = $group->getFrontUrl();

                foreach ($group->services ?? [] as $key => $service){
                    /** @var $service Service */
                    $data[$k]['parent'][$ke]['parent'][$key]['id'] = $service->id;
                    $data[$k]['parent'][$ke]['parent'][$key]['sort'] = $service->sort;
                    $data[$k]['parent'][$ke]['parent'][$key]['name'] = $service->name;
                    $data[$k]['parent'][$ke]['parent'][$key]['url'] = $service->getFrontUrl();
                }
            }
        }

        return $data;
    }

    /**
     * @param  SitemapXmlGeneratorInterface  $sitemap
     * @throws \ErrorException
     */
    public function sitemapXml(SitemapXmlGeneratorInterface $sitemap)
    {
        $services = \App::make(ServiceGroupRepository::class)->getBySiteMap();
        foreach($services ?? [] as $k => $baseGroup){
            /** @var $baseGroup ServiceGroup */
            $sitemap->addLocalizedRoute('service-group-base.inner', $baseGroup->slug);
            foreach($baseGroup->children ?? [] as $ke => $group){
                /** @var $group ServiceGroup */
                $sitemap->addLocalizedRoute('service-group.inner', ['baseSlug' => $baseGroup->slug, 'slug' => $group->slug]);
                foreach ($group->services ?? [] as $key => $service){
                    /** @var $service Service */
                    $sitemap->addLocalizedRoute('services.inner', $service->slug);
                }
            }
        }
    }
}
