<?php

namespace WezomCms\Services\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Traits\LocalizedRequestTrait;

class DocumentRequest extends FormRequest
{
    use LocalizedRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->localizeRules(
            [
                'name' => 'required|string|max:255',
                'text' => 'required|string',
            ],
            [
                'published' => 'required',
                'icon' => 'nullable|string'
            ]
        );
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->localizeAttributes(
            [
                'name' => __('cms-services::admin.Name'),
                'text' => __('cms-services::admin.Text'),
            ],
            [
                'published' => __('cms-core::admin.layout.Published'),
                'icon' => __('cms-core::admin.layout.Icon')
            ]
        );
    }
}


