<?php

namespace WezomCms\Services\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Services\Http\Requests\Admin\DocumentRequest;
use WezomCms\Services\Models\Document;

class DocumentController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Document::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-services::admin.documents';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.service-documents';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = DocumentRequest::class;

    /**
     * ServicesController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-services::admin.Service document');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }
}


