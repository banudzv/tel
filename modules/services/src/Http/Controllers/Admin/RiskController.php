<?php

namespace WezomCms\Services\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Services\Http\Requests\Admin\RiskRequest;
use WezomCms\Services\Models\Risk;
use WezomCms\Services\Models\RiskGroup;
use WezomCms\Services\Models\Service;
use WezomCms\Services\Repositories\RiskGroupRepository;

class RiskController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Risk::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-services::admin.risks';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.risks';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = RiskRequest::class;
    /**
     * @var RiskGroupRepository
     */
    private $riskGroupRepository;

    /**
     * ServicesController constructor.
     */
    public function __construct(RiskGroupRepository $riskGroupRepository)
    {
        parent::__construct();
        $this->riskGroupRepository = $riskGroupRepository;
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-services::admin.Risks');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }

    /**
     * @param  Service  $model
     * @param  array  $viewData
     * @return array
     */
    protected function formData($model, array $viewData): array
    {
        return [
            'groups' => $this->riskGroupRepository->getBySelect(),
        ];
    }
}

