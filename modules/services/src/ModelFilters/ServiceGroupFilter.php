<?php

namespace WezomCms\Services\ModelFilters;

use EloquentFilter\ModelFilter;
use WezomCms\Core\Contracts\Filter\FilterListFieldsInterface;
use WezomCms\Core\Filter\FilterField;
use WezomCms\Services\Models\ServiceGroup;

/**
 * Class ServiceGroupFilter
 * @package WezomCms\Services\ModelFilters
 * @mixin ServiceGroup
 */
class ServiceGroupFilter extends ModelFilter implements FilterListFieldsInterface
{
    /**
     * Generate array with fields
     * @return iterable|FilterField[]
     */
    public function getFields(): iterable
    {
        return [
            FilterField::makeName(),
            FilterField::published(),
            FilterField::is_top(),
        ];
    }

    public function published($published)
    {
        $this->where('published', $published);
    }

    public function top($is_top)
    {
        $this->where('is_top', $is_top);
    }

    public function name($name)
    {
        $this->related('translations', 'name', 'LIKE', '%' . $name . '%');
    }
}
