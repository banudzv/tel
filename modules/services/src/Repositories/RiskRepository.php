<?php

namespace WezomCms\Services\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Services\Models\Risk;

class RiskRepository extends AbstractRepository
{
    protected function model()
    {
        return Risk::class;
    }

    public function getBySelectWithGroup()
    {
        return $this->getModelQuery()
            ->published()
            ->with('group')
            ->orderBy('sort')
            ->groupBy('group_id')
            ->get()
            ->toArray();
    }
}
