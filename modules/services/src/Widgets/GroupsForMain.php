<?php

namespace WezomCms\Services\Widgets;

use Illuminate\Support\Collection;
use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\Services\Models\ServiceGroup;
use WezomCms\Services\Repositories\ServiceGroupRepository;

class GroupsForMain extends AbstractWidget
{

    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        $repository = resolve(ServiceGroupRepository::class);
        $limit = config('cms.services.services.count_top');

        /** @var  $models ServiceGroup[] */
        $models = $repository->getTop($limit);

        if ($models->isEmpty()) {
            return null;
        }

        return compact('models');
    }
}
