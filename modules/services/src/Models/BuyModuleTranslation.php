<?php

namespace WezomCms\Services\Models;

use Illuminate\Database\Eloquent\Model;
use WezomCms\Core\Traits\Model\MultiLanguageSluggableTrait;

/**
 *
 * @property int $id
 * @property int $buy_module_id
 * @property string $title
 * @property string $sub_title
 * @property string $text_by_phone
 * @property string $text_by_office
 * @property string $text_by_online
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class BuyModuleTranslation extends Model
{
    protected $table = 'buy_module_service_group_translations';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'sub_title',
        'text_by_phone',
        'text_by_office',
        'text_by_online',
    ];
}

