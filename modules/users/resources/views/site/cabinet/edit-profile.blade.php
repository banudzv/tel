@extends('cms-users::site.layouts.cabinet')

@php
    /**
     * @var $user \WezomCms\Users\Models\User
     */
@endphp

@section('content')
    <div class="container">
        <h1>{{ SEO::getH1() }}</h1>
        <div>
            @widget('cabinet-menu')
        </div>
        <div class="grid">
            <div class="gcell">
                <form class="js-import" method="post" action="{{ route('cabinet.update') }}">
                    <div>
                        <input type="text"
                               name="username"
                               value="{{ old('name', $user->name) }}"
                               placeholder="@lang('cms-users::site.Имя') *"
                               inputmode="text"
                               spellcheck="true"
                               required="required">
                    </div>
                    <div>
                        <input type="text"
                               name="surname"
                               value="{{ old('surname', $user->surname) }}"
                               placeholder="@lang('cms-users::site.Фамилия')"
                               inputmode="text"
                               spellcheck="true">
                    </div>
                    <div>
                        <input type="email"
                               name="email"
                               inputmode="email"
                               placeholder="@lang('cms-users::site.E-mail') *"
                               value="{{ old('email', $user->email) }}"
                               required="required">
                    </div>
                    <div>
                        <input type="tel"
                               name="phone"
                               inputmode="tel"
                               value="{{ old('phone', $user->phone) }}"
                               placeholder="@lang('cms-users::site.Телефон')">
                    </div>
                    <button type="submit" class="button">@lang('cms-users::site.Сохранить')</button>
                    <a href="{{ route('cabinet') }}">@lang('cms-users::site.Отмена')</a>
                </form>
            </div>
            <div class="gcell">
                @widget('cabinet-socials')
                @widget('cabinet-submenu')
            </div>
        </div>
    </div>
@endsection
