@php
    /**
     * @var $menu \Lavary\Menu\Builder
     * @var $highlightedUri string
     */
@endphp
<p>@lang('cms-users::site.Мой аккаунт')</p>
@foreach($menu->roots() as $item)
    <div>
        @if($item->isActive)
            <span class="is-active">{{ $item->title }}</span>
        @elseif($item->url() === $highlightedUri)
            <a href="{{ $item->url() }}" class="is-active">{{ $item->title }}</a>
        @elseif($item->url())
            <a href="{{ $item->url() }}"
               class="{{ $item->class }}" {!! $item->el_attributes !!}>{{ $item->title }}</a>
        @endif
    </div>
@endforeach
<div>
    <a href="#" onclick="event.preventDefault(); document.forms['logout-form'].submit();">
        @lang('cms-users::site.cabinet.Logout')
    </a>
</div>
<form id="logout-form" action="{{ route('auth.logout') }}" method="POST"
      style="display: none;">
    @csrf
</form>
