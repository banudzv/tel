<?php

namespace WezomCms\Users\Http\Controllers\Site;

use Auth;
use Flash;
use Hash;
use Illuminate\Validation\ValidationException;
use NotifyMessage;
use WezomCms\Core\Foundation\JsResponse;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Users\Http\Requests\Site\UpdatePasswordRequest;
use WezomCms\Users\Http\Requests\Site\UpdateUserInfoRequest;

class CabinetController extends SiteController
{
    protected function before()
    {
        $this->addBreadcrumb(settings('users.cabinet.name'), route('cabinet'));
    }

    /**
     * Page with form for edit profile info.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->before();

        $settings = settings('users.cabinet', []);
        $pageName = array_get($settings, 'name');

        $this->seo()
            ->setTitle(settings('users.cabinet.title'))
            ->setH1(settings('users.cabinet.h1'))
            ->setPageName($pageName);

        return view('cms-users::site.cabinet.index', ['user' => Auth::user()]);
    }

    /**
     * Page with form for edit profile info.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editProfile()
    {
        $this->before();

        $settings = settings('users.edit-profile', []);
        $pageName = array_get($settings, 'name', __('cms-users::site.cabinet.Edit personal info'));

        $this->addBreadcrumb($pageName);
        $this->seo()
            ->setTitle(settings('users.edit-profile.title'))
            ->setH1(settings('users.edit-profile.h1'))
            ->setPageName($pageName);

        return view('cms-users::site.cabinet.edit-profile', ['user' => Auth::user()]);
    }

    /**
     * @param  UpdateUserInfoRequest  $request
     * @return JsResponse
     */
    public function updateUserInfo(UpdateUserInfoRequest $request)
    {
        $user = Auth::user();
        $user->update($request->validated());

        $msg = NotifyMessage::success(__('cms-users::site.cabinet.Data successfully updated'));
        if ($request->expectsJson()) {
            return JsResponse::make()->notification($msg)->reset(false);
        }

        Flash::push($msg);

        return back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function changePassword()
    {
        $this->before();

        $settings = settings('users.change-password', []);
        $pageName = array_get($settings, 'name', __('cms-users::site.cabinet.Change password'));

        $this->addBreadcrumb($pageName);
        $this->seo()
            ->setTitle(settings('users.change-password.title'))
            ->setH1(settings('users.change-password.h1'))
            ->setPageName($pageName);

        return view('cms-users::site.cabinet.change-password', [
            'passwordMinLength' => config('cms.users.users.password_min_length'),
        ]);
    }

    /**
     * @param  UpdatePasswordRequest  $request
     * @return JsResponse
     */
    public function saveNewPassword(UpdatePasswordRequest $request)
    {
        $user = $request->user();

        if (!password_verify($request->get('old_password'), $user->getAuthPassword())) {
            throw ValidationException::withMessages([
                'old_password' => [__('cms-users::site.cabinet.Password is entered incorrectly')],
            ]);
        }

        $user->update([
            'password' => Hash::make($request->get('password')),
        ]);

        return JsResponse::make()
            ->notification(__('cms-users::site.cabinet.Password successfully changed'));
    }
}
