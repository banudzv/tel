## Installation

#### Use console
```
composer require wezom-cms/pages
```
#### Or edit composer.json
```
"require": {
    ...
    "wezom-cms/pages": "^7.0"
}
```
#### Install dependencies:
```
composer update
```
#### Run migrations
```
php artisan migrate
```

## Publish
#### Views
```
php artisan vendor:publish --provider="WezomCms\Pages\PagesServiceProvider" --tag="views"
```
#### Lang
```
php artisan vendor:publish --provider="WezomCms\Pages\PagesServiceProvider" --tag="lang"
```
#### Migrations
```
php artisan vendor:publish --provider="WezomCms\Pages\PagesServiceProvider" --tag="migrations"
```
