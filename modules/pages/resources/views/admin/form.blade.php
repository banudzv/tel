<div class="row">
    <div class="col-lg-7">
        <div class="card mb-3">
            <div class="card-body">
                @langTabs
                <div class="form-group">
                    {!! Form::label($locale . '[name]', __('cms-pages::admin.Name')) !!}
                    {!! Form::text($locale . '[name]', old($locale . '.name', $obj->translateOrNew($locale)->name)) !!}
                </div>
                <div class="form-group">
                    {!! Form::label($locale . '[slug]', __('cms-core::admin.layout.Slug')) !!}
                    {!! Form::slugInput($locale . '[slug]', old($locale . '.slug', $obj->translateOrNew($locale)->slug), ['source' => 'input[name="' . $locale . '[name]"']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label($locale . '[text]', __('cms-pages::admin.Text')) !!}
                    {!! Form::textarea($locale . '[text]', old($locale . '.text', $obj->translateOrNew($locale)->text), ['class' => 'js-wysiwyg', 'data-lang' => $locale]) !!}
                </div>

                @include('cms-core::admin.partials.form-meta-inputs', compact('obj', 'locale'))
                @endLangTabs
            </div>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="card mb-3">
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label('published', __('cms-core::admin.layout.Published')) !!}
                    {!! Form::status('published') !!}
                </div>
{{--               todo удалить поле из бд--}}
{{--                <div class="form-group">--}}
{{--                    {!! Form::label(str_slug('for_main_report'), __('cms-pages::admin.For main report')) !!}--}}
{{--                    {!! Form::status('for_main_report', $obj->exists ? $obj->for_main_report : false) !!}--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
</div>
