<?php

use Faker\Generator as Faker;
use WezomCms\Contacts\Models\Contact;

$factory->define(Contact::class, function (Faker $faker) {
    return [
        'read' => $faker->boolean(80),
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'city' => $faker->city,
        'message' => $faker->realText($faker->numberBetween(100, 250)),
    ];
});
