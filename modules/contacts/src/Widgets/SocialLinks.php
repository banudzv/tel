<?php

namespace WezomCms\Contacts\Widgets;

use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\Core\Models\Setting;

class SocialLinks extends AbstractWidget
{
    /**
     * A list of models that, when changed, will clear the cache of this widget.
     *
     * @var array
     */
    public static $models = [Setting::class];

    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        $siteSettings = settings('contacts.site', []);

        $socials = array_filter([
            'facebook' => array_get($siteSettings, 'facebook'),
            'instagram' => array_get($siteSettings, 'instagram'),
            'youtube' => array_get($siteSettings, 'youtube'),
        ]);

        if (empty($socials)) {
            return null;
        }

        return compact('socials');
    }
}
