<?php

namespace WezomCms\Contacts\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'read' => 'required',
            'name' => 'nullable|string|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'nullable|email|max:255',
            'number_contract' => 'nullable|string',
            'message' => 'required|string|max:65535',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'read' => __('cms-contacts::admin.Status'),
            'name' => __('cms-contacts::admin.Name'),
            'phone' => __('cms-contacts::admin.Phone'),
            'email' => __('cms-contacts::admin.E-mail'),
            'message' => __('cms-contacts::admin.Message'),
            'number_contract' => __('cms-callbacks::admin.Contract number'),
        ];
    }
}
