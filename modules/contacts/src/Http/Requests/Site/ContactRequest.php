<?php

namespace WezomCms\Contacts\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'nullable|string|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'nullable|email|max:255',
            'number_contract' => 'nullable|string',
            'message' => 'nullable|string',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'username' => __('cms-contacts::site.Name'),
            'phone' => __('cms-contacts::site.Phone'),
            'email' => __('cms-contacts::site.E-mail'),
            'number_contract' => __('cms-callbacks::admin.Contract number'),
            'message' => __('cms-contacts::site.Message'),
        ];
    }
}
