<?php

namespace WezomCms\Contacts\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\Contacts\Http\Requests\Admin\ContactRequest;
use WezomCms\Contacts\Models\Contact;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Core\Settings\AdminLimit;
use WezomCms\Core\Settings\Fields\AbstractField;
use WezomCms\Core\Settings\Fields\Input;
use WezomCms\Core\Settings\Fields\Textarea;
use WezomCms\Core\Settings\Fields\Wysiwyg;
use WezomCms\Core\Settings\MetaFields\SeoFields;
use WezomCms\Core\Settings\Fields\Image;
use WezomCms\Core\Settings\MultilingualGroup;
use WezomCms\Core\Settings\RenderSettings;
use WezomCms\Core\Settings\Tab;
use WezomCms\Core\Traits\SettingControllerTrait;

class ContactsController extends AbstractCRUDController
{
    use SettingControllerTrait;

    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Contact::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-contacts::admin';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.contacts';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = ContactRequest::class;

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-contacts::admin.Contacts');
    }

    /**
     * @return string|null
     */
    protected function frontUrl(): ?string
    {
        return route('contacts');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->latest('created_at');
    }

    /**
     * @return array|AbstractField[]|MultilingualGroup[]
     * @throws \Exception
     */
    protected function settings(): array
    {

        $result = [];

        $subTitle = Textarea::make()
            ->setKey('sub_title')
            ->setName(__('cms-contacts::admin.Sub title'))
            ->setIsMultilingual()
            ->setRows(3)
            ->setSort(5);
        $textPopup = Wysiwyg::make()
            ->setKey('popup_text')
            ->setName(__('cms-contacts::admin.Text popup'))
            ->setIsMultilingual()
            ->setRows(3)
            ->setSort(5);

        $tabs = new RenderSettings(
            new Tab('page-settings', __('cms-core::admin.tabs.site data'), 1, 'fa-folder-o')
        );

        $tabsImage = new RenderSettings(
            new Tab('page', __('cms-core::admin.tabs.site banner'), 3, 'fa-folder-o')
        );

        $result[] = Input::make($tabs)
            ->setName(__('cms-contacts::admin.Hotline'))
            ->setSort(1)
            ->setKey('hotline')
            ->setRules('nullable|string');

        $result[] = Input::make($tabs)
            ->setName(__('cms-contacts::admin.Facebook'))
            ->setSort(2)
            ->setKey('facebook')
            ->setRules('nullable|string');

        $result[] = Input::make($tabs)
            ->setName(__('cms-contacts::admin.Telegram'))
            ->setSort(3)
            ->setKey('telegram')
            ->setRules('nullable|string');

        $result[] = Input::make($tabs)
            ->setName(__('cms-contacts::admin.Fb messanger'))
            ->setSort(4)
            ->setKey('fb_messenger')
            ->setRules('nullable|string');

        $result[] = Input::make($tabs)
            ->setName(__('cms-contacts::admin.Viber'))
            ->setSort(5)
            ->setKey('viber')
            ->setRules('nullable|string');

        $result[] = SeoFields::make('Contacts', [$subTitle, $textPopup]);
        $result[] = AdminLimit::make();
        $result[] = Image::make($tabsImage)
            ->setSettings([
                'images' => [
                    'directory' => 'settings',
                    'default' => 'medium', // For admin image preview
                    'sizes' => [
                        'big' => [
                            'width' => 1920,
                            'height' => 700,
                            'mode' => 'resize',
                        ],
                    ],
                ],
            ])
            ->setName(__('cms-reports::admin.image-size', ['width' => 1920, 'height' => 700]))
            ->setSort(1)
            ->setKey('image')
            ->setRules('nullable|file');

        return $result;


// так было
//        $siteSettings = RenderSettings::siteTab();
//
//        $workTime = Textarea::make()
//            ->setKey('work_time')
//            ->setName(__('cms-contacts::admin.Work time'))
//            ->setIsMultilingual()
//            ->setRows(3)
//            ->setSort(5);
//
//        $address = Input::make()
//            ->setKey('address')
//            ->setName(__('cms-contacts::admin.Address'))
//            ->setIsMultilingual()
//            ->setSort(6);
//
//        $email = Input::make($siteSettings)
//            ->setKey('email')
//            ->setName(__('cms-contacts::admin.E-mail'))
//            ->setRules('nullable|email')
//            ->setSort(1);
//
//        $phone = Textarea::make($siteSettings)
//            ->setKey('phone')
//            ->setName(__('cms-contacts::admin.Phone'))
//            ->setRows(3)
//            ->setSort(2);

//        return [
//            SeoFields::make('Contacts', [$workTime, $address], null, 10),
//            $email,
//            $phone,
////            $this->makeSocialInput($siteSettings, 'facebook', __('cms-contacts::admin.Facebook'), 3),
////            $this->makeSocialInput($siteSettings, 'instagram', __('cms-contacts::admin.Instagram'), 4),
////            $this->makeSocialInput($siteSettings, 'youtube', __('cms-contacts::admin.Youtube'), 5),
//            AdminLimit::make(),
//        ];
    }

    /**
     * @param  RenderSettings  $renderSettings
     * @param  string  $key
     * @param  string  $name
     * @param  int  $sort
     * @return AbstractField
     */
    private function makeSocialInput(
        RenderSettings $renderSettings,
        string $key,
        string $name,
        int $sort = 0
    ): AbstractField {
        return Input::make($renderSettings)
            ->setKey($key)
            ->setName($name)
            ->setSort($sort)
            ->setRules('nullable|url');
    }
}
