@php
    $mapConfig = (object) [
        'factory' => 'DefaultGoogleMap',
        'settings' => (object) [
            'center' => (object) $centerCoords,
        ],
        'props' => (object) [
            'markerUrl' => '/static/images/map-marker.svg',
            'markersData' => $branchesCoords
        ],
    ];


@endphp

@extends('cms-ui::layouts.main')

@section('content')
    @widget('ui:heading-section', [
        'bannerImage' => $banner,
        'title' => SEO::getH1(),
        'text' => $subTitle,
        'classes' => 'heading-section--contacts',
        'button' => (object)[
            'text' => __('cms-contacts::site.Send message'),
            'attrs' => [
                'type' => 'button',
                'data-scroll-window' => '#quality-control'
            ],
            'component' => 'button',
            'classes' => 'js-import'
        ]
    ])
    <div class="section">
        <div class="container">
            <div class="nav-block nav-block--contacts">
                <div class="container container--theme-default">
                    <div class="nav-block__inner">
                        <div class="nav-block__group">
                            <div class="nav-block__text">
                                @lang('cms-contacts::site.Choose region')
                            </div>
                            <div class="nav-block__select">
                                @component('cms-ui::components.form.select2',
                                     [
                                         'selectOptions' => [
                                             'Factory' => 'SelectWithLinks'
                                        ],
                                        'class' => 'form-item--dark',
                                        'options' => $regions,
                                        'name' => uniqid('select2'),
                                        'attrs' => ['name' => 'district_id']
                                    ]
                                )
                                    @foreach($regions ?? [] as $item)
                                        @if($selectedId && $selectedId == $item['id'])
                                            <option value="{{ $item['id'] }}" selected data-url-to-go="{{ route('contacts', ['region' => $item['slug']]) }}">
                                                {{ $item['name'] }}
                                            </option>
                                        @else
                                            <option value="{{ $item['id'] }}" data-url-to-go="{{ route('contacts', ['region' => $item['slug']]) }}">
                                                {{ $item['name'] }}
                                            </option>
                                        @endif
                                    @endforeach
                                @endcomponent
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section--bg-grey _py-lg _md:py-hg">
        <div class="container container--theme-default">
            @foreach($branches ?? [] as $branch)
                @include('cms-contacts::site.components.contact', ['contact' => $branch])
            @endforeach
        </div>
    </div>
    @if(!empty($branches))
        <div class="section">
            <div data-map-container>
                <div class="google-map js-import"
                     id="map"
                     data-initialize-google-map='{!! json_encode($mapConfig) !!}'>
                </div>
            </div>
        </div>
    @endif

    @include('cms-ui::widgets.feedback-form')
@endsection
