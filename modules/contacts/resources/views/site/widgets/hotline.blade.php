@php
    /**
      * @var $hotline string
      */
@endphp

<div class="hotline {{ isset($classes) && $classes ? $classes : '' }}">
    @if(isset($icon) && $icon)
        @svg('phone', null, null, 'hotline__icon')
    @endif
    <div class="_flex _flex-column _items-end">
        <a href="tel:{{ $hotline }}" class="hotline__link">{{ $hotline }}</a>
        <div class="hotline__title">@lang('cms-contacts::site.Hotline')</div>
    </div>
</div>
