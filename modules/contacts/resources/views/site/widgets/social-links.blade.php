@php
    /**
     * @var $socials array
     */
@endphp
<ul>
    @foreach($socials as $name => $link)
        <li>
            <a href="{{ $link }}" target="_blank" rel="nofollow" title="{{ ucfirst($name) }}">
                @switch($name)
                    @case('facebook')
                    @lang('cms-contacts::site.Facebook')
                    @break
                    @case('instagram')
                    @lang('cms-contacts::site.Instagram')
                    @break
                    @case('youtube')
                    @lang('cms-contacts::site.Youtube')
                    @break
                @endswitch
            </a>
        </li>
    @endforeach
</ul>
