<?php

namespace WezomCms\Menu\Widgets;

use Illuminate\Support\Collection;
use WezomCms\Core\Foundation\Helpers;
use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\Menu\Models\Menu;

class MenuWidget extends AbstractWidget
{
    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        $position = array_get($this->data, 'position');
        if (!$position) {
            return null;
        }

        $forceRender = array_get($this->data, 'force_render', false);

        $menu = $this->getMenu($position);

        if (!$forceRender && $menu->isEmpty()) {
            return null;
        }

        $menu = Helpers::groupByParentId($menu);

        if (!$forceRender && empty($menu[null])) {
            return null;
        }

        $maxDepth = config("cms.menu.menu.groups{$position}.depth", 10);

        return compact('menu', 'maxDepth');
    }

    /**
     * @param  string  $position
     * @return Collection
     */
    protected function getMenu(string $position)
    {
        $menu = \Cache::driver('array')->rememberForever('site_menu', function () {
            return Menu::published()
                ->orderBy('sort')
                ->latest('id')
                ->with([
                    'children' => function ($query) {
                        $query->published()
                            ->orderBy('sort')
                            ->latest('id')
                            ->with([
                                'children' => function ($query) {
                                    $query->published()
                                        ->orderBy('sort')
                                        ->latest('id')
                                        ->with([
                                            'children' => function ($query) {
                                                $query->published()
                                                    ->orderBy('sort')
                                                    ->latest('id');
                                            }
                                        ]);
                                }
                            ]);
                    }
                ])
                ->get()
                ->groupBy('group');
        });

        return array_get($menu, $position, collect());
    }
}
