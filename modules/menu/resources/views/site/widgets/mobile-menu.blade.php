@php
    /**
     * @var $menu array|\WezomCms\Menu\Models\Menu[][]
     * @var $maxDepth int
     */
    $mmenuData = [
        'factory' => 'MobileNav',
        'factoryExtend' => (object)[
            'options' => [
                'navbar' => [
                    'title' => ''
                ],
            ],
            'configuration' => (object)[],
            'customData' => (object)[
                'openButtonSelector' => '[data-mmenu-opener-mobile-nav]'
            ]
        ]
    ];
@endphp
<div hidden>
    <nav data-mmenu-init='{!! r2d2()->attrJsonEncode($mmenuData) !!}'>
        <ul class="mobile-menu" id="mobile-nav-menu" class="_flex _flex-column">
            <li>
                @widget('ui:lang-switcher')
            </li>
            <li>
                @widget('ui:search-button', ['classes' => 'search-block--theme-mobile', 'button' => 'true'])
            </li>
            @if(!empty($menu[null]))
                @foreach($menu[null] as $item)
                    @include('cms-menu::site.partials.recursive')
                @endforeach
            @endif
            <li class="mobile-menu__buttons">
                @widget('ui:button', [
                    'component' => 'button',
                    'classes' => 'js-import',
                    'attrs' => [
                        'type' => 'button',
                        'data-mfp' => 'ajax',
                        'data-mfp-src' => route('callbacks.insurance-popup')
                    ],
                    'text' => __('cms-services::site.Insurance case button'),
                    'modificators' => ['color-accent-2', 'theme-pulse-animated', 'size-md', 'uppercase', 'ls-def', 'bold']
                ])
                @widget('ui:button', [
                'component' => 'a',
                'attrs' => [
                'href' => route('home')
                ],
                'text' => __('cms-payments::site.Pay online button'),
                'modificators' => ['color-light', 'size-md', 'uppercase', 'ls-md', 'bold']])
            </li>
        </ul>
    </nav>
</div>
