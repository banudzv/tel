@php
    /**
     * @var $menu array|\WezomCms\Menu\Models\Menu[][]
     * @var $item \WezomCms\Menu\Models\Menu
     * @var $maxDepth int
     */
$depth = $depth ?? 1;
@endphp
<li>
    @if($item->activeMode() === \WezomCms\Menu\Models\Menu::MODE_SPAN)
        <span>{{ $item->name }}</span>
    @else
        <a href="{{ url($item->url) }}">{{ $item->name }}</a>
    @endif
    @if($depth < $maxDepth && $item->children->isNotEmpty())
        <ul>
            @foreach($item->children as $children)
                @include('cms-menu::site.partials.recursive', ['item' => $children, 'depth' => $depth + 1])
            @endforeach
        </ul>
    @endif
</li>
