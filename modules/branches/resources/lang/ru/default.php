<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
        'Branch' => 'Филиал',
        'E-mail' => 'E-mail',
        'Name' => 'Название',
        'Address' => 'Адрес',
        'Branches list' => 'Список филиалов',
        'Phones' => 'Телефоны',
        'Branches' => 'Филиалы',
        'List' => 'Список',
        'Add' => 'Добавить',
        'Map' => 'Карта',
        'City' => 'Город',
        'Time working' => 'Время работы',
        'Is main' => 'Центральный филиал',
        'Branch type' => 'Тип филиала',
        'Type office' => 'Офис',
        'Type cok' => 'Центр обслуживания клиентов',
        'Employee' => 'Руководитель',
        'Code' => 'Код ЕГРПОУ',
        'Date create' => 'Дата и номер решений',
        'Services additional' => 'Сервисы для данного филиала',
        'Zip code' => 'Почтовый индекс',
        'last 5 branches are shown' => 'последние 5 офисов отображаются в блоке Филиалы',
        'pay attention' => 'Обратите внимание'
    ],
    TranslationSide::SITE => [
        'title' => 'Сеть филиалов',
        'Central branch' => 'Центральный офис',
        'Type office' => 'Офис',
        'Type cop' => 'ЦОП',
        'type' => 'Центральный офис',
        'word city' => 'г.',
        'description' => 'Региональная сеть насчитывает :count филиалов и отделений по всей стране',
    ],
];
