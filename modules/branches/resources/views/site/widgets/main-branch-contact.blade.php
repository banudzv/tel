@php
    /**
     * @var $email string
     * @var $phones array
     * @var $full_address string
     */
@endphp

<div class="main-contacts {{ isset($modifier) ? $modifier : '' }}">
    <div class="main-contacts__title">@lang('cms-branches::site.Central branch')</div>
    <div class="main-contacts__address">{{ $full_address }}</div>
    <a href="mailto:{{ $email }}" class="main-contacts__link">
        @svg('email', null, null, 'main-contacts__icon')
        <span>
            {{ $email }}
        </span>
    </a>
    @foreach($phones as $phone)
        <a href="tel:{{ $phone }}" class="main-contacts__link">
            @svg('phone', null, null, 'main-contacts__icon')
            <span>
                {{ $phone }}
            </span>
        </a>
    @endforeach
</div>
