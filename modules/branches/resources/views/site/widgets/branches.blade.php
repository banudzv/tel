@php
    /**
     * @var $models \WezomCms\Branches\Models\Branch[]
     */
@endphp

<div class="section section--branches">
    <div class="container">
        <div class="branches js-import" data-branches>
            <div class="_grid _justify-center _spacer _spacer--md">
                <div class="_cell _cell--12 _ms:cell--10 _md:cell--8 _df:cell--5 _flex _flex-column _justify-center">
                    <div class="branches__content">
                        <div class="branches__title">
                            @lang('cms-branches::site.title')
                        </div>
                        <div class="branches__description">
                            @lang('cms-branches::site.description', ['count' => $models->count()])
                        </div>
                        <div class="branches__cities">
                            @foreach($models ?? [] as $branch)
                                <div class="branches__city">
                                    @widget('ui:button', ['component' => 'button', 'classes' => $loop->first ? 'is-active' : '', 'attrs' => ['type' => 'button', 'data-branches-button' => $loop->index], 'text' => $branch->city->name, 'modificators' => ['color-accent-3-light', 'size-sm']])
                                </div>
                            @endforeach
                        </div>
                        <div class="_mt-df _df:mt-hg">
                            <a href="{{ route('contacts') }}" class="button button--theme-page-link is-active">
                                @lang('cms-contacts::site.Contacts')
                            </a>
                        </div>
                    </div>
                </div>
                <div class="_cell _cell--12 _ms:cell--10 _md:cell--8 _df:cell--7">
                    <div class="branches-map">
                        <div class="branches-map__image">
                            <img data-src="{{ asset('/static/images/branches-map.png') }}"
                                 class="js-import lozad fade-in"
                                 src="{{ url('/images/empty.gif') }}"
                                 width="771"
                                 height="516"
                                 alt=""
                            >
                        </div>
                        @foreach($models ?? [] as $branch)
                            <div class="branches-map__marker branches-map__marker--{{ $loop->iteration }} js-import"
                                 data-branches-tooltip="{{ $loop->index }}"
                                 data-tooltip="{{json_encode(
                                     (object)[
                                     'options' => (object)[
                                         'theme' => 'branches',
                                         'trigger' => 'click',
                                         'hideOnClick' => false,
                                         'flip' => false,
                                         'flipOnUpdate' => false
                                     ]
                                 ])}}">
                                <div data-tooltip-container style="display: none">
                                    <div class="tooltip tooltip--theme-branches">
                                        <div class="tooltip__address">
                                            {{ $branch->city->name }},<br>
                                            {{ $branch->address }}
                                        </div>
                                        <a href="mailto:{{ $branch->email }}" class="tooltip__email">
                                            @svg('email')
                                            <span>
                                                {{ $branch->email }}
                                            </span>
                                        </a>
                                        @if($branch->phones)
                                            <div class="tooltip__phones">
                                                @svg('phone')
                                                <div>
                                                    @foreach($branch->phones ?? [] as $phone)
                                                        <a class="tooltip__phone" href="tel:{{ $phone }}">{{ $phone }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endif
                                        <div class="tooltip__schedule">
                                            @svg('clock-filled')
                                            <div>
                                                {!! $branch->time_working !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{--<div class="container">--}}
{{--    <h1>{{ SEO::getH1() }}</h1>--}}
{{--    <div class="grid">--}}
{{--        @foreach($branches as $branch)--}}
{{--            <div class="gcell">--}}
{{--                <div>--}}
{{--                    <div>{{ $branch->name }}</div>--}}
{{--                    <div>--}}
{{--                        <div class="grid">--}}
{{--                            @if($branch->address)--}}
{{--                                <div class="gcell">--}}
{{--                                    <div>@lang('cms-branches::site.Адрес')</div>--}}
{{--                                    <div>{{ $branch->address }}</div>--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                            @if($branch->phones)--}}
{{--                                <div class="gcell">--}}
{{--                                    <div>@lang('cms-branches::site.Телефон')</div>--}}
{{--                                    <div>--}}
{{--                                        @foreach($branch->phones as $phone)--}}
{{--                                            <a href="tel:{{ preg_replace('/[^\d\+]/', '', $phone) }}">{{ $phone }}</a>--}}
{{--                                        @endforeach--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            @endif```--}}
{{--                            @if($branch->email)--}}
{{--                                <div class="gcell">--}}
{{--                                    <div>@lang('cms-branches::site.Email')</div>--}}
{{--                                    <div>--}}
{{--                                        <a href="mailto:{{ $branch->email }}">{{ $branch->email }}</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="gglm gglm--small js-import" data-gglm data-no-xhr="true"--}}
{{--                         data-options="{{ json_encode($branch->map) }}">--}}
{{--                        <div class="gglm__display" data-gglm-display></div>--}}
{{--                        <div class="gglm__overlay js-init" data-gglm-stub--}}
{{--                             data-lozad="{{ url('assets/css/static/pic/google-map-overlay.jpg') }}">--}}
{{--                            <div class="gglm__overlay-text">--}}
{{--                                <svg viewBox="0 0 20 32" width="20" height="32">--}}
{{--                                    <use xlink:href="{{ url('assets/images/sprites/icons.svg#tap') }}"></use>--}}
{{--                                </svg>--}}
{{--                                <div>--}}
{{--                                    <span class="_i-mobile">@lang('cms-branches::site.Нажмите')</span>--}}
{{--                                    <span class="_i-desktop">@lang('cms-branches::site.Кликните')</span>--}}
{{--                                    <span>@lang('cms-branches::site.для отображения карты')</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endforeach--}}
{{--    </div>--}}
{{--</div>--}}
