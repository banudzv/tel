@extends('cms-core::admin.crud.index')

@section('info')
    <div class="d-flex align-items-center">
        <h6 class="m-b-0 m-r-10">@lang('cms-branches::admin.pay attention')</h6>
        <i class="fa fa-info-circle"
           data-toggle="tooltip"
           data-placement="top"
           title="@lang('cms-branches::admin.last 5 branches are shown')"
           style="font-size: 24px;"
        ></i>
    </div>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th class="sortable-column"></th>
                <th width="1%">@massControl($routeName)</th>
                <th>@lang('cms-branches::admin.City')</th>
                <th>@lang('cms-branches::admin.Address')</th>
                <th>@lang('cms-branches::admin.Branch type')</th>
                <th>@lang('cms-branches::admin.E-mail')</th>
                <th width="1%" class="text-center">@lang('cms-core::admin.layout.Manage')</th>
            </tr>
            </thead>
            <tbody class="js-sortable"
                   data-params="{{ json_encode(['model' => encrypt(\WezomCms\Branches\Models\Branch::class)]) }}">
                @foreach($result as $obj)
                    <tr data-id="{{ $obj->id }}">
                        <td>
                            <div class="js-sortable-handle sortable-handle">
                                <i class="fa fa-arrows"></i>
                            </div>
                        </td>
                        <td>@massCheck($obj)</td>
                        <td>@editResource(['obj'=>$obj->city, 'text' => $obj->city->name, 'ability' => 'cities.edit', 'target' => '_blank'])</td>
                        <td>@editResource(['obj' => $obj, 'text' => $obj->address])</td>
                        <td>{{ $obj::getTypeBySelect()[$obj->type] }}</td>
                        <td>@editResource(['obj' => $obj, 'text' => $obj->email])</td>
                        <td>
                            <div class="btn-group list-control-buttons" role="group">
                                @smallStatus(['obj' => $obj, 'request' => \WezomCms\Branches\Http\Requests\Admin\BranchRequest::class])
                                @editResource($obj, false)
                                @deleteResource($obj)
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
