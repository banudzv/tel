<?php

use Faker\Generator as Faker;
use WezomCms\Branches\Models\Branch;

$factory->define(Branch::class, function (Faker $faker) {
    return [
        'published' => $faker->boolean(80),
        'name' => $faker->realText(50),
        'email' => $faker->email,
        'address' => $faker->address,
        'phones' => [$faker->phoneNumber, $faker->phoneNumber],
    ];
});
