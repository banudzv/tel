<?php

namespace WezomCms\Branches\ModelFilters;

use EloquentFilter\ModelFilter;
use WezomCms\Branches\Models\Branch;
use WezomCms\Core\Contracts\Filter\FilterListFieldsInterface;
use WezomCms\Core\Filter\FilterField;
use WezomCms\Regions\Repositories\RegionsRepository;

/**
 * Class BranchFilter
 * @package WezomCms\Branches\ModelFilters
 * @mixin Branch
 */
class BranchFilter extends ModelFilter implements FilterListFieldsInterface
{
    /**
     * Generate array with fields
     * @return iterable|FilterField[]
     */
    public function getFields(): iterable
    {
        $regions = resolve(RegionsRepository::class)->getBySelect();

        return [
//            new FilterField(['name' => '', 'label' => __('cms-branches::admin.City')]),
            new FilterField(['name' => 'email', 'label' => __('cms-branches::admin.E-mail')]),
            new FilterField(['name' => 'address', 'label' => __('cms-branches::admin.Address'), 'colSize' => 2]),
            FilterField::make()
                ->name('type')
                ->label(__('cms-branches::admin.Branch type'))
                ->class('js-select2')
                ->type(FilterField::TYPE_SELECT)
                ->options(Branch::getTypeBySelect())
            ,
            FilterField::make()
                ->name('region')
                ->label(__('cms-regions::admin.Region'))
                ->class('js-select2')
                ->type(FilterField::TYPE_SELECT)
                ->options($regions)
            ,
        ];
    }

//    public function city($city)
//    {
//        $this->related('translations', 'city', 'LIKE', '%' . $city . '%');
//    }

    public function email($email)
    {
        $this->whereLike('email', $email);
    }

    public function address($name)
    {
        $this->related('translations', 'address', 'LIKE', '%' . $name . '%');
    }

    public function type($type)
    {
        $this->where('type', $type);
    }

    public function region($regionId)
    {
        $this->related('city', 'region_id', $regionId);
    }

    public function published($published)
    {
        $this->related('translations', 'published', $published);
    }

    public function locale($locale)
    {
        $this->related('translations', 'locale', $locale);
    }
}
