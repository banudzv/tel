<?php

namespace WezomCms\Branches\Repositories;

use WezomCms\Branches\Models\Branch;
use WezomCms\Core\Repositories\AbstractRepository;

class BranchRepository extends AbstractRepository
{
    protected function model()
    {
        return Branch::class;
    }

    public function getMainBranch()
    {
        return $this->getModelQuery()
            ->published()
            ->with(['translations', 'city', 'city.region.branches', ])
            ->where('is_main', true)
            ->first();
    }

    public function getByTypeWithRelation(array $relation, string $sort='sort', $type = Branch::TYPE_OFFICE)
    {
        return $this->getModelQuery()
            ->published()
            ->with($relation)
            ->where('type', $type)
            ->limit(Branch::LIMIT_MAIN)
            ->orderBy($sort)
            ->get();
    }

    public function getRegionsId()
    {
        $id = $this->getModelQuery()
            ->published()
            ->with(['city', 'city.region', ])
            ->get()->pluck('city.region_id')
            ->toArray();

        return array_values(array_unique($id));
    }
}

