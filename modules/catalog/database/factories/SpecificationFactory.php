<?php

use Faker\Generator as Faker;
use WezomCms\Catalog\Models\Specifications\Specification;

$factory->define(Specification::class, function (Faker $faker) {
    return [
        'published' => true,
        'sort' => $faker->randomNumber(),
        'name' => $faker->realText(15),
    ];
});
