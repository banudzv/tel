<?php

use WezomCms\Catalog\Models\Product;
use WezomCms\Catalog\Models\ProductSpecification;
use WezomCms\Catalog\Models\Specifications\Specification;

$factory->define(ProductSpecification::class, function () {
    $product = Product::inRandomOrder()->first();

    $specification = Specification::inRandomOrder()->first();

    $specValue = $specification->specValues()->inRandomOrder()->first();

    if (!$product || !$specification || !$specValue) {
        throw new Exception('Not enough data to create relations');
    }

    return [
        'product_id' => $product->id,
        'spec_id' => $specification->id,
        'spec_value_id' => $specValue->id
    ];
});
