<?php

use Faker\Generator as Faker;
use WezomCms\Catalog\Models\Model;

$factory->define(Model::class, function (Faker $faker) {
    $data = [
        'published' => $faker->boolean,
    ];

    return array_merge($data, array_fill_keys(array_keys(app('locales')), ['name' => $faker->realText(15)]));
});
