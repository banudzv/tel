<?php

namespace WezomCms\Catalog\Http\Controllers\Site;

use WezomCms\Catalog\Models\Category;
use WezomCms\Core\Http\Controllers\SiteController;

class CatalogController extends SiteController
{
    /**
     * @return mixed
     * @throws \Throwable
     */
    public function __invoke()
    {
        $catalogSettings = settings('categories.site', []);
        $pageName = array_get($catalogSettings, 'name') ?: __('cms-catalog::site.catalog.Catalog');

        $this->addBreadcrumb($pageName, route('catalog'));

        $this->seo()
            ->setTitle(array_get($catalogSettings, 'title'))
            ->setPageName($pageName)
            ->setH1(array_get($catalogSettings, 'h1'))
            ->setDescription(array_get($catalogSettings, 'description'))
            ->metatags()
            ->setKeywords(array_get($catalogSettings, 'keywords'));

        $categories = Category::published()
            ->whereNull('parent_id')
            ->orderBy('sort')
            ->latest('id')
            ->paginate(settings('categories.site.categories_limit', 10));

        // Render
        return view('cms-catalog::site.category.categories', ['result' => $categories]);
    }
}
