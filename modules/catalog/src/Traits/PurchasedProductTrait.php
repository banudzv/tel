<?php

namespace WezomCms\Catalog\Traits;

use Lang;

trait PurchasedProductTrait
{
    /**
     * @return bool
     */
    public function availableForPurchase(): bool
    {
        return $this->available;
    }

    /**
     * @return string|null
     */
    public function unit(): ?string
    {
        return Lang::get('cms-catalog::' . app('side') . '.products.pieces');
    }

    /**
     * @return float|int
     */
    public function minCountForPurchase()
    {
        return 1;
    }

    /**
     * @return float|int
     */
    public function stepForPurchase(): float
    {
        return 1;
    }

    /**
     * @param  float  $quantity
     * @return bool
     */
    public function validatePurchaseQuantity(float $quantity): bool
    {
        return $quantity > 0 && filter_var($quantity, FILTER_VALIDATE_INT) !== false;
    }
}
