<?php

namespace WezomCms\Catalog\Models;

use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use WezomCms\Catalog\Filter\Contracts\StorageInterface;
use WezomCms\Catalog\Models\Specifications\Specification;
use WezomCms\Catalog\Models\Specifications\SpecValue;
use WezomCms\Catalog\Traits\ProductFlagsTrait;
use WezomCms\Catalog\Traits\ProductImageTrait;
use WezomCms\Catalog\Traits\PurchasedProductTrait;
use WezomCms\Core\ExtendPackage\Translatable;
use WezomCms\Core\Traits\Model\Filterable;
use WezomCms\Core\Traits\Model\PublishedTrait;

/**
 * WezomCms\Catalog\Models\Product
 *
 * @property int $id
 * @property bool $published
 * @property string|null $group_key
 * @property float $cost
 * @property float $old_cost
 * @property array $videos
 * @property bool $novelty
 * @property bool $popular
 * @property bool $sale
 * @property \Illuminate\Support\Carbon|null $expires_at
 * @property int|null $discount_percentage
 * @property bool $available
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $brand_id
 * @property int|null $model_id
 * @property int|null $category_id
 * @property int|null $color_id
 * @property int $rating
 * @property-read \WezomCms\Catalog\Models\Brand|null $brand
 * @property-read \WezomCms\Catalog\Models\Category|null $category
 * @property-read \WezomCms\Catalog\Models\Specifications\SpecValue $color
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Catalog\Models\Product[] $combinedProducts
 * @property-read int|null $combined_products_count
 * @property-read string $flag_color
 * @property-read string $flag_text
 * @property-read \SupportCollection $flags
 * @property-read \Collection|\ProductImage[]|string[] $gallery
 * @property-read bool $has_flag
 * @property-read string|null $image_alt
 * @property-read string|null $image_title
 * @property-read \Illuminate\Support\Collection|\Product[] $variations
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Catalog\Models\ProductImage[] $images
 * @property-read int|null $images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Catalog\Models\ProductImage[] $mainImage
 * @property-read int|null $main_image_count
 * @property-read \WezomCms\Catalog\Models\Model|null $model
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Catalog\Models\ProductSpecification[] $productSpecifications
 * @property-read int|null $product_specifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Catalog\Models\Specifications\Specification[] $specifications
 * @property-read int|null $specifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Catalog\Models\Specifications\SpecValue[] $specificationsValues
 * @property-read int|null $specifications_values_count
 * @property-read \WezomCms\Catalog\Models\ProductTranslation $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Catalog\Models\ProductTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product filter($input = [], $filter = null)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\WezomCms\Catalog\Models\Product onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product published()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product publishedWithSlug($slug, $slugField = 'slug')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereAvailable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereColorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereDiscountPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereExpiresAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereGroupKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereModelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereNovelty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereOldCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product wherePopular($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product whereVideos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Catalog\Models\Product withTranslation()
 * @method static \Illuminate\Database\Query\Builder|\WezomCms\Catalog\Models\Product withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\WezomCms\Catalog\Models\Product withoutTrashed()
 * @mixin \Eloquent
 * @mixin ProductTranslation
 */
class Product extends EloquentModel implements StorageInterface
{
    use EloquentTentacle;
    use Filterable;
    use Translatable;
    use ProductFlagsTrait;
    use ProductImageTrait;
    use PublishedTrait;
    use SoftDeletes;
    use PurchasedProductTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['published', 'group_key', 'cost', 'old_cost', 'category_id', 'brand_id', 'model_id',
        'novelty', 'popular', 'sale', 'videos', 'type', 'available'];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected $translatedAttributes = ['name', 'slug', 'text', 'title', 'h1', 'keywords', 'description'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'published' => 'bool',
        'novelty' => 'bool',
        'popular' => 'bool',
        'sale' => 'bool',
        'videos' => 'array',
        'available' => 'bool',
        'cost' => 'float',
        'old_cost' => 'float',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['expires_at'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations', 'mainImage'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function model()
    {
        return $this->belongsTo(Model::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function specifications()
    {
        return $this->belongsToMany(Specification::class, 'product_specifications', 'product_id', 'spec_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function specificationsValues()
    {
        return $this->belongsToMany(SpecValue::class, 'product_specifications');
    }

    /**
     * @return Builder|\Illuminate\Database\Eloquent\Relations\HasOneThrough
     */
    public function color()
    {
        return $this->hasOneThrough(
            SpecValue::class,
            ProductSpecification::class,
            'product_id',
            'id',
            'id',
            'spec_value_id'
        )->whereHas('specification', function ($query) {
            $query->whereType(Specification::COLOR);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productSpecifications()
    {
        return $this->hasMany(ProductSpecification::class);
    }

    /**
     * @return HasMany
     */
    public function combinedProducts()
    {
        return $this->hasMany(Product::class, 'group_key', 'group_key')
            ->select('products.*')
            ->whereNotNull('group_key')
            ->with('color', 'mainImage')
            ->published();
    }

    /**
     * @return mixed
     */
    public function publishedSpecifications()
    {
        return $this->specificationsValues()
            ->published()
            ->whereHas('specification', published_scope())
            ->with(['specification' => published_scope()]);
    }

    /**
     * Store a new view.
     */
    public function addView()
    {
        ViewedProducts::add($this->id);
    }

    /**
     * @param  array  $specValues
     */
    public function updateSpecValueRelation($specValues = [])
    {
        $this->productSpecifications()->delete();

        $specValues = array_filter($specValues);
        foreach ($specValues as $specId => $values) {
            foreach (array_filter($values) as $valueId) {
                $this->productSpecifications()->create(['spec_id' => $specId, 'spec_value_id' => $valueId]);
            }
        }
    }

    /**
     * @param  string  $search
     * @param  array  $criterion
     * @param  int  $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function search(string $search = null, array $criterion = [], int $limit = 10)
    {
        $query = Product::query()->withTrashed();

        if ($search) {
            $query->whereTranslationLike('name', '%' . $search . '%');
        }

        if (!empty($criterion)) {
            foreach ($criterion as $field => $value) {
                if (in_array($field, ['category_id'])) {
                    $query->where($field, $value);
                }
            }
        }

        return $query->paginate($limit);
    }

    /**
     * @return string
     */
    public function getFrontUrl()
    {
        return route_localized('catalog.product', [$this->slug, $this->id]);
    }

    /**
     * @return \Illuminate\Support\Collection|Product[]
     */
    public function getVariationsAttribute()
    {
        $result = $this->combinedProducts;

        if ($result->count() < 2) {
            return collect();
        }

        return $result->sortBy('color.sort');
    }

    /**
     * @param  bool  $fullSelection
     * @return mixed
     */
    public function beginSelection(bool $fullSelection = true)
    {
        $query = $this->query()
            ->select('products.*')
            ->published();

        if ($fullSelection) {
            $query->with('combinedProducts')->orderByDesc('available');
        }

        return $query;
    }

    /**
     * @return mixed
     */
    public function beginCount()
    {
        return $this->query()->published();
    }
}
