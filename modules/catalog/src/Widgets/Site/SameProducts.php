<?php

namespace WezomCms\Catalog\Widgets\Site;

use WezomCms\Catalog\Models\Product;
use WezomCms\Core\Foundation\Widgets\AbstractWidget;

class SameProducts extends AbstractWidget
{
    /**
     * View name.
     *
     * @var string
     */
    protected $view = 'cms-catalog::site.widgets.products-carousel';

    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        /** @var Product $product */
        $product = array_get($this->data, 'product');
        if (!$product) {
            return null;
        }

        $products = Product::published()
            ->whereKeyNot($product->id)
            ->where('category_id', $product->category_id)
            ->inRandomOrder()
            ->limit(array_get($this->data, 'limit', 20))
            ->get();

        if ($products->isEmpty()) {
            return null;
        }

        $title = array_get($this->data, 'title', __('cms-catalog::site.products.Same products'));

        return ['result' => $products, 'title' => $title];
    }
}
