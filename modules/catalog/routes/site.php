<?php

use WezomCms\Catalog\Http\Controllers\Site\CatalogController;
use WezomCms\Catalog\Http\Controllers\Site\CategoryController;
use WezomCms\Catalog\Http\Controllers\Site\ProductController;
use WezomCms\Catalog\Http\Controllers\Site\SearchController;

Route::group(
    ['prefix' => 'catalog', 'as' => 'catalog'],
    function () {
        Route::get('', CatalogController::class);

        Route::filter(
            '.category',
            '{slug}/c{id}',
            CategoryController::class,
            ['slug' => '[\pL\pM\pN_-]+', 'id' => '\d+']
        );

        // Product
        Route::get('{slug}/p{id}', ProductController::class)
            ->name('.product')
            ->where(['slug' => '[\pL\pM\pN_-]+', 'id' => '\d+']);
    }
);

// Search
Route::get('search/live', [SearchController::class, 'liveSearch'])->name('search.live');

Route::filter('search', 'search', [SearchController::class, 'index']);
