<?php

use WezomCms\Callbacks\Dashboards;
use WezomCms\Callbacks\Widgets;

return [
    'widgets' => [
        'callbacks:button' => Widgets\CallbackButton::class,
        'callbacks:consultation' => Widgets\Consultation::class,
        'callbacks:quality-control' => Widgets\QualityControl::class,
        'callbacks:request' => Widgets\Request::class,
    ],
    'dashboards' => [
        Dashboards\CallbacksDashboard::class,
        Dashboards\ConsultationsDashboard::class,
        Dashboards\QualityControlDashboard::class,
        Dashboards\RequestDashboard::class,
        //Dashboards\CallbacksNoActiveDashboard::class,
    ],
];
