<div class="row">
    <div class="col-lg-7">
        <div class="card mb-3">
            <div class="card-body">

                <div class="form-group">
                    {!! Form::label('name', __('cms-callbacks::admin.Name')) !!}
                    {!! Form::text('name') !!}
                </div>
                <div class="form-group">
                    {!! Form::label('phone', __('cms-callbacks::admin.Phone')) !!}
                    {!! Form::text('phone') !!}
                </div>
                <div class="form-group">
                    {!! Form::label('time', __('cms-callbacks::admin.Time')) !!}
                    {!! Form::text('time') !!}
                </div>
                <div class="form-group">
                    {!! Form::label('message', __('cms-callbacks::admin.Message')) !!}
                    {!! Form::textarea('message') !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="card mb-3">
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label('read', __('cms-callbacks::admin.Read')) !!}
                    {!! Form::status('read') !!}
                </div>
                <div class="form-group">
                    {!! Form::label('service_id', __('cms-slider::admin.Service')) !!}
                    <div class="input-group">
                        {!! Form::select('service_id', $services, old('service_id', $selectedService), ['class' => 'js-select2']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
