@php
    /**
     * @var $services array
     * @var $service  null|\WezomCms\Services\Models\Service
     */

@endphp

<div class="popup popup--theme-request">
    <div class="popup__title">@lang('cms-callbacks::site.Request title')</div>
    <div class="popup__text">@lang('cms-callbacks::site.Request sub-title')</div>
    @component('cms-ui::components.form.form-ajax', [
            'class' => 'js-import popup__form',
            'method' => 'POST',
            'autocomplete' => 'off',
            'url' => route('callbacks.request'),
            'id' => uniqid('consultation-')
        ])
        @csrf
        <div class="_grid _spacer _spacer--md">
            <div class="_cell _mb-xs _pb-xs _cell--12 _md:cell--6">
                @include('cms-ui::components.form.input', [
                    'name' => 'name',
                    'attributes' => [ 'placeholder' => __('cms-callbacks::site.Name') ],
                    'classes' => 'form-item--theme-default',
                    'mode' => 'text'
                ])
            </div>
            <div class="_cell _mb-xs _pb-xs _cell--12 _md:cell--6">
                @include('cms-ui::components.form.input', [
                    'name' => 'text',
                    'attributes' => [ 'placeholder' => __('cms-callbacks::site.Time') ],
                    'component' => 'input',
                    'classes' => 'form-item--theme-default',
                    'mode' => 'text'
                ])
            </div>
            <div class="_cell _mb-xs _pb-xs _cell--12 _md:cell--6">
                @php
                    $inputmaskConfig = json_encode((object)[
                        'mask' => '+38(999)-99-99-999',
                        'androidMask' => '+38(999)-99-99-999'
                    ])
                @endphp
                @include('cms-ui::components.form.input', [
                    'name' => 'phone',
                    'attributes' => [
                        'placeholder' => __('cms-callbacks::site.Телефон') . ' *',
                        'data-mask' => $inputmaskConfig,
                        'class' => 'js-import form-item__control',
                        'required',
                        'data-rule-phone'
                    ],
                    'component' => 'input',
                    'classes' => 'form-item--theme-default',
                    'mode' => 'tel'
                ])
            </div>
            <div class="_cell _mb-xs _pb-xs _cell--12 _md:cell--6">
                @component('cms-ui::components.form.select2',
                     [
                         'selectOptions' => [
                             'Factory' => 'DefaultSelect2'
                        ],
                        'options' => $services,
                        'name' => uniqid('select2'),
                        'attrs' => ['name' => 'service_id']
                    ]
                )
                    @foreach($services ?? [] as $id => $item)
                        <option value="{{ $id }}">
                            {{ $item }}
                        </option>
                    @endforeach
                @endcomponent
            </div>
            <div class="_cell _mb-xs _pb-xs _cell--12">
                @include('cms-ui::components.form.input', [
                    'name' => 'message',
                    'attributes' => [ 'placeholder' => __('cms-callbacks::site.Message') ],
                    'component' => 'textarea',
                    'classes' => 'form-item--theme-default',
                    'mode' => 'text',
                ])
            </div>
        </div>
        <div class="_flex _flex-column _items-center _mt-df _pt-xxs">
            <div class="popup__description">@lang('cms-callbacks::site.Request description')</div>
            <button type="submit"
                    class="button button--bold button--ls-def button--uppercase button--color-accent button--size-def"
            >
                @lang('cms-callbacks::site.Request button')
            </button>
        </div>
    @endcomponent
</div>
