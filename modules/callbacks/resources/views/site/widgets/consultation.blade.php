<div class="popup  popup--theme-consultation">
    <div class="popup__title">@lang('cms-callbacks::site.Questions title')</div>
    <div class="popup__text">@lang('cms-callbacks::site.Questions description')</div>
    @component('cms-ui::components.form.form-ajax', [
            'method' => 'POST',
            'autocomplete' => 'off',
            'url' => route('callbacks.consultation'),
            'id' => uniqid('consultation-')])
        @csrf
        <div class="_pb-xs _mb-xs">
            @include('cms-ui::components.form.input', [
                'name' => 'name',
                'attributes' => [
                    'placeholder' => __('cms-callbacks::site.Name')
                ],
                'classes' => 'form-item--theme-default',
                'mode' => 'text'
            ])
        </div>
        <div class="_pb-xs _mb-xs">
            @php
                $inputmaskConfig = json_encode((object)[
                    'mask' => '+38(999)-99-99-999',
                    'androidMask' => '+38(999)-99-99-999'
                ])
            @endphp
            @include('cms-ui::components.form.input', [
                'name' => 'phone',
                'attributes' => [
                    'placeholder' => __('cms-callbacks::site.Phone'),
                    'data-mask' => $inputmaskConfig,
                    'class' => 'js-import form-item__control',
                    'required',
                    'data-rule-phone'
                    ],
                'modificators' => [],
                'component' => 'input',
                'classes' => 'form-item--theme-default',
                'mode' => 'tel'
            ])
        </div>
        <div class="_mb-xs _pb-xs">
                @include('cms-ui::components.form.input', [
                    'name' => 'text',
                    'attributes' => [ 'placeholder' => __('cms-callbacks::site.Time') ],
                    'modificators' => [],
                    'component' => 'input',
                    'classes' => 'form-item--theme-default',
                    'mode' => 'text'
                ])
        </div>
        <div class="popup__description">@lang('cms-callbacks::site.Consultation send text')</div>
        <div class="_flex _justify-center">
            <button type="submit"
                    class="button button--bold button--ls-def button--uppercase button--color-accent button--size-def">
                @lang('cms-callbacks::site.Consultation send button')
            </button>
        </div>
    @endcomponent
</div>
