<?php

Route::namespace('WezomCms\\Callbacks\\Http\\Controllers\\Admin')
    ->group(function () {
        Route::adminResource('callbacks', 'CallbacksController')->settings();
        Route::adminResource('callback-consultations', 'ConsultationController');
        Route::adminResource('callback-quality-controls', 'QualityControlController');
        Route::adminResource('callback-requests', 'RequestController');
        Route::adminResource('callback-insurances', 'InsuranceController');
    });
