<?php

use WezomCms\Core\Http\Middleware\CheckForSpam;

Route::namespace('WezomCms\\Callbacks\\Http\\Controllers\\Site')
    ->group(function () {
        Route::get('popups/callback', 'CallbackController@popup')->name('callbacks.popup');
        Route::get('popups/callback-request', 'CallbackController@popupRequest')->name('callbacks.request-popup');
        Route::get('popups/callback-insurance', 'CallbackController@popupInsurance')->name('callbacks.insurance-popup');

        Route::post('callback', 'CallbackController@sendForm')
            ->middleware([CheckForSpam::class, 'form_throttle'])
            ->name('callbacks.send-form');

        Route::post('callback-consultations', 'CallbackController@sendConsultation')
            ->middleware([CheckForSpam::class, 'form_throttle'])
            ->name('callbacks.consultation');

        Route::post('callback-quality-controls', 'CallbackController@sendQualityControl')
            ->middleware([CheckForSpam::class, 'form_throttle'])
            ->name('callbacks.quality-control');

        Route::post('callback-requests', 'CallbackController@sendRequest')
            ->middleware([CheckForSpam::class, 'form_throttle'])
            ->name('callbacks.request');

        Route::post('callback-insurance', 'CallbackController@sendInsurance')
            ->middleware([CheckForSpam::class, 'form_throttle'])
            ->name('callbacks.insurance');
    });
