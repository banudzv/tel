<?php

namespace WezomCms\Callbacks\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use WezomCms\Callbacks\Models\Callback;
use WezomCms\Callbacks\Models\Consultation;
use WezomCms\Callbacks\Models\Insurance;
use WezomCms\Callbacks\Models\QualityControl;

class InsuranceNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Insurance
     */
    private $insurance;

    /**
     * Create a new notification instance.
     *
     * @param  Insurance  $insurance
     */
    public function __construct(Insurance $insurance)
    {
        $this->insurance = $insurance;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            'mail',
            'database'
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject(__('cms-callbacks::admin.email.New Insurance order'))
            ->markdown('cms-callbacks::admin.notifications.email', [
                'order' => $this->insurance,
                'name' => __('cms-callbacks::admin.email.New Insurance order'),
                'urlToAdmin' => route('admin.callback-insurances.edit', $this->insurance->id),
            ])->from('nimych.a.wezom@gmail.com');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'route_name' => 'admin.callback-insurances.edit',
            'route_params' => $this->insurance->id,
            'icon' => 'fa-phone',
            'color' => 'success',
            'heading' => __('cms-callbacks::admin.Insurance'),
            'description' => implode('. ', array_filter([$this->insurance->phone])),
        ];
    }
}
