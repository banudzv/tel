<?php

namespace WezomCms\Callbacks\Widgets;

use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\Services\Repositories\ServiceRepository;

class QualityControl extends AbstractWidget
{
    /**
     * View name.
     *
     * @var string|null
     */
    protected $view = 'cms-callbacks::site.widgets.quality-control';

    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        $repository = resolve(ServiceRepository::class);

        $services = $repository->getBySelect();

        return compact('services');
    }
}
