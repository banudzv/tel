<?php

namespace WezomCms\Callbacks\Widgets;

use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\Services\Repositories\ServiceRepository;

class Request extends AbstractWidget
{
    /**
     * View name.
     *
     * @var string|null
     */
    protected $view = 'cms-callbacks::site.widgets.request';

    /**
     * @return array|null
     */
    public function execute(): ?array
    {

        $service = null;

        $repository = resolve(ServiceRepository::class);

        $services = $repository->getBySelect();

        if(isset($this->data['service']) && !empty($this->data['service'])){
            $service = $this->data['service'];
        }


        return compact('services', 'service');
    }
}
