<div class="row">
    <div class="col-lg-7">
        <div class="card mb-3">
            <div class="card-body">
                @langTabs
                <div class="form-group">
                    {!! Form::label($locale . '[name]', __('cms-benefits::admin.Name')) !!}
                    {!! Form::textarea($locale . '[name]', old($locale . '.name', $obj->translateOrNew($locale)->name), ['class' => 'js-wysiwyg']) !!}
{{--                    {!! Form::textarea($locale . '[name]', old($locale . '.name', $obj->translateOrNew($locale)->name)) !!}--}}
                </div>
                @endLangTabs
            </div>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="card mb-3">
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label(str_slug('published'), __('cms-core::admin.layout.Published')) !!}
                    {!! Form::status('published', old('published', $obj->exists ? $obj->published : true))  !!}
                </div>
                <div class="form-group">
                    {!! Form::label(str_slug('for_main'), __('cms-benefits::admin.For main')) !!}
                    {!! Form::status('for_main', old('for_main', $obj->exists ? $obj->for_main : true))  !!}
                </div>
                <div class="form-group">
                    {!! Form::label(str_slug('icon'), __('cms-core::admin.layout.Icon')) !!}
                    <div class="input-group">
                        {!! Form::select('icon', \WezomCms\Core\Foundation\Icon::getForSelect(), old('icon', $obj->icon), ['class' => 'js-select2']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
