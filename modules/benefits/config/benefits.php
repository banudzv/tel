<?php

use \WezomCms\Benefits\Widgets;

return [
    'widgets' => [
    	'benefits' => Widgets\Benefits::class
	]
];
