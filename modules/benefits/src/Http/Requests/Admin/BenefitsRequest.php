<?php

namespace WezomCms\Benefits\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Http\Requests\ChangeStatus\RequiredIfMessageTrait;
use WezomCms\Core\Traits\LocalizedRequestTrait;

class BenefitsRequest extends FormRequest
{
	use LocalizedRequestTrait;
	use RequiredIfMessageTrait;

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return $this->localizeRules(
			[
				'name' => 'required|string|max:255',
			],
			[
				'icon' => 'required|string',
				'published' => 'nullable',
				'for_main' => 'boolean',
				'sort' => 'nullable|integer',
			]
		);
	}

	/**
	 * Get custom attributes for validator errors.
	 *
	 * @return array
	 */
	public function attributes()
	{
		return $this->localizeAttributes(
			[
				'name' => __('cms-benefits::admin.Name'),
			],
			[
				'sort' => __('cms-core::admin.layout.Position'),
				'published' => __('cms-core::admin.layout.Published'),
				'icon' => __('cms-core::admin.layout.Icon'),
			]
		);
	}
}
