<?php

Route::namespace('WezomCms\\SeoRedirects\\Http\\Controllers')->group(function () {
    Route::adminResource('seo-redirects', 'RedirectsController');
    Route::get('seo-redirects/import', 'ImportRedirectsController@form')->name('seo-redirects.form');
    Route::post('seo-redirects/import', 'ImportRedirectsController@import')->name('seo-redirects.import');
});
