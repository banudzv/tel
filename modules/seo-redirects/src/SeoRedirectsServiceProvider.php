<?php

namespace WezomCms\SeoRedirects;

use SidebarMenu;
use WezomCms\Core\BaseServiceProvider;
use WezomCms\Core\Contracts\PermissionsContainerInterface;
use WezomCms\Core\Traits\SidebarMenuGroupsTrait;
use WezomCms\SeoRedirects\Http\Middleware\SeoRedirectMiddleware;

class SeoRedirectsServiceProvider extends BaseServiceProvider
{
    use SidebarMenuGroupsTrait;

    protected function afterBoot()
    {
        app('router')->pushMiddlewareToGroup('web', SeoRedirectMiddleware::class);
    }

    /**
     * @param  PermissionsContainerInterface  $permissions
     */
    public function permissions(PermissionsContainerInterface $permissions)
    {
        $permissions->add('seo-redirects', __('cms-seo-redirects::admin.SEO redirects'));
        $permissions->addItem('seo-redirects.import', __('cms-seo-redirects::admin.Import'));
    }

    public function adminMenu()
    {
        $seo = SidebarMenu::get('seo');
        if (!$seo) {
            $seo = $this->serviceGroup()
                ->add(__('cms-seo-redirects::admin.SEO'))
                ->data('position', 50)
                ->data('icon', 'fa-line-chart')
                ->nickname('seo');
        }

        $seo->add(__('cms-seo-redirects::admin.Redirects'), route('admin.seo-redirects.index'))
            ->data('permission', 'seo-redirects.view')
            ->data('icon', 'fa-exchange')
            ->data('position', 2)
            ->nickname('redirects');
    }
}
