<?php

namespace WezomCms\News\Widgets;

use Illuminate\Support\Collection;
use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\News\Models\News;

class LatestNews extends AbstractWidget
{
    /**
     * A list of models that, when changed, will clear the cache of this widget.
     *
     * @var array
     */
    public static $models = [News::class];
    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        /** @var Collection $result */
        $result = News::published()
            ->orderByDesc('published_at')
            ->latest('id')
            ->limit(array_get($this->data, 'limit', 3))
            ->get();

        if ($result->isEmpty()) {
            return null;
        }

        return compact('result');
    }
}
