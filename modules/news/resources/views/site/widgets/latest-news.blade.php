@php
    /**
     * @var $result \Illuminate\Database\Eloquent\Collection|\WezomCms\News\Models\News[]
     */
@endphp
@foreach($result as $key => $obj)
    <div>
        <a href="{{ $obj->getFrontUrl() }}">
            <img class="lozad js-import" src="{{ url('assets/images/empty.gif') }}"
                 data-lozad="{{ $obj->getImageUrl() }}" alt="{{ $obj->name }}">
        </a>
        <a href="{{ $obj->getFrontUrl() }}">{{ $obj->name }}</a>
        <div>{{ $obj->published_at->format('d.m.Y') }}</div>
        <div>{{ str_limit(strip_tags($obj->text), 120, '...') }}</div>
    </div>
@endforeach
<a href="{{ route('news') }}">@lang('cms-news::site.Еще')</a>
