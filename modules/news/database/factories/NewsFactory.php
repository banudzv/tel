<?php

use Faker\Generator as Faker;
use WezomCms\News\Models\News;

$factory->define(News::class, function (Faker $faker) {
    $name = $faker->realText(50);

    return [
        'published_at' => $faker->dateTimeBetween('-10 days', 'now'),
        'published' => $faker->boolean(80),
        'name' => $name,
        'h1' => $name,
        'title' => $name,
        'text' => $faker->realText(1000),
    ];
});
