<?php

Route::namespace('WezomCms\\News\\Http\\Controllers\\Site')
    ->group(function () {
        Route::get('news', 'NewsController@index')->name('news');

        Route::get('tag/{tag}', 'NewsController@tag')->name('news.tag');

        Route::get('news/{slug}', 'NewsController@inner')->name('news.inner');
    });
