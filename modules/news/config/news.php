<?php

use WezomCms\News\Dashboards;
use WezomCms\News\Widgets;

return [
    'use_tags' => true,
    'images' => [
        'directory' => 'news',
        'default' => 'medium',
        'sizes' => [
            'medium' => [
                'width' => 370,
                'height' => 208,
                'mode' => 'fit',
                'watermark' => true,
            ],
        ],
    ],
    'widgets' => [
        'news:most-viewed' => Widgets\MostViewed::class,
        'news:latest' => Widgets\LatestNews::class,
        'news:tags' => Widgets\Tags::class,
    ],
    'sitemap' => [
        'news' => true, // Enable/disable render links to all published news in sitemap page.
        'tags' => true // Enable/disable render links to all published news tags in sitemap page.
    ],
    'dashboards' => [
        Dashboards\NewsDashboard::class,
    ]
];
