<?php

namespace WezomCms\Ui\Http\Controllers\Site;

use Illuminate\Http\Response;
use WezomCms\Core\Http\Controllers\SiteController;

class UiPageController extends SiteController
{
	/**
	 * @return Response
	 */
	public function index()
	{
		return response()->view('cms-ui::ui-page', []);
	}
	/**
	 * @return Response
	 */
	public function searchResults()
	{
		return response()->view('cms-ui::search-results', []);
	}
	/**
	 * @return Response
	 */
	public function sitemap()
	{
		return response()->view('cms-ui::sitemap', []);
	}
}
