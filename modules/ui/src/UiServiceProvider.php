<?php

namespace WezomCms\Ui;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use WezomCms\Core\BaseServiceProvider;
use WezomCms\Ui\Listeners\AddBreadcrumbsMicroData;

class UiServiceProvider extends BaseServiceProvider
{
    /**
     * All module widgets.
     *
     * @var array|string|null
     */
    protected $widgets = 'cms.ui.ui.widgets';

    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'assets: js-site-head' => [
            AddBreadcrumbsMicroData::class,
        ],
    ];

    /**
     * Load module views.
     */
    protected function views()
    {
        parent::views();

        if (!$this->app['isBackend']) {
            $paginationViews = config('cms.ui.ui.pagination', []);

            // Default view
            if ($defaultView = array_get($paginationViews, 'default')) {
                Paginator::defaultView($defaultView);
            }

            // Simple view
            if ($simpleView = array_get($paginationViews, 'simple')) {
                Paginator::defaultSimpleView($simpleView);
            }
        }

        Blade::directive('svg', function ($arguments) {
            return '<?php echo svg(' . $arguments . '); ?>';
        });

        Blade::directive('svgAll', function ($arguments) {
            return '<?php echo svgAll(' . $arguments . '); ?>';
        });

        Blade::directive('emptyResult', function ($text) {
            if ($text) {
                return "<?php echo app('view')->make('cms-ui::empty-result', ['text' => $text]); ?>";
            }
            return "<?php echo app('view')->make('cms-ui::empty-result', ['text' => null]); ?>";
        });
    }
}
