<?php

namespace WezomCms\Ui\Widgets;

use WezomCms\Core\Foundation\Widgets\AbstractWidget;

class SearchBlock extends AbstractWidget
{
	/**
	 * View name.
	 *
	 * @var string|null
	 */
	protected $view = 'cms-ui::widgets.search-block';
}
