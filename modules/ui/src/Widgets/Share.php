<?php

namespace WezomCms\Ui\Widgets;

use SEO;
use URL;
use WezomCms\Core\Foundation\Widgets\AbstractWidget;

class Share extends AbstractWidget
{
    /**
     * View name.
     *
     * @var string|null
     */
    protected $view = 'cms-ui::widgets.share';

    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        return [
            'title' => array_get($this->data, 'title') ?: SEO::getTitle(),
            'url' => array_get($this->data, 'url') ?: URL::current(),
        ];
    }
}
