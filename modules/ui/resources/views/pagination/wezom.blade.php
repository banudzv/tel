@php
    /**
     * @var $paginator \Illuminate\Pagination\LengthAwarePaginator|\Illuminate\Contracts\Pagination\Paginator
     * @var $elements array
     */
@endphp
@if ($paginator->hasPages())
    <div class="pagination" role="navigation">
        {{-- Previous Page Link --}}
        @if (!$paginator->onFirstPage())
            <a href="{{ $paginator->previousPageUrl() }}" class="pagination__arrow pagination__arrow--prev"
               rel="prev">
                @svg('arrow-left-thick')
            </a>
        @else
            <span class="pagination__arrow pagination__arrow--prev disabled"
               rel="prev">
                @svg('arrow-left-thick')
            </span>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <span class='pagination__number'>...</span>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a class="pagination__number pagination__number--current"><span>{{ $page }}</span></a>
                    @else
                        <a href="{{ $url }}" class="pagination__number">
                            <span>{{ $page }}</span>
                        </a>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" class="pagination__arrow pagination__arrow--next"
               rel="next">
                @svg('arrow-right-thick')
            </a>
        @else
            <span class="pagination__arrow pagination__arrow--next disabled"
               rel="next">
                @svg('arrow-right-thick')
            </span>
        @endif
    </div>
@endif
