@extends('cms-ui::layouts.base', ['lightHeader' => true])

@section('main')
    <div class="section section--sitemap">
        <div class="container container--theme-default">
            <div class="title title--size-h2 _text-center">
                Карта сайта
            </div>
            <div class="sitemap">
                <ul>
                    @for($i = 0; $i < 10; $i++)
                        <li>
                            <a href="#">пункт {{ $i }}</a>
                            @if($i === 3)
                                <ul>
                                    @for($_i = 0; $_i < 5; $_i ++)
                                        <li>
                                            <a href="#">подпункт {{ $_i }}</a>
                                            @if($_i === 3)
                                                <ul>
                                                    @for($_i = 0; $_i < 5; $_i ++)
                                                        <li>
                                                            <a href="#">подпункт {{ $_i }}</a>
                                                        </li>
                                                    @endfor
                                                </ul>
                                            @endif
                                        </li>
                                    @endfor
                                </ul>
                            @endif
                        </li>
                    @endfor
                </ul>
            </div>
        </div>
    </div>
    <div class="section section--questions">
        <div class="section__decor" data-scroll-window="#consultation">
            <span>@lang('cms-core::site.or')</span>
            @svg('arrow-bottom-lg')
        </div>
        <div class="container">
            <div class="questions">
                <div class="_grid _justify-center">
                    <div class="_cell _cell--12 _md:cell--10 _df:cell-8 _lg:cell--6 _flex _flex-column _items-center">
                        <div class="questions__title">
                            @lang('cms-services::site.Hotline title')
                        </div>
                        {{-- @todo вывести $hotline, заменить блок ниже--}}
                        <a href="tel:+380000000000" class="questions__link">
                            +380000000000
                        </a>
{{--                        <a href="tel:{{ $hotline }}" class="questions__link">--}}
{{--                            {{ $hotline }}--}}
{{--                        </a>--}}
                        <div class="questions__button">
                            @widget('ui:button', [
                            'component' => 'button',
                            'classes' => 'js-import',
                            'attrs' => [
                            'type' => 'button',
                            'data-mfp' => 'ajax',
                            'data-mfp-src' => route('callbacks.request-popup')
                            ],
                            'text' => __('cms-callbacks::site.send order by contract'),
                            'modificators' => [
                            'color-accent',
                            'size-def',
                            'uppercase',
                            'ls-def',
                            'bold'
                            ]
                            ])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section--consultation" id="consultation">
        <div class="consultation-block">
            <div class="container">
                <div class="_grid _spacer _spacer--md _justify-center">
                    <div class="_cell--12 _lg:cell--10">
                        <div class="_flex _flex-column _items-center">
                            <div class="consultation-block__title">
                                @lang('cms-callbacks::site.Consultation send button')
                            </div>
                            <div class="consultation-block__text">
                                @lang('cms-callbacks::site.Questions description with br')
                            </div>
                        </div>
                        @component('cms-ui::components.form.form-ajax', [
                            'class' => 'js-import',
                            'method' => 'POST',
                            'autocomplete' => 'off',
                            'url' => route('callbacks.consultation'),
                            'id' => uniqid('consultation-')])
                            @csrf
                            <div class="_grid _spacer _spacer--md _df:mb-none _mb-xs">
                                <div class="_cell--12 _df:cell--4">
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'name',
                                        'attributes' => [ 'placeholder' => __('cms-callbacks::site.Name') ],
                                        'modificators' => ['inactive'],
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'text'
                                    ])
                                </div>
                                <div class="_cell--12 _df:cell--4">
                                    @php
                                        $inputmaskConfig = json_encode((object)[
                                            'mask' => '+38(999)-99-99-999',
                                            'androidMask' => '+38(999)-99-99-999'
                                        ])
                                    @endphp
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'phone',
                                        'attributes' => [
                                            'placeholder' => __('cms-callbacks::site.Phone'),
                                            'data-mask' => $inputmaskConfig,
                                            'class' => 'js-import form-item__control',
                                            'required',
                                            'data-rule-phone'
                                            ],
                                        'modificators' => [],
                                        'component' => 'input',
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'tel'
                                    ])
                                </div>
                                <div class="_cell--12 _df:cell--4">
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'text',
                                        'attributes' => [ 'placeholder' => __('cms-callbacks::site.Time') ],
                                        'modificators' => [],
                                        'component' => 'input',
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'text'
                                    ])
                                </div>
                            </div>
                            <div class="_flex _flex-column _items-center _df:mt-sm _df:pt-xxs">
                                <button type="submit"
                                        class="button button--bold button--ls-def button--uppercase button--color-accent button--size-def">
                                    @lang('cms-callbacks::site.Consultation send button')
                                </button>
                                <div class="consultation-block__description">@lang('cms-callbacks::site.Consultation send text')</div>
                            </div>
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
