<!doctype html>
<html lang="{{ app()->getLocale() }}" dir="ltr">
<head>
    @include('cms-ui::partials.head.head')
    <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-M5Z8WS6');</script>
        <!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M5Z8WS6"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="page
    {{ isset($lightHeader) && $lightHeader ? 'page--light-header' : '' }}
    {{ isset($staticHeader) && $staticHeader ? 'page--static-header' : '' }}"
>
    @if(\WezomAgency\Browserizr::detect()->isDesktop())
        @widget('ui:header', [
            'lightHeader' => isset($lightHeader) && $lightHeader ?? false,
            'staticHeader' => isset($staticHeader) && $staticHeader ?? false,
        ])
        @widget('ui:mobile-header', [
            'class' => '_lg:hide',
            'lightHeader' => isset($lightHeader) && $lightHeader ?? false,
            'staticHeader' => isset($staticHeader) && $staticHeader ?? false,
        ])
    @else
        @widget('ui:mobile-header', [
            'lightHeader' => isset($lightHeader) && $lightHeader ?? false,
            'staticHeader' => isset($staticHeader) && $staticHeader ?? false,
        ])
    @endif
    <div class="main" role="main">
        @yield('main')
    </div>
    @widget('ui:footer')
    @widget('ui:socials-widget', ['fb_messenger' => '#', 'viber' => '#'])
</div>
@widget('ui:hidden-data')
</body>
</html>
