<div class="popup popup--theme-response popup--message-success">
    <div class="title title--size-h2 _mb-xl _pb-def _text-center ">{{ $title }}</div>
    @if ($text)
        <div class="popup__text">{{ $text }}</div>
    @endif
    <div class="_flex _justify-center">
        @widget('ui:button', ['component' => 'button', 'attrs' => ['type' => 'button', 'data-mfp-close'], 'text' => 'OK', 'modificators' => ['color-accent', 'size-def', 'uppercase', 'ls-def', 'bold']])
    </div>
</div>

