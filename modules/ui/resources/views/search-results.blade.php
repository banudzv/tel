@extends('cms-ui::layouts.base', ['lightHeader' => true])

@php
    $services = [
        (object)[
            'icon' => 'civil',
            'title' => 'Страхование гражданской ответственности',
            'text' => 'Позволяет защитить свои имущественные интересы, связанные с данной обязанностью и означает, что страховая компания возмещает ущерб, неумышленно причиненный работниками страхователя третьим лицам.',
            'href' => '#'
        ],
        (object)[
            'icon' => 'building',
            'title' => 'Страхование строительно-монтажных работ',
            'text' => 'При проведении строительно-монтажных работ (СМР) застройщик всегда сталкивается с риском возникновения непредвиденных ситуаций, которые могут привести к повреждению или утрате объекта строительства, строительных материалов ',
            'href' => '#'
        ],
        (object)[
            'icon' => 'special-vehicles',
            'title' => 'Страхование спецтехники',
            'text' => 'Даже самая высокая квалификация специалистов не может полностью обезопасить строительную организацию от возможности нанесения при осуществлении своей деятельности вреда имуществу, жизни и здоровью граждан',
            'href' => '#'
        ],
        (object)[
            'icon' => 'mortgage',
            'title' => 'Обязательное страхование ипотеки',
            'text' => 'Предмет ипотеки: недвижимое имущество (недвижимость, земельные участки) Договор обязательного страхования предмета ипотеки заключается по типовой форме договора, установленной Постановлением КМУ.',
            'href' => '#'
        ],
        (object)[
            'icon' => 'civil',
            'title' => 'Страхование гражданской ответственности',
            'text' => 'Позволяет защитить свои имущественные интересы, связанные с данной обязанностью и означает, что страховая компания возмещает ущерб, неумышленно причиненный работниками страхователя третьим лицам.',
            'href' => '#'
        ],
        (object)[
            'icon' => 'building',
            'title' => 'Страхование строительно-монтажных работ',
            'text' => 'При проведении строительно-монтажных работ (СМР) застройщик всегда сталкивается с риском возникновения непредвиденных ситуаций, которые могут привести к повреждению или утрате объекта строительства, строительных материалов ',
            'href' => '#'
        ],
        (object)[
            'icon' => 'special-vehicles',
            'title' => 'Страхование спецтехники',
            'text' => 'Даже самая высокая квалификация специалистов не может полностью обезопасить строительную организацию от возможности нанесения при осуществлении своей деятельности вреда имуществу, жизни и здоровью граждан',
            'href' => '#'
        ],
        (object)[
            'icon' => 'mortgage',
            'title' => 'Обязательное страхование ипотеки',
            'text' => 'Предмет ипотеки: недвижимое имущество (недвижимость, земельные участки) Договор обязательного страхования предмета ипотеки заключается по типовой форме договора, установленной Постановлением КМУ.',
            'href' => '#'
        ]
    ];

    $news = [
        (object)[
            'img' => url('/static/images/placeholders/no-image.png'),
            'text' => 'СК "Теком" выплатила более 830 тыс.грн. за поврежденный специальный автомобиль',
            'date' => '27 сентября'
        ],
        (object)[
            'img' => url('/static/images/placeholders/no-image.png'),
            'text' => 'СК "Теком" выплатила более 830 тыс.грн. за поврежденный специальный автомобиль',
            'date' => '27 сентября'
        ],
        (object)[
            'img' => url('/static/images/placeholders/no-image.png'),
            'text' => 'СК "Теком" выплатила более 830 тыс.грн. за поврежденный специальный автомобиль',
            'date' => '27 сентября'
        ],
        (object)[
            'img' => url('/static/images/placeholders/no-image.png'),
            'text' => 'СК "Теком" выплатила более 830 тыс.грн. за поврежденный специальный автомобиль',
            'date' => '27 сентября'
        ],
        (object)[
            'img' => url('/static/images/placeholders/no-image.png'),
            'text' => 'СК "Теком" выплатила более 830 тыс.грн. за поврежденный специальный автомобиль',
            'date' => '27 сентября'
        ],
        (object)[
            'img' => url('/static/images/placeholders/no-image.png'),
            'text' => 'СК "Теком" выплатила более 830 тыс.грн. за поврежденный специальный автомобиль',
            'date' => '27 сентября'
        ],
        (object)[
            'img' => url('/static/images/placeholders/no-image.png'),
            'text' => 'СК "Теком" выплатила более 830 тыс.грн. за поврежденный специальный автомобиль',
            'date' => '27 сентября'
        ],
        (object)[
            'img' => url('/static/images/placeholders/no-image.png'),
            'text' => 'СК "Теком" выплатила более 830 тыс.грн. за поврежденный специальный автомобиль',
            'date' => '27 сентября'
        ],
    ];
@endphp

@section('main')
    <div class="section section--search-results">
        <div class="search-results">
            <div class="container">
                <div class="search-results__title _mb-sm _md:pb-lg">
                    По вашему запросу “<span>страхование</span>” найдено 16
                </div>
                <div class="search-results__subtitle">
                    В услугах
                </div>
                <div class="_grid _spacer _spacer--md _justify-center _md:justify-start">
                    @foreach($services as $service)
                        <div class="_cell _cell--12 _sm:cell--8 _md:cell--6 _df:cell--4 _lg:cell--3">
                            <div class="service-card service-card--theme-related">
                                <div class="service-card__body">
                                    <div class="service-card__top">
                                        <div class="service-card__icon">
                                            @svg($service->icon)
                                        </div>
                                        <div class="service-card__title">
                                            {{ $service->title }}
                                        </div>
                                    </div>
                                    <div class="service-card__text">
                                        {!! $service->text !!}
                                    </div>
                                </div>
                                <div class="_flex _justify-start">
                                    @widget('ui:button', [
                                        'component' => 'a',
                                        'attrs' => [
                                            'href' => $service->href
                                        ],
                                        'text' => 'подробнее',
                                        'modificators' => [
                                            'color-accent',
                                            'size-md',
                                            'uppercase',
                                            'ls-def',
                                            'bold'
                                        ]
                                    ])
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="search-results__subtitle _md:pt-lg _mt-sm">
                    В новостях
                </div>
                <div class="_grid _spacer _spacer--md _justify-center _md:justify-start">
                    @foreach($news as $news_item)
                        <div class="_cell _cell--12 _sm:cell--8 _md:cell--6 _df:cell--4 _lg:cell--3">
                            @include('cms-ui::widgets.blog-item', [
                                   'img' => $news_item->img,
                                   'text' => $news_item->text,
                                   'date' => $news_item->date,
                                   'href' => '#',
                                   'class' => 'blog-item--theme-white'
                              ])
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="_mb-df">
        @include('cms-ui::widgets.feedback-form')
    </div>
@endsection
