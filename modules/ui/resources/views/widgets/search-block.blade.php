@php
    $_classes = isset($classes) ? $classes : '';
    $_button = isset($button) && $button ?? false;
@endphp

@component('cms-ui::components.form.form-ajax', ['route' => 'demo.form', 'class' => 'search-block ' . $_classes, 'id' => uniqid('form-')])
    <label for="search" class="js-import" data-form-item='{!! json_encode(['blurTimeout' => 300, 'triggerFocus' => true]) !!}'>
        <input id="search" name="search-field" type="search" class="search-block__input">
        @if($_button)
            <button type="submit" class="search-block__button">
                <svg class="search-block__icon" width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle transform="rotate(45) translate(4.5, -10.5)" class="search-block__icon-circle" stroke="#fff" stroke-width="1.3" cx="10.2" cy="10.5" r="9.6"/>
                    <line transform="rotate(180) translate(-43.5, -43.5)" class="search-block__icon-line" stroke="#fff" stroke-width="1.3" x1="18" y1="17.3" x2="26.5" y2="25.8"/>
                </svg>
            </button>
        @else
            <svg class="search-block__icon" width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle transform="rotate(45) translate(4.5, -10.5)" class="search-block__icon-circle" stroke="#fff" stroke-width="1.3" cx="10.2" cy="10.5" r="9.6"/>
                <line transform="rotate(180) translate(-43.5, -43.5)" class="search-block__icon-line" stroke="#fff" stroke-width="1.3" x1="18" y1="17.3" x2="26.5" y2="25.8"/>
            </svg>
        @endif
    </label>
@endcomponent
