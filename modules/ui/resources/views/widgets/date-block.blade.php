<div class="date-block {{$classDate ?? ''}}">
    @svg('clock', 27, 27, 'date-block__icon')
    <span class="date-block__text">{{$date ?? ''}}</span>
</div>
