<div class="blog-item {{$class ?? ''}}">
    <a {!! $href ? sprintf("href=\"%s\"", $href) : null !!} class="blog-item__img">
        <img src="{!! asset($img) !!}" alt="">
    </a>
   <div class="blog-item__body">
       <a {!! $href ? sprintf("href=\"%s\"", $href) : null !!} class="blog-item__text _mb-sm">{{ $text ?? '' }}</a>
         @include('cms-ui::widgets.date-block',[
             'classDate' => '_pt-sm _mt-auto',
             'date' => $date
         ])
   </div>
</div>
