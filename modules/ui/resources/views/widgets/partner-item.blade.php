<a href="" class="partner-item {{$class ?? ''}}">
    <div class="partner-item__img">
        <img src="{!! asset($img) !!}" width="170" height="170" alt="">
    </div>
    <span class="partner-item__text">{{ $name }}</span>
</a>
