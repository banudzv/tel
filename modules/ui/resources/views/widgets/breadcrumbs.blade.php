@php
    /**
     * @var $items Illuminate\Support\Collection
     */
@endphp
<div class="breadcrumbs">
    @foreach($items as $key => $item)
        <a class="breadcrumbs__link" @if(!$loop->last)href="{{ url($item['link']) }}"@endif>{{ $item['name'] }}</a>
    @endforeach
</div>
