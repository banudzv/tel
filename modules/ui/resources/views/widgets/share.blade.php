@php
/**
 * @var $title string|null
 * @var $url string|null
 */
@endphp
<ul class="share-links">
    <li class="share-links__item">
        <a href="http://www.facebook.com/sharer.php?u={{ $url }}&quote={{ $title }}" class="share-links__link"
           onclick="shareEvent(event, this);" target="_blank" rel="nofollow">
            @svg('facebook', 10, 18)
        </a>
    </li>
    <li class="share-links__item">
        <a href="https://t.me/share/url?url={{ $url }}&text={{ $title }}&to=" class="share-links__link"
           onclick="shareEvent(event, this);" target="_blank" rel="nofollow">
            @svg('telegram', 15, 13)
        </a>
    </li>
    <li class="share-links__item">
        <a href="https://twitter.com/intent/tweet?url={{ $url }}&text={{ $title }}" class="share-links__link"
           onclick="shareEvent(event, this);" target="_blank" rel="nofollow">
            @svg('twitter', 16, 11)
        </a>
    </li>
    <li class="share-links__item">
        <a href="https://www.linkedin.com/sharing/share-offsite/?url={{ $url }}&title={{ $title }}"
           class="share-links__link" onclick="shareEvent(event, this);" target="_blank" rel="nofollow">
            @svg('linkedin', 12, 10)
        </a>
    </li>
</ul>
