<footer class="footer">
    <div class="container">
        <div class="footer__body">
            <div class="_grid _spacer _spacer--md _justify-center">
                <div class="_cell _cell--10 _df:cell--4 _flex _justify-center _lg:cell--2 _md:cell--4 _sm:cell--6 _sm:justify-start">
                    <div>
                        @if(Request::is('/'))
                            <div class="logo logo--footer">
                                <svg>
                                    <use xlink:href="#logo-symbol"></use>
                                </svg>
                            </div>
                        @else
                            <a class="logo logo--footer" href="{{ route('home') }}">
                                <svg>
                                    <use xlink:href="#logo-symbol"></use>
                                </svg>
                            </a>
                        @endif
                        <div class="apps">
                            <div class="apps__title">
                                @lang('cms-ui::site.Загрузи мобильное приложение'):
                            </div>
                            <div class="apps__images">
                                <a href="#" target="_blank" rel="nofollow" class="apps__item">
                                    <img data-src="{{ url('/static/images/app-store-ru.png') }}"
                                         class="js-import lozad fade-in"
                                         src="{{ url('/images/empty.gif') }}"
                                         width="111"
                                         height="34"
                                         alt="">
                                </a>
                                <a href="#" target="_blank" rel="nofollow" class="apps__item">
                                    <img data-src="{{ url('/static/images/google-play-ru.png') }}"
                                         class="js-import lozad fade-in"
                                         src="{{ url('/images/empty.gif') }}"
                                         width="111"
                                         height="34"
                                         alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="_cell _cell--6 _lg:cell--7 _lg:show">
                    @widget('menu', ['position' => 'footer'], 'cms-menu::site.widgets.footer')
                </div>
                <div class="_cell _cell--10 _flex _justify-center _lg:cell--3 _md:cell--8 _sm:cell--6 _sm:justify-start">
                    @widget('newsletter:subscribe')
                    <div class="contacts-block contacts-block--footer">
                        <div class="contacts-block__hotline">
                            @widget('contacts:hotline', ['icon' => true, 'classes' => 'hotline--theme-menu _md:show'])
                            @widget('ui:button', ['component' => 'button', 'classes' => '_mt-md _mb-sm _md:show js-import', 'attrs' => ['type' => 'button', 'data-mfp' => 'ajax', 'data-mfp-src' => route('callbacks.popup')], 'text' => __('cms-ui::site.callback'), 'modificators' => ['color-light-transparent', 'size-def', 'uppercase', 'ls-def', 'bold']])
                        </div>
                        @widget('branch:main-contact', ['modifier' => 'main-contacts--footer'])
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="_grid _spacer _spacer--md _justify-between">
                <div class="_cell _cell--12 _df:cell--4">
                    <div class="copyright">
                        &copy; 2011-{{ now()->year }}, @lang('cms-core::site.copyright')
                    </div>
                </div>
                <div class="_cell _cell--6 _df:cell--4 _flex _df:justify-center _items-center">
                    @widget('ui:button', ['component' => 'a', 'attrs' => [
                                'href' => \WezomCms\Contacts\Models\Contact::socialLink('facebook'), 'rel' => 'nofollow'], 'icon' => ['facebook', 9, 18], 'classes' => '_mr-xs', 'modificators' => ['theme-social button--theme-social-facebook']])
                    @widget('ui:button', ['component' => 'a', 'attrs' => [
                                'href' => \WezomCms\Contacts\Models\Contact::socialLink('telegram'), 'rel' => 'nofollow'], 'icon' => ['telegram', 14, 12, false, '_nml-xxs'], 'modificators' => ['theme-social button--theme-social-telegram']])
                </div>
                <div class="_cell _cell--6 _df:cell--4 _flex _justify-end _items-center">
                    <a href="https://wezom.com.ua"
                       target="_blank"
                       class="link link--theme-wezom">
                        @lang('cms-ui::site.Сделано в') <span>&nbsp;Wezom</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>
