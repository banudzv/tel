@extends('cms-ui::layouts.error')

@section('code', '429')

@section('message', __('cms-ui::site.many requests to our servers'))
