@extends('cms-ui::layouts.error')

@section('code', '403')

@section('message', __('cms-ui::site.forbidden to use this page'))
