@extends('cms-ui::layouts.error')

@section('code', '404')

@section('message', __('cms-ui::site.not found page'))
