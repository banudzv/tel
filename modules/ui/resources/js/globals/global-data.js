'use strict';

/**
 * @module
 */

import $ from 'jquery';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * Получение глобальных данных со страницы
 * @example
 * <script>
 *     (function () {
 *         window.__GLOBAL_DATA = {
 *             key: <?php echo $value ?>;
 *         }
 *     })()
 * </script>
 * @type {GlobalData}
 */
export const globalData = window.env || {};

globalData.CSRFToken = $('meta[name="csrf-token"]').attr('content');
