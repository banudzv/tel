// -----------------------------------------------------------------------------
// Deps
// -----------------------------------------------------------------------------

// global
import jQuery from 'js#/lib/jquery';
// scripts
import DMI from 'js#/dmi';
import cookieData from 'js#/modules/cookie-data';
import wsTabs from 'wezom-standard-tabs';
import initMmenu from 'js#/modules/mmenu';
import throttle from 'lodash.throttle';

// -----------------------------------------------------------------------------
// Initialize
// -----------------------------------------------------------------------------

jQuery(function ($) {
    const $root = $('html, body, .layout');
    const $window = $(window);

	initMmenu($('[data-mmenu-init]'));
	DMI.importAll();

    // -----------------------------------------------------------------------------
    // Button scroll top/scroll to
    // -----------------------------------------------------------------------------
	const checkClass = ($buttonUp) => {
		if ($window.scrollTop() > $window.height()) {
			$buttonUp.addClass('is-show');
		} else {
			$buttonUp.removeClass('is-show');
		}
	};

	$('[data-scroll-window]').each((i, button) => {
		const target = button.dataset.scrollWindow;
		const offset = button.dataset.offset ? button.dataset.offset : 0;

		switch (target) {
			case 'up':
				$(button).on('click', () => {
					$root.stop().animate({
						scrollTop: 0
					}, 500);
				});

				$window.scroll(throttle(function () {
					checkClass($(button));
				}, 100));

				checkClass($(button));
				break;
			default:
				$(button).on('click', () => {
					$root.stop().animate({
						scrollTop: $(target).length ? $(target)[0].offsetTop - offset : 0
					}, 500);
				});
		}
	});

    // -----------------------------------------------------------------------------

    cookieData.askUsage();
	wsTabs.init();
	wsTabs.setActive();
	wsTabs.hooks.on.push((ns, tabIndex, currentTrigger, currentPanel) => {
		window.setTimeout(() => {
			currentPanel.removeClass('is-hidden').siblings().addClass('is-hidden');
		}, 600);
	});
});
