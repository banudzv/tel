'use strict';

/**
 * @module initEach
 * @author Oleg Dutchenko <dutchenko.o.dev@gmail.com>
 * @version 1.0.0
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';
import 'custom-jquery-methods/fn/has-inited-key';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {jQuery} $elements
 * @param {string} key
 * @param {Function} fn
 */
export function initEach ($elements, key, fn) {
	$elements.each((i, element) => {
		const $element = $(element);
		if ($element.hasInitedKey(key)) {
			return true;
		} else {
			fn($element, element);
		}
	});
}
