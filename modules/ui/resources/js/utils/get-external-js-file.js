'use strict';

/**
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @type {Object}
 * @enum {Object}
 * @private
 */
const _cache = {};

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param src
 * @param queryData
 * @return {Promise}
 */
export function getExternalJsFile (src, queryData = {}) {
	return new Promise((resolve, reject) => {
		let source = _cache[src] || {};
		if (source.successData) {
			return resolve(...source.successData);
		}
		if (source.errorData) {
			return reject(...source.errorData); // eslint-disable-line prefer-promise-reject-errors
		}
		if (source.queue) {
			return source.queue.push({ resolve, reject });
		}

		source = _cache[src] = {
			queue: []
		};
		const query = [];
		for (let key in queryData) {
			if (queryData.hasOwnProperty(key)) {
				query.push(key + '=' + queryData[key]);
			}
		}

		const url = [src, query.join('&')].join('?');
		$.ajax({
			url,
			cache: true,
			dataType: 'script'
		}).then(
			(...args) => {
				source.successData = args;
				source.queue.unshift({ resolve });
				source.queue.forEach(({ resolve }) => resolve(...args));
			},
			(...args) => {
				source.errorData = args;
				source.queue.unshift({ reject });
				source.queue.forEach(({ reject }) => reject(...args));
			}
		);
	});
}

// ----------------------------------------
// Definitions
// ----------------------------------------

// описание кастомных типов jsdoc
