// -----------------------------------------------------------------------------
// Deps
// -----------------------------------------------------------------------------

// global
import { create } from '@wezom/dynamic-modules-import/dist/es5';

// -----------------------------------------------------------------------------
// Initialize
// -----------------------------------------------------------------------------

export default create({
	selector: '.js-import',
	debug: true,
	modules: {
		lozad: {
			filter: '[data-src]',
			importFn (stats) {
				return import('js#/modules/lozad');
			}
		},
		slickCarousel: {
			filter: '[data-slick-carousel]',
			importFn (stats) {
				return import('js#/modules/slick-carousel');
			}
		},
		inview: {
			filter: '[data-inview]',
			importFn (stats) {
				return import('js#/modules/inview');
			}
		},
		formValidation: {
			filter: 'form',
			importFn (stats) {
				return import('js#/modules/jquery-validation');
			}
		},
		header: {
			filter: '[data-header]',
			importFn (stats) {
				return import('js#/modules/header');
			}
		},
		magnificPopup: {
			filter: '[data-mfp]',
			importFn (stats) {
				return import('js#/modules/magnific-popup');
			}
		},
		stickyHeader: {
			filter: '[data-sticky-header]',
			importFn (stats) {
				return import('js#/modules/sticky-header');
			}
		},
		inputmask: {
			filter: '[data-mask]',
			importFn (stats) {
				return import('js#/modules/mask');
			}
		},
		select2: {
			filter: '[data-select2]',
			importFn (stats) {
				return import('js#/modules/select2');
			}
		},
		formItem: {
			filter: '[data-form-item]',
			importFn (stats) {
				return import('js#/modules/form-item');
			}
		},
		counter: {
			filter: '[data-counter]',
			importFn (stats) {
				return import('js#/modules/counter');
			}
		},
		toggle: {
			filter: '[data-toggle]',
			importFn (stats) {
				return import('js#/modules/toggle');
			}
		},
		tooltip: {
			filter: '[data-tooltip]',
			importFn (stats) {
				return import('js#/modules/tooltip');
			}
		},
		branches: {
			filter: '[data-branches]',
			importFn: async (stats) => {
				await import('js#/modules/tooltip');
				return import('js#/modules/branches');
			}
		},
		uploadFile: {
			filter: '[data-upload-file]',
			importFn (stats) {
				return import('js#/modules/upload-file');
			}
		},
		fixedButton: {
			filter: '[data-fixed-button]',
			importFn (stats) {
				return import('js#/modules/fixed-button');
			}
		},
		loadMore: {
			filter: '[data-load-more]',
			importFn (stats) {
				return import('js#/modules/load-more');
			}
		},
		truncateText: {
			filter: '[data-truncate]',
			importFn (stats) {
				return import('js#/modules/truncate-text');
			}
		},
		showMore: {
			filter: '[data-show-more]',
			importFn (stats) {
				return import('js#/modules/show-more');
			}
		},
		accordion: {
			filter: '[data-accordion]',
			importFn () {
				return import('js#/modules/accordion');
			}
		},
		googleMap: {
			filter: '[data-initialize-google-map]',
			importFn () {
				return import('js#/modules/google-map');
			}
		},
		navTabs: {
			filter: '[data-nav-tabs]',
			importFn () {
				return import('js#/modules/nav-tabs');
			}
		},
		search: {
			filter: '.js-search',
			importFn () {
				return import('js#/modules/search');
			}
		}
	}
})
