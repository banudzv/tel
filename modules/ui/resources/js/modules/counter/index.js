import '../../gsap/gsap';
import TweenMax from '../../gsap/TweenMax';
import '../../gsap/plugins/RoundPropsPlugin';
import IO from '../../classes/IO';

// ----------------------------------------
// Public
// ----------------------------------------

export default function ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('counterInitialized')) {
			return true;
		}

		_init($el);
	});
}

function _init ($element) {
	let counterIO = new IO();

	counterIO.setCallback(function (entry) {
		const $elementa = $(entry.target);

		if (entry.isIntersecting && !$elementa.data('counter')) {
			let duration = parseInt($elementa.data('duration')) || 1;
			let from = $elementa.data('from') || 0;
			let to = $elementa.data('to');
			let game = {
				score: from
			};

			TweenMax.to(game, duration, {
				score: to,
				roundProps: 'score',
				onUpdate: function () {
					$elementa.html(game.score);
				}
			});

			$elementa.data('counter', true);
		}
	});
	counterIO.observe($element);
}
