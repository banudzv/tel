'use strict';

/**
 * @module
 * @author Oleg Dutchenko <dutchenko.o.dev@gmail.com>
 * @version 1.0.0
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import 'custom-jquery-methods/fn/has-inited-key';
import { initEach } from '../../utils/init-each';
import createNewFactory from '../../utils/create-new-factory';
import { AbstractSelect2 } from './abstract';
import * as factory from './factory';

// ----------------------------------------
// Exports
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
	initEach($elements, 'select2IsInitialized', _init);
}

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {jQuery} $container
 * @private
 */
function _init ($container) {
	const data = $container.data('initialize-select2');
	/** @type {AbstractSelect2} */
	const instance = createNewFactory(AbstractSelect2, factory, $container, data);
	instance.initialize();
}
