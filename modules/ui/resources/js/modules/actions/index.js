'use strict';

/**
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import * as submitActions from './handler-actions';

// ----------------------------------------
// Private
// ----------------------------------------

export default async function actions ($container, response, settings = {}) {
	const { actions, status, description } = response;
	function executeAction ({ name, value, props }) {
		const actionFn = submitActions[name];
		if (typeof actionFn === 'function') {
			return actionFn($container, status, value, props, description, settings);
		}
		console.warn(`Action "${name}" - not found!`);
		return Promise.resolve();
	}

	for (const action of actions) {
		await executeAction(action);
	}

	if (IS_DEVELOPMENT) {
		console.log('All actions done!');
	}
}
