'use strict';

/**
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';
import 'custom-jquery-methods/fn/get-my-elements';
import { notifier } from 'js#/modules/notifier';
import { reInitScripts } from 'js#/utils/re-init-scripts';
import 'magnific-popup';

// -----------------------------------------
// Actions
// Каждый action - ДОЛЖЕН возвращать Promise !!!
// -----------------------------------------

/**
 * @param {jQuery} $container
 * @param {string} status
 * @param {string} value
 * @param {Object} [props={}]
 * @param {string} [props.layout]
 * @param {string|null} [description=null]
 * @return {Promise<any> | Promise}
 */
export function clear ($container, status, value, props = {}, description = null) {
	return new Promise((resolve) => {
		console.warn('clear - заглушка, переписать!');
		resolve();
	});
}

/**
 * @param {jQuery} $container
 * @param {string} status
 * @param {string} value
 * @param {Object} [props={}]
 * @param {string} [props.layout]
 * @param {string|null} [description=null]
 * @return {Promise<any> | Promise}
 */
export function notyMessage ($container, status, value, props = {}, description = null) {
	return new Promise((resolve) => {
		notifier({
			text: value,
			type: status,
			layout: props.layout || 'bottomRight'
		}).then((noty) => {
			noty.show();
			resolve();
		});
	});
}

/**
 * @param {jQuery} $container
 * @param {string} status
 * @param {string} value
 * @param {Object} [props={}]
 * @param {number} [props.timeout=0]
 * @param {string|null} [description=null]
 * @return {Promise<any> | Promise}
 */
export function overlappingMessage ($container, status, value, props = {}, description = null) {
	return new Promise((resolve) => {
		let $message = $container.getMyElements(
			'$myOverlappingMessage',
			props.block ? props.block : '.form__overlapping-message',
			'find'
		);

		if (!$message.length) {
			$message = $('<div class="form__overlapping-message"></div>');
			$container.append($message);
		}
		$message.html(`<span>${value}</span>`);

		window.setTimeout(() => {
			$container.addClass('is-overlapped');
		}, 50);

		window.setTimeout(() => {
			$container.removeClass('is-overlapped');
			resolve();
		}, props.timeout);
	});
}

export function popupShow ($container, status, value, props = {}, description = null) {
	return new Promise((resolve) => {
		if ($.magnificPopup) {
			if ($.magnificPopup.instance.isOpen) {
				$.magnificPopup.close();
			}

			const timeout = props.timeout * 1000 || 300;
			window.setTimeout(() => {
				$.magnificPopup.open({
					items: {
						src: value
					},
					closeMarkup: `
						<button title="%title%" type="button" class="mfp-close">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.97 17.88">
								<path d="M2.56.44,17.41,15.29a1.49,1.49,0,0,1,0,2.12h0a1.49,1.49,0,0,1-2.12,0L.44,2.56A1.49,1.49,0,0,1,.44.44h0A1.49,1.49,0,0,1,2.56.44Z"/>
								<path d="M.56,15.32,15.41.47a1.49,1.49,0,0,1,2.12,0h0a1.49,1.49,0,0,1,0,2.12L2.68,17.44a1.49,1.49,0,0,1-2.12,0h0A1.49,1.49,0,0,1,.56,15.32Z"/>
							</svg>
						</button>
					`,
					type: 'inline'
				});
			}, timeout);
		}

		resolve();
	});
}

/**
 * @param {jQuery} $container
 * @param {string} status
 * @param {string} value
 * @param {Object} [props={}]
 * @param {number} [props.timeout=0]
 * @param {string|null} [description=null]
 * @return {Promise<any> | Promise}
 */
export function popupClose ($container, status, value, props = {}, description = null) {
	return new Promise((resolve) => {
		const close = () => {
			if ($.magnificPopup && $.magnificPopup.instance.isOpen) {
				$.magnificPopup.close();
			}
			resolve();
		};

		if (props.timeout) {
			window.setTimeout(() => {
				close();
			}, props.timeout);
		} else {
			close();
		}
	});
}

/**
 * @param {jQuery} $container
 * @param {string} status
 * @param {string} value
 * @param {Object} [props={}]
 * @param {number} [props.timeout=0]
 * @param {string|null} [description=null]
 * @return {Promise<any> | Promise}
 */
export function redirect ($container, status, value, props = {}, description = null) {
	return new Promise((resolve) => {
		const timeout = props.timeout || 300;
		window.setTimeout(() => {
			window.location = value;
			resolve();
		}, timeout);
	});
}

export function reload ($container, status, value = '', props = {}, description = null) {
	return new Promise((resolve) => {
		document.location.reload();
		resolve();
	});
}

/**
 * @param {jQuery} $container
 * @param {string} status
 * @param {string} value
 * @param {Object} [props={}]
 * @param {string|null} [description=null]
 * @return {Promise<any> | Promise}
 */
export function reset ($container, status, value, props = {}, description = null) {
	return new Promise((resolve) => {
		$container.trigger('reset');
		resolve();
	});
}

/**
 * @param {jQuery} $container
 * @param {string} status
 * @param {string} value
 * @param {Object} [props={}]
 * @param {string|null} [description=null]
 * @return {Promise<any> | Promise}
 */
export function replaceForm ($container, status, value, props = {}, description = null) {
	return new Promise((resolve) => {
		$container.html(value);
		resolve();
	});
}

/**
 * @param {jQuery} $container
 * @param {string} status
 * @param {string} value
 * @param {Object} [props={}]
 * @param {string|null} [description=null]
 * @return {Promise<any> | Promise}
 */
export function items ($container, status, value = [], props = {}, description = null) {
	return new Promise((resolve) => {
		value.forEach(function (item) {
			if (item.selector !== undefined) {
				const $element = $container.find(item.selector);

				if ($element.length) {
					if (item.html !== undefined) {
						if (item.method && item.method === 'append') {
							$element.append(item.html);
						} else {
							$element.html(item.html);
						}
						reInitScripts($container);
					}

					if (item.addClass !== undefined) {
						$element.addClass(item.addClass);
					}

					if (item.removeClass !== undefined) {
						$element.removeClass(item.removeClass);
					}
				}
			}

			if (item.element !== undefined) {
				const $element = $(item.element);

				if ($element.length) {
					if (item.html !== undefined) {
						if (item.method && item.method === 'append') {
							$element.append(item.html);
						} else {
							$element.html(item.html);
						}
						reInitScripts();
					}

					if (item.addClass !== undefined) {
						$element.addClass(item.addClass);
					}

					if (item.removeClass !== undefined) {
						$element.removeClass(item.removeClass);
					}
				}
			}
		});
		resolve();
	});
}

export function replaceHtml ($container, status, value = '', props = {}, description = null) {
	return new Promise((resolve) => {
		const $element = $container.find('[data-send-ajax-replace]');
		if ($element.length) {
			$element.html(value);
			reInitScripts($container);
		}
		resolve();
	});
}

export function appendHtml ($container, status, value = '', props = {}, description = null) {
	return new Promise((resolve) => {
		const $element = $container.find('[data-send-ajax-append]');
		if ($element.length) {
			$element.append(value);
			reInitScripts($container);
		}
		resolve();
	});
}

export function addClass ($container, status, value = '', props = {}, description = null) {
	return new Promise((resolve) => {
		let $element = $container.find('[data-send-ajax-addclass]');
		if ($element.length) {
			$element.addClass(value);
		}
		resolve();
	});
}

export function removeClass ($container, status, value = '', props = {}, description = null) {
	return new Promise((resolve) => {
		let $element = $container.find('[data-send-ajax-removeclass]');
		if ($element.length) {
			$element.removeClass(value);
		}
		resolve();
	});
}

export function options ($container, status, value = {}, props = {}, description = null, settings = {}) {
	return new Promise((resolve) => {
		const $element = settings.element || null;
		if ($element.length) {
			const param = $element.data('send-ajax') || {};
			const { options = {} } = param;
			$element.data(
				'send-ajax',
				{
					...param,
					options: {
						...options,
						...value
					}
				}
			);
		}
		resolve();
	});
}

/**
 * @param {jQuery} $container
 * @param {string} status
 * @param {string} value
 * @param {Object} [props={}]
 * @param {string|null} [description=null]
 * @return {Promise<any> | Promise}
 */
export function VoteCount ($container, status, value, props = {}, description = null) {
	return new Promise((resolve) => {
		$container
			.closest('.js-parent-vote-count')
			.find('.js-vote-count')
			.html(value);
		resolve();
	});
}
