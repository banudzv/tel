/**
 * @module toggleClass
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';
import Toggle from './Toggle';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {jQuery} $containers
 */
export default function ($containers) {
	$.each($containers, (i, root) => {
		let $root = $(root);
		if ($root.data('Toggle') instanceof Toggle === false) {
			const instance = new Toggle($root);
			$root.data('Toggle', instance);
			$root.addClass('is-initialized');
		}
	});
}
