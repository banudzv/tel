import $ from 'jquery';

export default function ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('showMoreInitialized')) {
			return true;
		}

		init($el);
	});
}

function init ($container) {
	const activeClass = 'is-active';
	const $content = $container.find('[data-show-more-content]');
	const $block = $container.find('[data-show-more-block]');
	const $trigger = $container.find('[data-show-more-trigger]');
	const maxHeight = $block.data('show-more-block');

	const calculateMaxHeight = () => {
		if (window.outerWidth < 1280) {
			return maxHeight;
		} else {
			return maxHeight / 19.2;
		}
	};

	const getContentHeight = () => $content.outerHeight();
	const isActive = () => $block.hasClass(activeClass) && $trigger.hasClass(activeClass);

	const setContentHeight = () => {
		if (isActive()) {
			console.log(1);
			$block.css('max-height', `${getContentHeight() + 20}px`);
		} else if (getContentHeight() >= $block.outerHeight()) {
			console.log(2);

			const $child = $content.children().eq(0);
			const childHeight = $child.outerHeight();
			if (window.outerWidth < 1280) {
				$block.css('max-height', `${childHeight > maxHeight || !$child.length ? '100px' : '100px'}`);
			} else {
				$block.css('max-height', `${childHeight > maxHeight || !$child.length ? '100px' : '100px'}`);
			}
		}

		if (getContentHeight() < $block.outerHeight() && !isActive()) {
			$trigger.addClass('_hide');
		} else {
			$trigger.removeClass('_hide');
		}
	};

	$trigger.on('click', () => {
		if (!isActive()) {
			$trigger.addClass(activeClass);
			$container.addClass(activeClass);
			$block.addClass(activeClass);
		} else {
			$trigger.removeClass(activeClass);
			$container.removeClass(activeClass);
			$block.removeClass(activeClass);
		}

		setContentHeight();
	});
	$(window).on('resize orientationchange', setContentHeight);

	setContentHeight();

	if (getContentHeight() <= $block.outerHeight() && !isActive()) {
		$trigger.addClass('_hide');
	}
}
