'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import Noty from 'noty';

// ----------------------------------------
// Private
// ----------------------------------------

Noty.overrideDefaults({
	theme: 'metroui',
	layout: 'bottomRight',
	timeout: 5000,
	progressBar: true,
	closeWith: ['click']
});

// ----------------------------------------
// Exports
// ----------------------------------------

export default Noty;
