// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';
import { GoogleMapPluginAbstract } from './abstract';

// ----------------------------------------
// Exports
// ----------------------------------------

export class DefaultGoogleMap extends GoogleMapPluginAbstract {
	get defaultSettings() {
		return Object.assign(super.defaultSettings, {});
	}

	get defaultProps() {
		return Object.assign(super.defaultProps, {});
	}

	_afterInitialize() {
		super._afterInitialize();
	}
}
