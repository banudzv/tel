'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';
import 'jquery-inview';
import 'custom-jquery-methods/fn/get-my-elements';
import createTimer from 'create-timer';
import { AbstractSlickCarousel } from 'web-plugin-abstract-slick-carousel';
import wsTabs from 'wezom-standard-tabs';
import {reInitScripts} from "js#/utils/re-init-scripts";

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @inheritDoc
 */

export class HeroSlider extends AbstractSlickCarousel {
	_afterInitialize () {
		super._afterInitialize();
	}
}

export class ServicesSlider extends AbstractSlickCarousel {
	_afterInitialize() {
		super._afterInitialize();
		import('./helpers/services-slider').then(({ GSAP }) => {
			this.dotsAnimate(GSAP);
		});
	}

	dotsAnimate (GSAP) {
		const { TimelineMax, TweenMax } = GSAP;
		const $list = this.$list;
		const slick = $list.slick('getSlick');
		const duration = slick.options.autoplaySpeed / 1000;
		const $services = $('[data-service]');

		const animate = index => {
			const $service = $services.eq(index);
			const path = $service.getMyElements('$myPath', '.js-progress', 'find')[0];
			const timeline = new TimelineMax({
				onComplete () {
					$list.slick('slickNext');
					this.kill();
					$service.data('timeline', null);
				}
			});

			$service.addClass('is-active');

			timeline.set(path, {
				drawSVG: '0',
				opacity: 1,
			}).to(path, duration, {
				drawSVG: '100%',
			}).play();
			$service.data('timeline', timeline);
		};

		const hide = index => {
			const $service = $services.eq(index);
			$service.removeClass('is-active');
			const path = $service.getMyElements('$myPath', '.js-progress', 'find')[0];
			const timeline = $service.data('timeline');

			if (timeline instanceof TimelineMax) {
				timeline.kill();
			}

			TweenMax.to(path, 100 / 1000, {
				opacity: 0
			});
		};

		$list.on('beforeChange', (event, slick, currentSlide) => hide(currentSlide));
		$list.on('afterChange', (event, slick, currentSlide) => animate(currentSlide));

		// click nav
		$services.on('click', function () {
			const nextSlide = $(this).data('service');
			$list.slick('slickGoTo', nextSlide);
		});

		let animated = false;
		$list.on('inview', (event, isInview) => {
			if (this.isInitialized) {
				if (isInview) {
					if (!animated) {
						animate(slick.currentSlide);
						animated = true;
					}
				}
			}
		});
		$list.trigger('inview');
	}
}

function counter (th) {
	const initialSlick = th.$list.slick('getSlick');

	th.$list.closest(`[data-${wsTabs.keys.block}]`).on(wsTabs.events.on, () => {
		th.update();
	});
}

export class InfoSlickCarousel extends AbstractSlickCarousel {
	/**
	 * @type {SlickCarouselSettings}
	 */
	get defaultSettings () {
		/** @type {SlickCarouselSettings} */
		const settings = {
			accessibility: false,
			mobileFirst: true,
			slidesToShow: 2,
			infinite: false,
			swipeToSlide: true,
			adaptiveHeight: true,
			responsive: [
				{
					breakpoint: 1366,
					settings: {
						slidesToShow: 4
					}
				},
				{
					breakpoint: 640,
					settings: {
						slidesToShow: 3
					}
				},
			]
		};
		return Object.assign(super.defaultSettings, settings);
	}


	_afterInitialize () {
		super._afterInitialize();
		counter(this);
		$(this.$container).closest('.tabs--slider').css('height', 'auto');
	}
}

export class BlogCardCarousel extends AbstractSlickCarousel {
	/**
	 * @type {SlickCarouselSettings}
	 */
	get defaultSettings () {
		/** @type {SlickCarouselSettings} */
		const settings = {
			accessibility: false,
			mobileFirst: true,
			slidesToShow: 1,
			infinite: false,
			swipeToSlide: true,
			adaptiveHeight: true,
			responsive: [
				{
					breakpoint: 1366,
					settings: {
						slidesToShow: 4
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 640,
					settings: {
						slidesToShow: 2
					}
				},
			]
		};
		return Object.assign(super.defaultSettings, settings);
	}


	_afterInitialize () {
		super._afterInitialize();
		counter(this);
		$(this.$container).closest('.tabs--slider').css('height', 'auto');
	}
}

export class PartnerSlickCarousel extends AbstractSlickCarousel {
	/**
	 * @type {SlickCarouselSettings}
	 */
	get defaultSettings () {
		/** @type {SlickCarouselSettings} */
		const settings = {
			accessibility: false,
			mobileFirst: true,
			slidesToShow: 2,
			slidesToScroll: 2,
			infinite: false,
			adaptiveHeight: true,
			responsive: [
				{
					breakpoint: 1366,
					settings: {
						slidesToShow: 6,
						slidesToScroll: 6
					}
				},
				{
					breakpoint: 640,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 4
					}
				},
			]
		};
		return Object.assign(super.defaultSettings, settings);
	}


	_afterInitialize () {
		super._afterInitialize();
		counter(this);
		$(this.$container).closest('.tabs--slider').css('height', 'auto');
		// const $countOf = $('#' + this.props.countOfId);
		// const countOf = new CountOf($countOf);
		// const initialSlick = this.$list.slick('getSlick');

		// this.$list.on('afterChange', (event, slick, currentSlide) => {
		// 	countOf.updateCurrent(currentSlide + 1);
		// });
		// countOf.updateCurrent(initialSlick.currentSlide + 1);
		// countOf.updateAmount(initialSlick.slideCount);
	}
}

export class ChronologySliderYears extends AbstractSlickCarousel {
	/**
	 * @type {SlickCarouselSettings}
	 */

	get defaultSettings () {
		/** @type {SlickCarouselSettings} */

		const settings = {
			accessibility: false,
			mobileFirst: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			infinite: false,
			adaptiveHeight: true,
			draggable: false,
			centerMode: true,
			centerPadding: 0,
			speed: 500,
			responsive: [
				{
					breakpoint: 568,
					settings: {
						slidesToShow: 5,
					}
				},
				{
					breakpoint: 1280,
					settings: {
						slidesToShow: 9,
						slidesToScroll: 1,
						variableWidth: true,
						centerMode: false,
						speed: 100
					}
				}
			]
		};
		return Object.assign(super.defaultSettings, settings);
	}


	_afterInitialize () {
		super._afterInitialize();
		this.$container.data('slick', this);

		const prevArrow = this.$container.parents('[data-slider-root]').find('[data-slick-carousel-prev-arrow]');
		const nextArrow = this.$container.parents('[data-slider-root]').find('[data-slick-carousel-next-arrow]');
		prevArrow.on('click', () => this.$list.slick('slickPrev'));
		nextArrow.on('click', () => this.$list.slick('slickNext'));

		this.$list.on('afterChange', (event, slick, currentSlideIdx, nextSlideIdx) => {
			this.update();
		});
	}
}
export class ChronologySliderInfo extends AbstractSlickCarousel {
	/**
	 * @type {SlickCarouselSettings}
	 */

	get defaultSettings () {
		/** @type {SlickCarouselSettings} */

		const settings = {
			accessibility: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: false,
			adaptiveHeight: true,
			draggable: false
		};
		return Object.assign(super.defaultSettings, settings);
	}

	_afterInitialize () {
		super._afterInitialize();

		const additionalSlider = this.$container.siblings('[data-slick-carousel]');
		const prevArrow = this.$container.parents('[data-slider-root]').find('[data-slick-carousel-prev-arrow]');
		const nextArrow = this.$container.parents('[data-slider-root]').find('[data-slick-carousel-next-arrow]');
		prevArrow.on('click', () => this.$list.slick('slickPrev'));
		nextArrow.on('click', () => this.$list.slick('slickNext'));
		this.$list.on('beforeChange', (event, slick, currentSlideIdx, nextSlideIdx) => {
			const slidesAmount = this.$list.find('[data-slick-index]').length;
			const penultimateSlide = slidesAmount - 1;

			if (nextSlideIdx === penultimateSlide) {
				nextArrow.hide();
			} else {
				nextArrow.show();
			}

			if (nextSlideIdx === 0) {
				prevArrow.hide();
			} else {
				prevArrow.show();
			}

			if ($(window).innerWidth() > 1280) {
				additionalSlider.find('[data-slick-index]').find('[data-chronology-slider-year]').removeClass('is-active');
				additionalSlider.find(`[data-slick-index=${nextSlideIdx}]`).find('[data-chronology-slider-year]').addClass('is-active');
				this.update();
			} else {
				this.$list.on('swipe', (event, slick, direction) => {
					if (direction === 'left') {
						additionalSlider.data().slick.$list.slick('slickNext');
					} else {
						additionalSlider.data().slick.$list.slick('slickPrev');
					}
				})
			}
		});
	}
}

export class AwardsSlider extends AbstractSlickCarousel {
	/**
	 * @type {SlickCarouselSettings}
	 */

	get defaultSettings () {
		/** @type {SlickCarouselSettings} */

		const settings = {
			accessibility: false,
			mobileFirst: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: false,
			adaptiveHeight: true,
			arrows: true,
			speed: 500,
			responsive: [
				{
					breakpoint: 568,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 1280,
					settings: {
						slidesToShow: 5
					}
				}
			]
		};
		return Object.assign(super.defaultSettings, settings);
	}
}

export class PersonSlider extends AbstractSlickCarousel {
	/**
	 * @type {SlickCarouselSettings}
	 */

	get defaultSettings () {
		/** @type {SlickCarouselSettings} */

		const settings = {
			accessibility: false,
			mobileFirst: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: false,
			adaptiveHeight: true,
			arrows: true,
			speed: 500,
			responsive: [
				{
					breakpoint: 568,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 1280,
					settings: {
						slidesToShow: 5
					}
				}
			]
		};
		return Object.assign(super.defaultSettings, settings);
	}

	_afterInitialize() {

		super._afterInitialize();
		reInitScripts(this.$container);
	}
}

