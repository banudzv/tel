'use strict';

/**
 * @module heroDotSlider
 * @author OlegDutchenko <dutchenko.o.dev@gmail.com>
 * @version 0.0.1
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';
import '../../../gsap/TweenMax';
import GSAP from 'js#/gsap/gsap';
import '../../../gsap/plugins/DrawSVGPlugin';

// ----------------------------------------
// Public
// ----------------------------------------

export { GSAP };
