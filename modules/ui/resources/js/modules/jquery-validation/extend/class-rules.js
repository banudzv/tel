/**
 * List rules validation by one class
 * Every property - html class
 * @type {Object}
 * @example
 *	'validate-file': {
 *		filetype: 'doc|pdf|zip|rar|txt|docx|odt',
 *		filesize: 3000,
 *		required: true
 *	}
 */
const classRules = {};

export default classRules;
