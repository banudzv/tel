import 'custom-jquery-methods/fn/has-inited-key';

export default function init ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('formItemInitialized')) {
			return true;
		}
		_init($el);
	});
}

function _init ($element) {
	const $control = $element.find('input, textarea, select');

	const {
		blurTimeout = 0,
		triggerFocus = false
	} = $element.data('form-item');

	if (triggerFocus) {
		$element.on('click', () => {
			$control[0].focus();
		});
	}

	$control.on('focus', () => {
		$element.removeClass('not-in-focus').addClass('is-in-focus');
	});

	$control.on('blur', () => {
		setTimeout(() => {
			$element.removeClass('is-in-focus').addClass('not-in-focus');
		}, blurTimeout);
	});
}
