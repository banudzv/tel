import $ from 'jquery';

export default function ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('searchInitialized')) {
			return true;
		}

		init($el);
	});
}

function init ($button) {
	const $html = $('html');
	const $block = $('[data-search-block]');
	const $dim = $('[data-dim]');

	$button.on('click', () => {
		$html.addClass('dimmed');
		$block.addClass('is-active');

		$dim.on('click', () => {
			$html.removeClass('dimmed');
			$block.removeClass('is-active');
			$dim.off('click');
		});
	});

	$(window).on('resize orientationchange', () => {
		if (window.innerWidth < 1280 && $html.hasClass('dimmed')) {
			$html.removeClass('dimmed');
			$block.removeClass('is-active');
			$dim.off('click');
		}
	});
}
