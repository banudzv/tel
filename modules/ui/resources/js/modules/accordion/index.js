'use strict';

/**
 * @module accordion
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Public
// ----------------------------------------
export default function ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('accordion-initialized')) {
			return true;
		}

		_init($el);
	});
}

function _init ($containers) {
	$containers.each(function () {
		const $container = $(this);
		const customOptions = $container.data('accordion');
		const options = Object.assign(
			{
				ns: '',
				type: 'multiple',
				slideTime: 300,
				openClass: 'is-active',
				eventName: 'click'
			},
			customOptions
		);

		const $buttons = $container.find('[data-accordion-ns="' + options.ns + '"][data-accordion-button]');
		const $blocks = $container.find('[data-accordion-ns="' + options.ns + '"][data-accordion-block]');
		const $items = $container.find('[data-accordion-ns="' + options.ns + '"][data-accordion-item]');

		$buttons.on('click', function () {
			const $button = $(this);
			const index = $button.data('accordion-button');
			const $block = $blocks.filter('[data-accordion-block="' + index + '"]');
			const $item = $items.filter('[data-accordion-item="' + index + '"]');
			const $otherBlocks = $blocks.filter(':not([data-accordion-block="' + index + '"])');

			if (options.type === 'single') {
				$otherBlocks.each(function () {
					const $otherBlock = $(this);
					const index = $otherBlock.data('accordion-block');
					const $otherButton = $buttons.filter('[data-accordion-button="' + index + '"]');
					const $otherItem = $items.filter('[data-accordion-item="' + index + '"]');

					$otherBlock.stop(false, true).slideUp(options.slideTime, function () {
						$otherBlock.removeClass(options.openClass);
						$otherButton.removeClass(options.openClass);
						$otherItem.removeClass(options.openClass);
					});
				});
			}

			if ($block.hasClass(options.openClass)) {
				$block.stop(false, true).slideUp(options.slideTime, function () {
					$container.removeClass(options.openClass);
					$block.removeClass(options.openClass);
					$button.removeClass(options.openClass);
					$item.removeClass(options.openClass);
				});
			} else {
				$block.stop(false, true).slideDown(options.slideTime, function () {
					$container.addClass(options.openClass);
					$button.addClass(options.openClass);
					$block.addClass(options.openClass);
					$item.addClass(options.openClass);

					$button.trigger('accordion-on');
					$block.trigger('accordion-on');
				});
			}
		});
	});
}
