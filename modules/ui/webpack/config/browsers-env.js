/**
 * @fileOverview Описываем справочник доступных путей
 * который используеться при сборке файлов
 */

module.exports = {
	development: 'development',
	production: 'production',
	legacy: 'legacy'
};
