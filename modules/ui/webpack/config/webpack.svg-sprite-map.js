/**
 * @fileOverview Конфигурация webpack под dev режим разработки
 */

const fs = require('fs');
const { join, basename } = require('path');
const glob = require('glob');
const SVGSpriteMapPlugin = require('svg-spritemap-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const paths = require('./paths');
const cleanPath = require('../utils/clean-path');

const sprites = glob
	.sync(paths.svgSpriteMap.folders, {
		ignore: paths.svgSpriteMap.entry
	})
	.filter((path) => fs.lstatSync(path).isDirectory())
	.map((src) => ({
		src: cleanPath(join(src, '/*')),
		filename: basename(src)
	}));

/**
 * Составляем webpack конфиг заточенный под инкрементальную сборку в dev режиме
 * @type {Configuration}
 */
module.exports = {
	mode: 'production',
	devtool: false,
	watch: false,
	entry: paths.svgSpriteMap.entry,
	output: {
		path: paths.dist.svg,
		publicPath: paths.public.svg,
		filename: 'blank.js'
	},
	plugins: [
		new CleanWebpackPlugin(),
		...sprites.map(
			({ src, filename }) =>
				new SVGSpriteMapPlugin(src, {
					output: {
						filename: `./${filename}.svg`
					},
					sprite: {
						prefix: false,
						gutter: 5,
						generate: {
							title: false
						}
					}
				})
		)
	]
};
