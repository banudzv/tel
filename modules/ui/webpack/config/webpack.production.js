/**
 * @fileOverview Конфигурация webpack под продакшин режим финальной сборки
 */

const { join } = require('path');
const { merge } = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const webpackConfigCommon = require('../../../../webpack.config');
const paths = require('./paths');
const { getPostcssPlugins } = require('../utils/get-postcss-options');
const {
	getBrowserslistEnvList,
	getBrowserslistQueries
} = require('../utils/get-browserslist');

const createConfig = (env) =>
	merge(webpackConfigCommon, {
		mode: 'production',
		devtool: false,
		name: env,
		entry: {
			app: join(paths.src.js, 'app.js'),
			styles: join(paths.src.sass, 'app.scss')
		},
		output: {
			path: paths.dist.buildTemp,
			publicPath: paths.public.build,
			filename: `[name].${env}.[contenthash].js`
		},
		module: {
			rules: [
				{
					test: /\.js$/,
					use: [
						{
							loader: 'babel-loader',
							options: {
								cacheDirectory: true,
								presets: [
									[
										'@babel/preset-env',
										{
											targets: getBrowserslistQueries({ env })
										}
									]
								]
							}
						}
					]
				},
				{
					test: /\.scss$/,
					use: [
						MiniCssExtractPlugin.loader,
						{
							loader: 'css-loader',
							options: {
								sourceMap: false,
								importLoaders: 1,
								url: false
							}
						},
						{
							loader: 'postcss-loader',
							options: {
								sourceMap: false,
								postcssOptions: {
									plugins: getPostcssPlugins(env)
								}
							}
						},
						{
							loader: 'sass-loader',
							options: {
								sourceMap: false,
								implementation: require('sass'),
								additionalData: [
									`$ENV: '${env}';`,
									`$STATIC_PATH: '/';`,
									`$PUBLIC_PATH: '${paths.public.base}';`
								].join('\n')
							}
						}
					]
				}
			]
		},
		optimization: {
			minimize: true,
			minimizer: [
				new CssMinimizerPlugin({
					cache: true,
					minimizerOptions: {
						preset: [
							'default',
							{
								zindex: false,
								autoprefixer: false,
								reduceIdents: false,
								discardUnused: false,
								cssDeclarationSorter: false,
								postcssCalc: false,
								discardComments: {
									removeAll: true
								}
							}
						]
					}
				}),
				new TerserPlugin({
					terserOptions: {
						mangle: true
					}
				})
			]
		},
		plugins: [
			new MiniCssExtractPlugin({
				filename: `[name].${env}.[contenthash].css`
			}),
			new ManifestPlugin({
				basePath: paths.public.base,
				fileName: `manifest.${env}.json`
			})
		]
	});

/** Составляем список webpack конфигов заточенный под финальный билд */
module.exports = getBrowserslistEnvList()
	.filter((env) => env !== 'development')
	.map(createConfig);
