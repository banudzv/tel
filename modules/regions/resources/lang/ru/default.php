<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
		'Region' => 'Область',
		'Regions' => 'Области',
		'City' => 'Город',
		'Cities' => 'Города',
		'Regions and cities' => 'Области и города',
		'latitude' => 'Широта',
		'longitude' => 'Долгота',
    ],
    TranslationSide::SITE => [
    ],
];
