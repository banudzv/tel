<?php

use Faker\Generator as Faker;
use WezomCms\Slider\Models\Slide;

$factory->define(Slide::class, function (Faker $faker) {
    $sliders = config('cms.slider.slider.sliders');

    return [
        'published' => $faker->boolean(80),
        'open_in_new_tab' => $faker->boolean,
        'name' => $faker->realText(50),
        'url' => $faker->url,
        'slider' => $faker->randomKey($sliders),
    ];
});
