<?php

namespace WezomCms\Slider\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Slider\Models\SlideLink;

class SlideLinkRepository extends AbstractRepository
{
    protected function model()
    {
        return SlideLink::class;
    }
}
