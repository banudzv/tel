<?php

namespace WezomCms\Slider\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Slider\Models\Slide;

class SlideRepository extends AbstractRepository
{
    protected function model()
    {
        return Slide::class;
    }

    public function getByFront($sliderKey = 'main')
    {
        return $this->getModelQuery()
            ->published()
            ->where('slider', $sliderKey)
            ->with([
                'service', 'services', 'links'
            ])
            ->orderBy('sort')
            ->latest('id')
            ->get();
    }
}
