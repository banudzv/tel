<?php

namespace WezomCms\Slider\Widgets;

use Illuminate\Database\Eloquent\Collection;
use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\Slider\Models\Slide;
use WezomCms\Slider\Repositories\SlideRepository;

class Slider extends AbstractWidget
{
    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        $sliderKey = array_get($this->data, 'key', 'main');

        $repository = resolve(SlideRepository::class);

        /** @var Collection $result */
        $result = $repository->getByFront();


        if ($result->isEmpty()) {
            return null;
        }

        $result = $this->prettyData($result);

        $size = config("cms.slider.slider.sliders.{$sliderKey}.size");

        return compact('result', 'size');
    }

    public function prettyData($data)
    {
//        dd($data);
        $arr = [];
        foreach ($data ?? [] as $key => $item){
            /** @var $item Slide */
            $arr[$key]['name'] = $item->name;
            $arr[$key]['text'] = $item->text;
            $arr[$key]['image'] = $item->getImage();

            $arr[$key]['service']['name'] = $item->service->name ?? null;
            $arr[$key]['service']['slug'] = $item->service ? $item->service->getFrontUrl() : null;

            $countLink = 0;
            foreach($item->services ?? [] as $ser){
//                dd($ser);
                $arr[$key]['links'][$countLink]['name'] = $ser->name;
                $arr[$key]['links'][$countLink]['link'] = $ser ? $ser->getFrontUrl() : null;
                $arr[$key]['links'][$countLink]['icon'] = $ser->icon;
                $countLink++;
            }
            foreach($item->links ?? [] as $lin){
                $arr[$key]['links'][$countLink]['name'] = $lin->name;
                $arr[$key]['links'][$countLink]['link'] = $lin->link;
                $arr[$key]['links'][$countLink]['icon'] = $lin->icon;
                $countLink++;
            }
        }

        return $arr;
    }
}
