<?php

namespace WezomCms\Slider\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Http\Requests\ChangeStatus\RequiredIfMessageTrait;
use WezomCms\Core\Traits\LocalizedRequestTrait;

class SliderRequest extends FormRequest
{
    use LocalizedRequestTrait;
    use RequiredIfMessageTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'slider' => 'nullable',
            'open_in_new_tab' => 'bool',
            'published' => 'bool',
            'service_id' => 'nullable|integer',
//            'image' => 'nullable|image',
        ];

        $sliders = collect(config('cms.slider.slider.sliders'))->keys();
        if ($sliders->count() > 1) {
            $rules['slider'] .= '|required|in:' . $sliders->implode(',');
        }

        return $this->localizeRules(
            [
                'url' => 'nullable|url|string|max:255',
                'name' => 'nullable|string|max:255|required_if:{locale}.published,1',
                'text' => 'nullable|string',
            ],
            $rules
        );
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->localizeAttributes(
            [
                'published' => __('cms-core::admin.layout.Published'),
                'url' => __('cms-slider::admin.Link'),
                'image' => __('cms-slider::admin.Image'),
                'name' => __('cms-slider::admin.Name'),
            ],
            ['slider' => __('cms-slider::admin.Slider')]
        );
    }
}
