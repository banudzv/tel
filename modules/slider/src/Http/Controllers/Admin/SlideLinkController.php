<?php

namespace WezomCms\Slider\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Slider\Http\Requests\Admin\SlideLinkRequest;
use WezomCms\Slider\Http\Requests\Admin\SliderRequest;
use WezomCms\Slider\Models\SlideLink;

class SlideLinkController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = SlideLink::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-slider::admin.link';

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = true;

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.slide-links';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = SlideLinkRequest::class;

    public function __construct()
    {
        parent::__construct();
    }

//    protected function fill($obj, $request): array
//    {
//        $data = parent::fill($obj, $request);
//
//        dd($request->all());
//
//        return $data;
//    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-slider::admin.Slide Links');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }
}
