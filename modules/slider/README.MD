## Installation

#### Use console
```
composer require wezom-cms/slider
```
#### Or edit composer.json
```
"require": {
    ...
    "wezom-cms/slider": "^7.0"
}
```
#### Install dependencies:
```
composer update
```
#### Package discover
```
php artisan package:discover
```
#### Run migrations
```
php artisan migrate
```

## Publish
#### Views
```
php artisan vendor:publish --provider="WezomCms\Slider\SliderServiceProvider" --tag="views"
```
#### Config
```
php artisan vendor:publish --provider="WezomCms\Slider\SliderServiceProvider" --tag="config"
```
#### Lang
```
php artisan vendor:publish --provider="WezomCms\Slider\SliderServiceProvider" --tag="lang"
```
#### Migrations
```
php artisan vendor:publish --provider="WezomCms\Slider\SliderServiceProvider" --tag="migrations"
```
