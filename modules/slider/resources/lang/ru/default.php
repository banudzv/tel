<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
        'Slider' => 'Слайдер',
        'Sliders' => 'Слайдеры',
        'Slides list' => 'Список слайдов',
        'Name' => 'Название',
        'Text' => 'Описание',
        'Image' => 'Изображение',
        'Link' => 'Cсылка',
        'No image' => 'Нет изображения',
        'Open link in new tab' => 'Открывать ссылку в новой вкладке',
        'Main slider' => 'Слайдер на главной',
        'Main service' => 'Основной сервис',
        'Services additional' => 'Доп. сервисы (в виде ссылок с иконкой)',
        'Slide Links' => 'Ссылки для слайдера',
    ],
];
