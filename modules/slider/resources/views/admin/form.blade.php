@php
    /**
     * @var $viewPath string
     */
    $tabs = [
        __('cms-core::admin.layout.Main data') => $viewPath . '.tabs.main',
        __('cms-core::admin.tabs.relation element') => $viewPath . '.tabs.relations',
    ];
@endphp

<div class="row">
    <div class="col-lg-7">
        <div class="card mb-3">
            <div class="card-body">
                @tabs($tabs)
            </div>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="card mb-3">
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label('published', __('cms-core::admin.layout.Published')) !!}
                    {!! Form::status('published', null, true)  !!}
                </div>
                <div class="form-group">
                    {!! Form::label('open_in_new_tab', __('cms-slider::admin.Open link in new tab')) !!}
                    {!! Form::status('open_in_new_tab', null, false) !!}
                </div>
{{--                @if($sliders->count() > 2)--}}
{{--                    <div class="form-group">--}}
{{--                        {!! Form::label('slider', __('cms-slider::admin.Slider')) !!}--}}
{{--                        {!! Form::select('slider', $sliders, null, ['class' => 'js-menu-group-selector']) !!}--}}
{{--                    </div>--}}
{{--                @endif--}}
                <div class="form-group">
                    {!! Form::label('image', __('cms-slider::admin.Image')) !!}
                    {!! Form::imageUploader('image', $obj, route($routeName . '.delete-image', [$obj->id])) !!}
                </div>
            </div>
        </div>
    </div>
</div>
