@langTabs
<div class="form-group">
    {!! Form::label($locale . '[name]', __('cms-slider::admin.Name')) !!}
    {!! Form::text($locale . '[name]', old($locale . '.name', $obj->translateOrNew($locale)->name)) !!}
</div>
<div class="form-group">
    {!! Form::label($locale . '[text]', __('cms-slider::admin.Text')) !!}
    {!! Form::textarea($locale . '[text]', old($locale . '.text', $obj->translateOrNew($locale)->text), ['class' => 'js-wysiwyg', 'data-lang' => $locale]) !!}
</div>
<div class="form-group">
    {!! Form::label($locale . '[url]', __('cms-slider::admin.Link')) !!}
    {!! Form::text($locale . '[url]', old($locale . '.url', $obj->translateOrNew($locale)->url), ['placeholder' => 'http://example.com']) !!}
</div>
@endLangTabs
