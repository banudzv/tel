@extends('cms-core::admin.crud.index')

@section('content')
    @foreach($sliders as $sliderKey => $slider)
        <div class="card {{ !$loop->last ? 'mb-2' : '' }}">
            @if(count($sliders) > 1)
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h5 class="py-2"><strong>{{ __($slider['name']) }}</strong></h5>
                </div>
            @endif
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="sortable-column"></th>
                            <th width="1%">@massControl($routeName)</th>
                            <th ></th>
                            <th width="1%" class="text-center">@lang('cms-core::admin.layout.Manage')</th>
                        </tr>
                        </thead>
                        <tbody class="js-sortable"
                               data-params="{{ json_encode(['model' => encrypt($model)]) }}">
                        @foreach($result[$sliderKey] as $item)
                            <tr data-id="{{ $item->id }}">
                                <td>
                                    <div class="js-sortable-handle sortable-handle">
                                        <i class="fa fa-arrows"></i>
                                    </div>
                                </td>
                                <td>@massCheck($item)</td>

                                <td>
                                    {{ str_limit(strip_tags(html_entity_decode($item->name))) }}
                                </td>
                                <td>
                                    <div class="btn-group list-control-buttons" role="group">
                                        @smallStatus($item)
                                        @editResource($item, false)
                                        @deleteResource($item)
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endforeach
@endsection


