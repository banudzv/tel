@extends('cms-ui::layouts.main', ['lightHeader' => true])

@php
    /**
     * @var $articles \Illuminate\Pagination\LengthAwarePaginator|\WezomCms\Articles\Models\Article[]
     * @var $groups \WezomCms\Articles\Models\ArticleGroup[]
     * @var $tag \WezomCms\Articles\Models\ArticleTag
     * @var $banner string|null
     * @var $subTitle string|null
     */
@endphp

@section('content')
    @widget('ui:heading-section', [
        'bannerImage' => $banner,
        'title' => SEO::getH1(),
        'text' => $subTitle,
        'classes' => 'heading-section--blog',
        'bottomLinks' => $groups
    ])
    <div class="section section--blog">
        <div class="container">
            <div class="_grid _spacer _spacer--md">
                @foreach($articles ?? [] as $article)
                    @if($loop->first)
                        <div class="_cell _cell--12 _md:cell--8 _df:cell--6">
                            @widget('articles:card', ['article' => $article, 'large' => true])
                        </div>
                    @else
                        <div class="_cell _cell--12 _sm:cell--6 _md:cell--4 _df:cell--3">
                            @widget('articles:card', ['article' => $article])
                        </div>
                    @endif
                @endforeach
            </div>
            @if($articles->hasPages())
                <div class="_flex _justify-center _mt-hg">
                    {!! $articles->links() !!}
                </div>
            @endif
        </div>
    </div>
    <div class="_mb-sm">
        @widget('branches:list')
    </div>
@endsection
