<?php

namespace WezomCms\Articles\Widgets;

use Illuminate\Support\Collection;
use WezomCms\Articles\Models\Article;
use WezomCms\Articles\Repositories\ArticleGroupRepository;
use WezomCms\Core\Foundation\Widgets\AbstractWidget;

class Blog extends AbstractWidget
{
    /**
     * A list of models that, when changed, will clear the cache of this widget.
     *
     * @var array
     */
    public static $models = [Article::class];

    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        $repository = resolve(ArticleGroupRepository::class);

        /** @var Collection $models */
        $models = $repository->getByFrontForMain();

        if ($models->isEmpty()) {
            return null;
        }
        $models = $this->prettyData($models);
        if(empty($models)){
            return null;
        }

        return compact('models');
    }

    public function prettyData($data)
    {
        $arr = [];

        foreach ($data ?? [] as $key => $group){
//            dd(route('article-groups.inner', ['slug' => $group->slug]));
            $arr[$key]['name'] = $group->name;
            $arr[$key]['linkMore'] = $group->getFrontUrl();
            foreach($group->articles ?? [] as $k => $article){
                $arr[$key]['articles'][$k]['name'] = $article->name;
                $arr[$key]['articles'][$k]['image'] = $article->getPreview();
                $arr[$key]['articles'][$k]['link'] = $article->getFrontUrl();
                $arr[$key]['articles'][$k]['published_at'] = $article->publishedHuman;
            }
        }

        return $arr;
    }
}
