<?php

namespace WezomCms\Articles\Http\Controllers\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use WezomCms\Articles\Http\Requests\Admin\ArticleRequest;
use WezomCms\Articles\Models\Article;
use WezomCms\Articles\Models\ArticleGroup;
use WezomCms\Articles\Repositories\TagRepository;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Core\Settings\AdminLimit;
use WezomCms\Core\Settings\Fields\AbstractField;
use WezomCms\Core\Settings\Fields\Image;
use WezomCms\Core\Settings\Fields\Textarea;
use WezomCms\Core\Settings\MetaFields\SeoFields;
use WezomCms\Core\Settings\MultilingualGroup;
use WezomCms\Core\Settings\RenderSettings;
use WezomCms\Core\Settings\SiteLimit;
use WezomCms\Core\Settings\Tab;
use WezomCms\Core\Traits\SettingControllerTrait;

class ArticlesController extends AbstractCRUDController
{
    use SettingControllerTrait;

    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-articles::admin';

    /**
     * Resource route name.
     *
     * @var string
     */

    protected $routeName = 'admin.articles';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = ArticleRequest::class;

    /**
     * @var bool
     */
    private $useGroups;

    /**
     * @var bool
     */
    private $useTags;
    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * ArticlesController constructor.
     */
    public function __construct(TagRepository $tagRepository)
    {
        parent::__construct();

        $this->useGroups = (bool) config('cms.articles.articles.use_groups');
        $this->useTags = (bool) config('cms.articles.articles.use_tags');

        $this->tagRepository = $tagRepository;
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-articles::admin.Articles');
    }

    /**
     * @return string|null
     */
    protected function frontUrl(): ?string
    {
        return $this->useGroups ? null : route('articles');
    }

    /**
     * @param $result
     * @param  Request  $request
     */
    protected function selectionIndexResult($result, Request $request)
    {
        $result->latest('published_at')
            ->latest('id');
    }

    /**
     * @param  Article  $obj
     * @param  array  $viewData
     * @return array
     */
    protected function formData($obj, array $viewData)
    {
        $groups = [];

        if ($this->useGroups) {
            $groups = ArticleGroup::getForSelect();
        }

        return compact('groups');
    }

    /**
     * @param  Article  $obj
     * @param  FormRequest  $request
     * @return array
     */
    protected function fill($obj, FormRequest $request): array
    {
        $data = parent::fill($obj, $request);

        if ($this->useGroups) {
            $data['article_group_id'] = $request->get('article_group_id');
        }

        $data['published_at'] = strtotime($request->get('published_at') . ' ' . date('H:i:s'));

        return $data;
    }

    /**
     * @param  Article  $model
     * @param  Request  $request
     */
    protected function afterSuccessfulSave($model, Request $request)
    {
        if ($this->useTags) {
            $model->tags()->sync($request->get('tags', []));
        }
    }

    /**
     * @param  Article  $model
     * @param  array  $viewData
     * @return array
     */
    protected function createViewData($model, array $viewData): array
    {
        $tags = [];
        if ($this->useTags) {
            $tags = $this->tagRepository->getBySelect();
        }

        $groups = [];
        if ($this->useGroups) {
            $groups = ArticleGroup::getForSelect();
        }


        return [
            'groups' => $groups,
            'tags' => $tags,
            'selectedTags' => [],
        ];
    }

    /**
     * @param  Article  $model
     * @param  array  $viewData
     * @return array
     */
    protected function editViewData($model, array $viewData): array
    {
        $tags = [];
        $selectedTags = [];
        if ($this->useTags) {
            $tags = $this->tagRepository->getBySelect();
            $selectedTags = $model->tags()->pluck('article_tag_id')->toArray();
        }

        $groups = [];
        if ($this->useGroups) {
            $groups = ArticleGroup::getForSelect();
        }

        return [
            'groups' => $groups,
            'tags' => $tags,
            'selectedTags' => $selectedTags,
        ];
    }

    /**
     * @return array|AbstractField[]|MultilingualGroup[]
     * @throws \Exception
     */
    protected function settings()
    {
        $subTitle = Textarea::make()
            ->setKey('sub_title')
            ->setName(__('cms-articles::admin.Sub title'))
            ->setIsMultilingual()
            ->setRows(3)
            ->setSort(5);

        $result = [
            SiteLimit::make()->setName(__('cms-articles::admin.Site articles limit at page')),
            SeoFields::make('Articles', [$subTitle]),
            AdminLimit::make()
        ];

        $tabs = new RenderSettings(
            new Tab('page', __('cms-core::admin.tabs.site banner'), 1, 'fa-folder-o')
        );

        $result[] = Image::make($tabs)
            ->setSettings([
                'images' => [
                    'directory' => 'settings',
                    'default' => 'big', // For admin image preview
                    'sizes' => [
                        'big' => [
                            'width' => 1920,
                            'height' => 700,
                            'mode' => 'resize',
                        ],
                    ],
                ],
            ])
            ->setName(__('cms-articles::admin.image-size', ['width' => 1920, 'height' => 700]))
            ->setSort(1)
            ->setKey('image')
            ->setRules('nullable|file');

        return $result;
    }
}
