<?php

namespace WezomCms\Articles\Http\Controllers\Site;

use WezomCms\Articles\Models\Article;
use WezomCms\Articles\Models\ArticleGroup;
use WezomCms\Articles\Repositories\ArticleGroupRepository;
use WezomCms\Articles\Repositories\ArticleRepository;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Core\Traits\ImageFromSettings;

class ArticleGroupsController extends SiteController
{
    use ImageFromSettings;

    /**
     * @var ArticleGroupRepository
     */
    private $articleGroupRepository;

    private $limit;
    private $settings;

    public function __construct(
        ArticleGroupRepository $articleGroupRepository
    )
    {
        $this->articleGroupRepository = $articleGroupRepository;
        $this->settings = settings('articles.site', []);
        $this->limit = array_get($this->settings, 'limit', Article::DEFAULT_LIMIT);
    }

    public function index()
    {
        $settings = settings('article-groups.site', []);

        $pageName = array_get($settings, 'name');

        $result = ArticleGroup::published()
            ->has('publishedArticles')
            ->orderBy('sort')
            ->latest('id')
            ->paginate(array_get($settings, 'limit', 6));

        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('blog'));

        // SEO
        $this->seo()
            ->setTitle(array_get($settings, 'title'))
            ->setPageName($pageName)
            ->setH1(array_get($settings, 'h1'))
            ->setDescription(array_get($settings, 'description'))
            ->metatags()
            ->setKeywords(array_get($settings, 'keywords'))
            ->setNext($result->nextPageUrl())
            ->setPrev($result->previousPageUrl());

        return view('cms-articles::site.groups.index', [
            'result' => $result
        ]);
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inner($slug)
    {
        /** @var ArticleGroup $group */
        $groups = $this->articleGroupRepository->getByFront();
        $group = $this->articleGroupRepository->getBySlugForFront($slug);

        $this->setLangSwitchers($group, 'article-groups');

        $articles = $group->publishedArticles()
            ->with(['tags'])
            ->latest('published_at')
            ->latest('id')
            ->paginate($this->limit);

        // Breadcrumbs
        $this->addBreadcrumb(settings('article-groups.site.name'), $group->getFrontUrl());
        $this->addBreadcrumb(array_get($this->settings, 'name'), route('blog'));
//        $this->addBreadcrumb($group->name, $group->getFrontUrl());

        // SEO
        $this->seo()
            ->setTitle($group->title)
            ->setH1($group->h1)
            ->setPageName($group->name)
            ->setDescription($group->description)
            ->metatags()
            ->setKeywords($group->keywords)
            ->setNext($articles->nextPageUrl())
            ->setPrev($articles->previousPageUrl());

        // Render
        return view('cms-articles::site.groups.inner', [
            'articles' => $articles,
            'groups' => $groups,
            'banner' => $this->getBannerUrl('articles.page'),
            'subTitle' => array_get($this->settings, 'sub_title')
        ]);
    }
}
