<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
		'Reports' => 'Отчеты',
		'Report' => 'Отчет',
		'Categories report' => 'Категории отчетов',
		'Category' => 'Категория',
        'Parent' => 'Родительский элемент',
        'Root' => 'Корневой элемент',
        'Published at' => 'Дата публикации',
        'File' => 'Файл',
        'image-size' => 'Рекомендуемый размер :width x :height px',
        'Sub title' => 'Подзаголовок',
        'Time' => 'Время',
        'For main' => 'Для гл. стр.',
    ],
    TranslationSide::SITE => [
        'Choice category' => 'Выберите категорию',
        'Choice sub-category' => 'Выберите подкатегорию',
        'report title' => 'Отчетность',
        'report description' => 'Мы гордимся тем, что финансовая деятельность СК "Теком" являеться публичной и открытой. Предлагаем ознакомиться с финансовыми показателями и результатами СК "Теком"',
        'report button' => 'Подробнее о финансовых показателях',
        'Choice year' => 'Выберите год'
    ],
];
