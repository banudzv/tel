<option value="">@lang('cms-reports::admin.Category')</option>
@foreach($tree as $key => $category)
    <option value="{{ $key }}" {{ $key == request()->get($name ?? 'category_id') ? 'selected': null }}
        {{ $category['disabled'] ?? false ? 'disabled' : null }}>{!! $category['name'] !!}</option>
@endforeach
