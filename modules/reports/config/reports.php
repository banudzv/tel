<?php

use WezomCms\Reports\Dashboards;
use WezomCms\Reports\Widgets;

return [
    'max_depth' => 3,
    'dashboards' => [
        Dashboards\ReportDashboard::class,
    ],
    'widgets' => [
        'reports:report-for-main' => Widgets\ReportForMain::class,
    ]
];
