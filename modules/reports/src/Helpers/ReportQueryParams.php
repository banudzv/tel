<?php

namespace WezomCms\Reports\Helpers;

class ReportQueryParams
{
    const YEAR = 'year';
    const CATEGORY = 'category';
    const SUB_CATEGORY = 'sub_category';
}
