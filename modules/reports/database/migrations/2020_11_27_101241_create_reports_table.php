<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reports', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('sort')->default(0);
			$table->boolean('published')->default(true);
            $table->unsignedBigInteger('category_id')->nullable();
            $table->dateTime('published_at');
            $table->string('file')->nullable();
            $table->timestamps();

            $table->foreign('category_id')
                ->references('id')
                ->on('report_categories')
                ->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('reports');
	}
}
