<?php

namespace WezomCms\Sitemap\Commands;

use Exception;
use Illuminate\Console\Command;
use Mcamara\LaravelLocalization\LaravelLocalization;
use WezomCms\Sitemap\SitemapXmlGenerator;

class SitemapCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate {--path=public} {--file=sitemap.xml}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate sitemap.xml';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->option('path');

        $file = trim($path, '/\\') . '/' . $this->option('file');

        try {
            $sitemap = new SitemapXmlGenerator($file, app('locales'), app(LaravelLocalization::class));

            $sitemap->start();

            event('sitemap:xml', $sitemap);

            $sitemap->save();
        } catch (Exception $e) {
            $this->error('Error to open stream for writing ' . $file);
            $this->error('Message: ' . $e->getMessage());

            return;
        }

        $this->info('Done');
    }
}
