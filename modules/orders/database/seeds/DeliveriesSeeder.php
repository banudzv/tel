<?php

namespace WezomCms\Orders\Database\Seeds;

use Illuminate\Database\Seeder;
use WezomCms\Orders\Drivers\Delivery\NovaPoshta;
use WezomCms\Orders\Enums\DeliveryTypes;
use WezomCms\Orders\Models\Delivery;

class DeliveriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Новая почта',
                'type' => DeliveryTypes::POSTAL,
                'driver' => NovaPoshta::KEY,
            ],
            [
                'name' => 'Новая почта',
                'type' => DeliveryTypes::COURIER,
                'driver' => NovaPoshta::KEY,
            ],
        ];

        foreach ($data as $index => $datum) {
            $obj = new Delivery();
            $obj->sort = $index;
            $obj->published = true;
            $obj->type = $datum['type'];
            $obj->driver = $datum['driver'];

            $translatable = [];

            foreach (app('locales') as $locale => $language) {
                $translatable[$locale] = [
                    'name' => $datum['name'],
                ];
            }

            $obj->fill($translatable);
            $obj->save();
        }
    }
}
