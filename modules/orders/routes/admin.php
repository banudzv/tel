<?php

Route::namespace('WezomCms\\Orders\\Http\\Controllers\\Admin')
    ->group(function () {
        // Orders
        Route::adminResource('orders', 'OrdersController')->softDeletes()->show()->settings();
        Route::get('orders/{id}/add-item', 'OrdersController@addItem')->name('orders.add-item');
        Route::post('orders/{id}/store-item', 'OrdersController@storeItem')->name('orders.store-item');
        Route::get('orders/{id}/delete-item/{item_id}', 'OrdersController@deleteItem')->name('orders.delete-item');

        // Statuses
        Route::adminResource('order-statuses', 'OrderStatusesController');

        //Deliveries
        Route::adminResource('deliveries', 'DeliveriesController')->settings();

        //Payments
        Route::adminResource('payments', 'PaymentsController')->settings();

        // Delivery and payment
        Route::settings('delivery-and-payment', 'DeliveryAndPaymentController');

        Route::adminResource('delivery-variants', 'DeliveryVariantsController');

        Route::adminResource('payment-variants', 'PaymentVariantsController');
    });
