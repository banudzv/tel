<?php

Route::namespace('WezomCms\\Orders\\Http\\Controllers\\Site')
    ->group(function () {
        // Static pages
        Route::get('thanks/{id}', 'ThanksController@index')
            ->name('thanks-page')
            ->where('id', '\d+');

        Route::get('delivery-and-payment', 'DeliveryAndPaymentController@index')->name('delivery-and-payment');

        Route::post('payment-callback/{id}/{driver}', 'OrderPaymentController@callback')
            ->name('payment-callback')
            ->where('id', '\d+');

        // Cart routes
        Route::post('cart/add', 'CartController@add')->name('cart.add');
        Route::post('cart/update', 'CartController@update')->name('cart.update');
        Route::post('cart/remove', 'CartController@remove')->name('cart.remove');
        Route::get('cart/view', 'CartController@view')->name('cart.view');
        Route::post('cart/remove-conditions', 'CartController@removeAllConditions')->name('cart.remove-conditions');

        // Checkout
        Route::group(['as' => 'checkout.'], function () {
            Route::get('checkout', 'CheckoutController@step1')->name('step1');

            Route::post('checkout', 'CheckoutController@saveStep1')->name('save-step1');

            Route::get('checkout/delivery', 'CheckoutController@step2')->name('step2');

            Route::post('checkout/delivery', 'CheckoutController@saveStep2')->name('save-step2');
        });

        // Cabinet orders
        Route::get('orders', 'CabinetOrdersController@index')
            ->middleware('auth', 'verified')
            ->name('cabinet.orders');

        // Cabinet cart
        Route::get('cart', 'CabinetOrdersController@cart')
            ->middleware('auth', 'verified')
            ->name('cabinet.cart');

        // Nova Poshta routes
        Route::get('nova-poshta/get-cities', 'NovaPoshtaController@getCities')
            ->name('nova-poshta.get-cities');

        Route::get('nova-poshta/get-city-warehouses', 'NovaPoshtaController@getCityWarehouses')
            ->name('nova-poshta.get-city-warehouses');
    });
