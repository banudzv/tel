<?php

namespace WezomCms\Orders\Drivers\Delivery;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use WezomCms\Orders\Contracts\DeliveryDriverInterface;
use WezomCms\Orders\Enums\DeliveryTypes;

class NovaPoshta implements DeliveryDriverInterface
{
    public const KEY = 'nova-poshta';

    /**
     * @param  array  $stepData
     * @param $deliveryType
     * @return mixed
     */
    public function renderFormInputs(array $stepData, $deliveryType)
    {
        switch ($deliveryType) {
            case DeliveryTypes::POSTAL:
                return view('cms-orders::site.delivery.np-postal', [
                    'stepData' => $stepData,
                ]);
                break;
            case DeliveryTypes::COURIER:
                return view('cms-orders::site.delivery.address', [
                    'stepData' => $stepData,
                ]);
                break;
            default:
                return '';
                break;
        }
    }

    /**
     * Validate incoming data
     *
     * @param  Request  $request
     * @return array
     * @throws ValidationException
     */
    public function validateData(Request $request): array
    {
        switch ($request->get('delivery')) {
            case DeliveryTypes::POSTAL:
                $rules = [
                    'city' => 'required|string',
                    'postal_office' => 'required|string',
                ];
                $attributes = [
                    'city' => __('cms-orders::site.checkout.City'),
                    'postal_office' => __('cms-orders::site.checkout.Postal office'),
                ];
                break;
            case DeliveryTypes::COURIER:
                $rules = ['address' => 'required|string'];
                $attributes = ['address' => __('cms-orders::site.checkout.Address')];
                break;
            default:
                return [];
                break;
        }

        return \Validator::make(
            $request->all(),
            $rules ?? [],
            $messages ?? [],
            $attributes ?? []
        )->validate();
    }

    /**
     * Prepare data for saving in order.
     *
     * @param  Request  $request
     * @return array
     */
    public function prepareStoreData(Request $request): array
    {
        switch ($request->get('delivery')) {
            case DeliveryTypes::POSTAL:
                $result = ['delivery_address' => $request->get('city') . ' ' . $request->get('postal_office')];
                break;
            case DeliveryTypes::COURIER:
                $result = ['delivery_address' => $request->get('address')];
                break;
            default:
                return [];
                break;
        }

        return $result;
    }
}
