<?php

namespace WezomCms\Orders\Models;

use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use WezomCms\Core\Traits\Model\Filterable;
use WezomCms\Orders\Enums\DeliveryTypes;
use WezomCms\Users\Models\User;

/**
 * WezomCms\Orders\Models\Order
 *
 * @property int $id
 * @property int|null $delivery_id
 * @property string|null $delivery_address
 * @property int|null $payment_id
 * @property int|null $status_id
 * @property int|null $user_id
 * @property bool $payed
 * @property bool $dont_call_back
 * @property string|null $payed_mode
 * @property \Illuminate\Support\Carbon|null $payed_at
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \WezomCms\Orders\Models\Delivery $delivery
 * @property-read string $currency_iso_code
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Orders\Models\OrderItem[] $items
 * @property-read \WezomCms\Orders\Models\Payment $payment
 * @property-read \WezomCms\Orders\Models\OrderStatus $status
 * @property-read \WezomCms\Users\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Order filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Order new()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeliveryAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeliveryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePayed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePayedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePayedMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePaymentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUserId($value)
 * @mixin \Eloquent
 * @property-read \WezomCms\Orders\Models\OrderRecipient $recipient
 * @property-read \WezomCms\Orders\Models\OrderClient $client
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Orders\Models\OrderStatus[] $statusHistory
 * @property-read string $quantity
 * @property-read float $whole_purchase_price
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\WezomCms\Orders\Models\Order onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\WezomCms\Orders\Models\Order withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\WezomCms\Orders\Models\Order withoutTrashed()
 * @property-read string $formatted_price
 * @property-read string $formatted_whole_price
 * @property-read mixed $discount
 * @property-read string $formatted_discount
 * @property string|null $delivery_type
 * @property-read string $full_delivery_type
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Orders\Models\Order whereDeliveryType($value)
 * @property string|null $ttn
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Orders\Models\Order whereTtn($value)
 */
class Order extends Model
{
    use Filterable;
    use SoftDeletes;
    use EloquentTentacle;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['delivery_type', 'delivery_address', 'dont_call_back', 'ttn'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['payed' => 'bool', 'dont_call_back' => 'bool'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['payed_at'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['items', 'items.product', 'status', 'statusHistory', 'user', 'recipient', 'client'];

    /**
     * @param  Builder|Order  $query
     */
    public function scopeNew(Builder $query)
    {
        $query->whereStatusId(OrderStatus::NEW);
    }

    /**
     * @param $value
     */
    public function setPayedAttribute($value)
    {
        $value = $this->castAttribute('payed', $value);

        if ($value !== $this->payed) {
            $this->attributes['payed_at'] = $value ? now() : null;
        }

        $this->attributes['payed'] = $value;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function delivery()
    {
        return $this->belongsTo(Delivery::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function client()
    {
        return $this->hasOne(OrderClient::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function recipient()
    {
        return $this->hasOne(OrderRecipient::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return string
     */
    public function getCurrencyIsoCodeAttribute()
    {
        return 'UAH';
    }

    /**
     * @param  OrderStatus|null  $status
     * @return Order
     */
    public function changeStatus(?OrderStatus $status)
    {
        $this->status()->associate($status);

        if ($this->isDirty('status_id')) {
            $this->statusHistory()->attach($status->id);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getFullDeliveryTypeAttribute()
    {
        return DeliveryTypes::getDescription($this->delivery_type) . ' '
            . ($this->delivery ? $this->delivery->name : '');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function statusHistory()
    {
        return $this->belongsToMany(OrderStatus::class, 'order_status_history', 'order_id', 'status_id')
            ->withPivot('created_at')
            ->orderBy('order_status_history.created_at');
    }

    /**
     * @return float
     */
    public function getWholePurchasePriceAttribute()
    {
        return $this->items->sum('whole_purchase_price');
    }

    /**
     * @return string
     */
    public function getQuantityAttribute()
    {
        return $this->items->count();
    }

    /**
     * @return mixed
     */
    public function getDiscountAttribute()
    {
        return $this->items->sum('discount');
    }
}
