<?php

namespace WezomCms\Orders\CheckoutStorage;

use Illuminate\Session\SessionManager;
use Illuminate\Session\Store;
use Illuminate\Support\Arr;
use stdClass;
use WezomCms\Orders\Contracts\CheckoutStorageInterface;

class Session implements CheckoutStorageInterface
{
    protected const KEY = 'checkout';

    /**
     * @var SessionManager|Store
     */
    private $session;

    /**
     * SessionCheckoutStorage constructor.
     * @param  SessionManager  $session
     */
    public function __construct(SessionManager $session)
    {
        $this->session = $session;
    }

    /**
     * @param  array  $data
     * @return CheckoutStorageInterface
     */
    public function put(array $data): CheckoutStorageInterface
    {
        $this->session->put(static::KEY, $data);

        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return CheckoutStorageInterface
     */
    public function set($key, $value): CheckoutStorageInterface
    {
        $this->session->put(static::KEY . '.' . $key, $value);

        return $this;
    }

    /**
     * @param  string|array  $key
     * @param  null  $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return $this->session->get(static::KEY . '.' . $key, $default);
    }

    /**
     * Get a subset containing the provided keys with values from the storage.
     *
     * @param  array|mixed  $keys
     * @return array
     */
    public function only($keys): array
    {
        $results = [];

        $input = $this->getAll();

        $placeholder = new stdClass();

        foreach (is_array($keys) ? $keys : func_get_args() as $key) {
            $value = data_get($input, $key, $placeholder);

            if ($value !== $placeholder) {
                Arr::set($results, $key, $value);
            }
        }

        return $results;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->session->get(static::KEY, []);
    }

    /**
     * @return void
     */
    public function clear()
    {
        return $this->session->forget(static::KEY);
    }
}
