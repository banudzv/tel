<?php

namespace WezomCms\Orders\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Rules\Phone;

class Step1Request extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:2|max:255',
            'surname' => 'required|string|min:2|max:255',
            'email' => 'nullable|email',
            'phone' => ['required', 'string', new Phone()],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => __('cms-orders::site.checkout.Name'),
            'surname' => __('cms-orders::site.checkout.Surname'),
            'email' => __('cms-orders::site.checkout.Email'),
            'phone' => __('cms-orders::site.checkout.Phone'),
        ];
    }
}
