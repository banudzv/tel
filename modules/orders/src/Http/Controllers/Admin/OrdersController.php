<?php

namespace WezomCms\Orders\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;
use WezomCms\Catalog\Models\Category;
use WezomCms\Catalog\Models\Product;
use WezomCms\Core\Contracts\ButtonInterface;
use WezomCms\Core\Contracts\ButtonsContainerInterface;
use WezomCms\Core\Foundation\Buttons\ButtonsMaker;
use WezomCms\Core\Foundation\Helpers;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Core\Settings\AdminLimit;
use WezomCms\Core\Settings\Fields\AbstractField;
use WezomCms\Core\Settings\Fields\Text;
use WezomCms\Core\Settings\MetaFields\Heading;
use WezomCms\Core\Settings\MetaFields\Title;
use WezomCms\Core\Settings\MultilingualGroup;
use WezomCms\Core\Settings\PageName;
use WezomCms\Core\Settings\RenderSettings;
use WezomCms\Core\Settings\SiteLimit;
use WezomCms\Core\Settings\Tab;
use WezomCms\Core\Traits\ActionShowTrait;
use WezomCms\Core\Traits\Model\Filterable;
use WezomCms\Core\Traits\SettingControllerTrait;
use WezomCms\Core\Traits\SoftDeletesActionsTrait;
use WezomCms\Orders\Enums\DeliveryTypes;
use WezomCms\Orders\Enums\PayedModes;
use WezomCms\Orders\Events\CreatedOrder;
use WezomCms\Orders\Http\Requests\Admin\AddOrderItemRequest;
use WezomCms\Orders\Http\Requests\Admin\CreateOrderRequest;
use WezomCms\Orders\Http\Requests\Admin\UpdateOrderRequest;
use WezomCms\Orders\ModelFilters\OrderTrashedFilter;
use WezomCms\Orders\Models\Delivery;
use WezomCms\Orders\Models\Order;
use WezomCms\Orders\Models\OrderStatus;
use WezomCms\Orders\Models\Payment;

class OrdersController extends AbstractCRUDController
{
    use ActionShowTrait;
    use SettingControllerTrait;
    use SoftDeletesActionsTrait;

    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-orders::admin.orders';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.orders';

    /**
     * Form request class name for "create" action.
     *
     * @var string
     */
    protected $createRequest = CreateOrderRequest::class;

    /**
     * Form request class name for "update" action.
     *
     * @var string
     */
    protected $updateRequest = UpdateOrderRequest::class;

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-orders::admin.orders.Orders');
    }

    /**
     * @param  Model|Order  $model
     * @return string
     */
    protected function showTitle(Model $model): string
    {
        return __(
            'cms-orders::admin.orders.Order: :number from: :date',
            ['number' => $model->id, 'date' => $model->created_at->format('d.m.Y H:i')]
        );
    }

    /**
     * @return string|object|Filterable|null
     */
    protected function trashedFilter()
    {
        return OrderTrashedFilter::class;
    }

    /**
     * @param  Order  $obj
     * @param  array  $viewData
     * @return array
     */
    protected function createViewData($obj, array $viewData): array
    {
        return [
            'statuses' => OrderStatus::getForSelect(false),
            'payments' => Payment::getForSelect(false),
            'users' => [],
            'deliveryTypes' => array_prepend(DeliveryTypes::toSelectArray(), __('cms-core::admin.layout.Not set'), ''),
            'deliveries' => Delivery::orderBy('sort')->latest('id')->get(),
        ];
    }

    /**
     * @param  Order  $obj
     * @param  array  $viewData
     * @return array
     */
    protected function editViewData($obj, array $viewData): array
    {
        $obj->load(['items.product' => function ($query) {
            $query->withTrashed();
        }]);

        $data = $this->createViewData($obj, $viewData);

        // Restore users select
        $users = ['' => __('cms-core::admin.layout.Not set')];
        if ($obj->user) {
            $users[$obj->user->id] = $obj->user->fullname;
        }

        $data['users'] = $users;

        return $data;
    }

    /**
     * @param  Model|Order  $obj
     * @param  FormRequest  $request
     * @return array
     */
    protected function fillStoreData($obj, FormRequest $request): array
    {
        $obj->payment()->associate($request->get('payment_id'));
        $obj->payed = $request->boolean('payed');
        $obj->payed_mode = PayedModes::MANUAL;

        $obj->delivery()->associate($request->get('delivery_id'));

        if (Helpers::providerLoaded('WezomCms\Users\UsersServiceProvider')) {
            $obj->user()->associate($request->get('user_id'));
        }

        return parent::fillStoreData($obj, $request);
    }

    /**
     * @param  Order  $obj
     * @param  FormRequest  $request
     */
    protected function afterSuccessfulStore($obj, FormRequest $request)
    {
        $obj->changeStatus(OrderStatus::find($request->get('status_id')))->save();

        $obj->client()->create($request->get('client', []));

        $obj->recipient()->create($request->get('recipient', []));
    }

    /**
     * @param  Order  $obj
     * @param  FormRequest  $request
     * @return array
     */
    protected function fillUpdateData($obj, FormRequest $request): array
    {
        $obj->changeStatus(OrderStatus::find($request->get('status_id')));

        $obj->payment()->associate($request->get('payment_id'));
        $obj->payed = $request->get('payed');

        // If manually changed payed status
        if ($obj->isDirty('payed')) {
            $obj->payed_mode = PayedModes::MANUAL;
        }

        $obj->delivery()->associate($request->get('delivery_id'));

        if (Helpers::providerLoaded('WezomCms\Users\UsersServiceProvider')) {
            $obj->user()->associate($request->get('user_id'));
        }

        return parent::fillUpdateData($obj, $request);
    }

    /**
     * @param  Order  $obj
     * @param  FormRequest  $request
     */
    public function afterSuccessfulUpdate($obj, FormRequest $request)
    {
        foreach ($request->get('QUANTITY', []) as $id => $quantity) {
            $obj->items()->whereKey($id)->update(compact('quantity'));
        }

        $obj->client()->updateOrCreate([], $request->get('client', []));

        $obj->recipient()->updateOrCreate([], $request->get('recipient', []));
    }

    /**
     * @param $id
     * @param  ButtonsContainerInterface  $buttonsContainer
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function addItem($id, ButtonsContainerInterface $buttonsContainer)
    {
        $order = Order::findOrFail($id);

        $this->authorizeForAction('edit', $order);

        $this->before();

        $buttonsContainer->add(ButtonsMaker::save())
            ->add(ButtonsMaker::saveAndClose(route('admin.orders.edit', $order->id)))
            ->add(ButtonsMaker::close(route('admin.orders.edit', $order->id)));

        $this->addBreadcrumb(
            __(
                'cms-orders::admin.orders.Order: :number from: :date',
                ['number' => $order->id, 'date' => $order->created_at->format('d.m.Y H:i')]
            )
        );

        $this->pageName->setPageName(__('cms-orders::admin.orders.Add item'));
        $this->addBreadcrumb(__('cms-orders::admin.orders.Add item'));
        $this->renderJsValidator(new AddOrderItemRequest());

        return view('cms-orders::admin.orders.add-item', [
            'routeName' => $this->routeName,
            'obj' => $order,
            'categoriesTree' => Category::getForSelect(),
        ]);
    }

    /**
     * @param $id
     * @param  AddOrderItemRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function storeItem($id, AddOrderItemRequest $request)
    {
        $order = Order::findOrFail($id);

        $this->authorizeForAction('edit', $order);

        $product = Product::findOrFail($request->get('product_id'));

        $order->items()->create([
            'product_id' => $request->get('product_id'),
            'quantity' => $request->get('quantity', $product->minCountForPurchase()),
            'price' => $product->cost,
            'purchase_price' => $product->cost,
        ]);

        event(new CreatedOrder($order));

        flash(__('cms-orders::admin.orders.Item successfully stored'))->success();

        // Redirect
        switch (app('request')->get('form-action')) {
            case ButtonInterface::ACTION_SAVE_AND_CLOSE:
                return redirect()->route('admin.orders.edit', $order->id);
                break;
            case ButtonInterface::ACTION_SAVE:
            default:
                if (ButtonInterface::ACTION_STORE === app(Route::class)->getActionMethod()) {
                    return redirect()->route('admin.orders.edit', [$order->id]);
                }

                return redirect()->back();
                break;
        }
    }

    /**
     * @param $id
     * @param $itemId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function deleteItem($id, $itemId)
    {
        $obj = Order::findOrFail($id);

        $this->authorizeForAction('edit', $obj);

        if ($obj->items()->where('id', $itemId)->delete()) {
            flash(__('cms-core::admin.layout.Data deleted successfully'))->success();
        } else {
            flash(__('cms-core::admin.layout.Data deletion error'))->error();
        }

        return redirect()->back();
    }

    /**
     * @return array|AbstractField[]|MultilingualGroup[]
     * @throws \Exception
     */
    protected function settings(): array
    {
        $siteRenderSettings = RenderSettings::siteTab();

        $siteThanksRenderSettings = new RenderSettings(
            new Tab('site_thanks', __('cms-orders::admin.orders.Thanks page'), 2, 'fa-file-text')
        );
        $siteCabinetCartRenderSettings = new RenderSettings(
            new Tab('cabinet_cart', __('cms-orders::admin.orders.Cabinet cart'), 3, 'fa-file-text')
        );

        $thanksText = Text::make()
            ->setIsMultilingual()
            ->setName(__('cms-orders::admin.thanks.Page text'))
            ->setKey('text')
            ->setSort(4);

        $thanksAuxiliaryText = Text::make()
            ->setIsMultilingual()
            ->setName(__('cms-orders::admin.thanks.Page auxiliary text'))
            ->setKey('auxiliary_text')
            ->setSort(5);

        $items = [PageName::make()->default('Orders'), Title::make(), Heading::make()];
        $thanksItems = [$thanksText, $thanksAuxiliaryText, PageName::make()->default('Thanks'), Title::make(), Heading::make()];
        $cabinetCartItems = [PageName::make()->default('Cart'), Title::make(), Heading::make()];

        return [
            SiteLimit::make()->setName(__('cms-orders::admin.orders.Limit orders at page in LK')),
            MultilingualGroup::make($siteRenderSettings, $items),
            MultilingualGroup::make($siteThanksRenderSettings, $thanksItems),
            MultilingualGroup::make($siteCabinetCartRenderSettings, $cabinetCartItems),
            AdminLimit::make(),
        ];
    }

    /**
     * @param  Order  $obj
     * @param  array  $viewData
     * @return array
     */
    protected function showViewData($obj, array $viewData): array
    {
        return [
            'status' => $obj->status ? $obj->status->name : '',
            'payment' => $obj->payment ? $obj->payment->name : '',
            'payed' => $obj->payed ? __('cms-core::admin.layout.Yes') : __('cms-core::admin.layout.No'),
            'deliveryType' => DeliveryTypes::getDescription($obj->delivery_type),
            'delivery' => $obj->delivery ? $obj->delivery->name : '',
        ];
    }
}
