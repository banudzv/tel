<?php

namespace WezomCms\Orders\Http\Controllers\Site;

use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Orders\Models\Order;

class CabinetOrdersController extends SiteController
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        // Meta
        $this->addBreadcrumb(settings('orders.site.name'));
        $this->seo()
            ->setPageName(settings('orders.site.name'))
            ->setTitle(settings('orders.site.title'))
            ->setH1(settings('orders.site.h1'));

        // Select orders
        $orders = Order::where('user_id', Auth::id())
            ->with('items', 'items.product', 'delivery', 'payment', 'client', 'recipient', 'status', 'statusHistory')
            ->latest('id')
            ->paginate(settings('orders.site.limit', 10));

        // Render
        return view('cms-orders::site.cabinet.orders', [
            'orders' => $orders,
        ]);
    }

    /**
     * @return Factory|View
     */
    public function cart()
    {
        // Meta
        $this->addBreadcrumb(settings('orders.cabinet_cart.name'));
        $this->seo()
            ->setTitle(settings('orders.cabinet_cart.title'))
            ->setH1(settings('orders.cabinet_cart.h1'));

        // Render
        return view('cms-orders::site.cabinet.cart');
    }
}
