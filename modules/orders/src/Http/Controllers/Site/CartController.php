<?php

namespace WezomCms\Orders\Http\Controllers\Site;

use Cart;
use WezomCms\Catalog\Models\Product;
use WezomCms\Core\Foundation\JsResponse;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Orders\Contracts\CartAdapterInterface;
use WezomCms\Orders\Contracts\CartItemInterface;
use WezomCms\Orders\Contracts\PurchasedProductInterface;
use WezomCms\Orders\Http\Requests\Site\Cart\AddProductRequest;
use WezomCms\Orders\Http\Requests\Site\Cart\RemoveCartItemRequest;
use WezomCms\Orders\Http\Requests\Site\Cart\UpdateCartItemRequest;

class CartController extends SiteController
{
    /**
     * @param  AddProductRequest  $request
     * @return JsResponse
     */
    public function add(AddProductRequest $request)
    {
        /** @var Product $product */
        $product = Product::published()->find($request->get('id'));

        if (!$product->availableForPurchase()) {
            return $this->failedResponse(__('cms-orders::site.cart.Product cannot be purchased'));
        }

        Cart::add($product->id, $product->cost, $this->normalizeCount($product, $request->get('count', 1)));

        return $this->view();
    }

    /**
     * @param  UpdateCartItemRequest  $request
     * @return JsResponse
     */
    public function update(UpdateCartItemRequest $request)
    {
        $rowId = $request->get('row_id');

        /** @var CartItemInterface|null $cartItem */
        $cartItem = Cart::get($rowId);
        if (null === $cartItem) {
            return $this->failedResponse();
        }

        $product = Product::published()->find($cartItem->getId());
        if (null === $product) {
            return $this->failedResponse();
        }

        Cart::setQuantity($rowId, $this->normalizeCount($product, $request->get('count', 1)));

        return $this->view();
    }

    /**
     * @param  RemoveCartItemRequest  $request
     * @return JsResponse
     */
    public function remove(RemoveCartItemRequest $request)
    {
        Cart::remove($request->get('row_id'));

        return $this->view();
    }

    /**
     * @return JsResponse
     */
    public function removeAllConditions()
    {
        Cart::clearConditions();

        return $this->view();
    }

    /**
     * @return JsResponse
     */
    public function view()
    {
        return JsResponse::make()->set('cart', app(CartAdapterInterface::class)->adapt());
    }

    /**
     * @param  string|null  $message
     * @param  int  $time
     * @return JsResponse
     */
    private function failedResponse(string $message = null, int $time = 5): JsResponse
    {
        $response = JsResponse::make()->success(false);

        if ($message !== null) {
            $response->notification($message, 'warning', $time);
        }

        return $response;
    }

    /**
     * @param  PurchasedProductInterface  $product
     * @param $count
     * @return float|int
     */
    private function normalizeCount(PurchasedProductInterface $product, $count)
    {
        if (is_int($product->minCountForPurchase())) {
            return (int) $count;
        } else {
            return (float) str_replace(',', '.', $count);
        }
    }
}
