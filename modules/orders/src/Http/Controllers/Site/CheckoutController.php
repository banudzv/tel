<?php

namespace WezomCms\Orders\Http\Controllers\Site;

use Auth;
use Cart;
use Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use NotifyMessage;
use WezomCms\Catalog\Models\Product;
use WezomCms\Core\Foundation\Helpers;
use WezomCms\Core\Foundation\JsResponse;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Orders\Contracts\CartItemInterface;
use WezomCms\Orders\Contracts\CheckoutStorageInterface;
use WezomCms\Orders\Enums\DeliveryTypes;
use WezomCms\Orders\Events\CreatedOrder;
use WezomCms\Orders\Http\Requests\Site\Step1Request;
use WezomCms\Orders\Http\Requests\Site\Step2Request;
use WezomCms\Orders\Models\Delivery;
use WezomCms\Orders\Models\Order;
use WezomCms\Orders\Models\OrderStatus;
use WezomCms\Orders\Models\Payment;
use WezomCms\Users\Events\AutoRegistered;
use WezomCms\Users\Models\User;

class CheckoutController extends SiteController
{
    public const FIRST_STEP_FIELDS = ['name', 'surname', 'email', 'phone'];

    /**
     * @var CheckoutStorageInterface
     */
    private $checkoutStorage;

    /**
     * Module "Users" is loaded.
     *
     * @var bool
     */
    private $usersLoaded = false;

    /**
     * CheckoutController constructor.
     * @param  CheckoutStorageInterface  $checkoutStorage
     */
    public function __construct(CheckoutStorageInterface $checkoutStorage)
    {
        $this->checkoutStorage = $checkoutStorage;

        $this->usersLoaded = Helpers::providerLoaded('WezomCms\\Users\\UsersServiceProvider');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function step1()
    {
        if (Cart::isEmpty()) {
            return $this->cartIsEmpty();
        }

        $this->setSeo();

        $userInfo = $this->checkoutStorage->only(self::FIRST_STEP_FIELDS)
            + (Auth::check() ? Auth::user()->toArray() : []);

        return view('cms-orders::site.checkout.step1', compact('userInfo'));
    }

    /**
     * @param  Step1Request  $request
     * @return JsResponse
     */
    public function saveStep1(Step1Request $request)
    {
        if (Cart::isEmpty()) {
            return JsResponse::make()->notification(__('cms-orders::site.cart.Cart is empty'))->reload();
        }

        $userInfo = $request->only(self::FIRST_STEP_FIELDS);

        // If user want register
        if (Auth::guest() && $request->get('register_me') && $this->usersLoaded) {
            if (($result = $this->tryRegister($request)) !== null) {
                return $result;
            }
        }

        $this->checkoutStorage->put($userInfo);

        return JsResponse::make()->redirect(route('checkout.step2'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|null
     */
    public function step2()
    {
        if (Cart::isEmpty()) {
            return $this->cartIsEmpty();
        }

        if (($result = $this->validateStep1()) !== null) {
            return $result;
        }

        $previousData = $this->checkoutStorage->only(self::FIRST_STEP_FIELDS);

        $this->setSeo();

        $deliveries = Delivery::published()
            ->with([
                'payments' => function ($query) {
                    $query->published()->select('id');
                }
            ])
            ->orderBy('sort')
            ->latest('id')
            ->get();
        $groupedDeliveries = Helpers::groupByParentId($deliveries, 'type');

        $payments = Payment::published()->orderBy('sort')->latest('id')->get();

        $deliveryTypes = array_intersect_key(DeliveryTypes::toSelectArray(), $groupedDeliveries);

        return view(
            'cms-orders::site.checkout.step2',
            [
                'deliveryTypes' => $deliveryTypes,
                'groupedDeliveries' => $groupedDeliveries,
                'payments' => $payments,
                'previousData' => $previousData,
                'stepData' => $this->checkoutStorage->getAll(),
            ]
        );
    }

    /**
     * @param  Step2Request  $request
     * @return RedirectResponse|\Illuminate\Routing\Redirector|JsResponse
     * @throws \Throwable
     */
    public function saveStep2(Step2Request $request)
    {
        if (Cart::isEmpty()) {
            return JsResponse::make()->notification(__('cms-orders::site.cart.Cart is empty'))->reload();
        }

        if (($result = $this->validateStep1()) !== null) {
            if ($result instanceof RedirectResponse) {
                return JsResponse::make()->redirect($result->getTargetUrl());
            } else {
                return $result;
            }
        }

        $deliveryData = [
            'delivery_type' => $request->get('delivery'),
        ];

        if ($request->get('dont_call_back')) {
            $deliveryData['dont_call_back'] = true;
        }

        $delivery = Delivery::published()->find($request->get('delivery_id'));
        $deliveryDriver = $delivery->makeDriver();
        if ($deliveryDriver) {
            try {
                $deliveryDriver->validateData($request);
            } catch (ValidationException $e) {
                return JsResponse::make()
                    ->reset(false)
                    ->notification(collect($e->errors())->flatten()->implode('<br>'), 'error', 20);
            }
            $deliveryData = array_merge($deliveryData, $deliveryDriver->prepareStoreData($request));
        } else {
            $deliveryData['delivery_address'] = $request->get('address');
        }

        $payment = Payment::published()->find($request->get('payment_id'));

        // Save order
        try {
            $order = \DB::transaction(function () use ($delivery, $deliveryData, $payment, $request) {
                $order = Order::create();

                // Delivery
                $order->delivery()->associate($delivery);
                $order->fill($deliveryData);

                // Payment
                $order->payment()->associate($payment);

                // User
                if (Auth::check() && $this->usersLoaded) {
                    $order->user()->associate(Auth::user());
                }

                $order->client()->create($this->checkoutStorage->only(self::FIRST_STEP_FIELDS));

                $order->recipient()->create($request->only('comment'));

                // Save order
                $order->changeStatus(OrderStatus::newStatus())->save();

                // Save cart content
                foreach (Cart::content() as $cartItem) {
                    /** @var CartItemInterface $cartItem */

                    $product = Product::published()->find($cartItem->getId());
                    if ($product) {
                        $order->items()->create([
                            'product_id' => $product->id,
                            'quantity' => $cartItem->getQuantity(),
                            'price' => $cartItem->getPrice(false),
                            'purchase_price' => round($cartItem->getTotal(false) / $cartItem->getQuantity(), 2),
                        ]);
                    }
                }

                event(new CreatedOrder($order));

                return $order;
            }, 3);
        } catch (\Exception $e) {
            logger('Order creation error', ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            return JsResponse::make()
                ->reset(false)
                ->notification(
                    __('cms-orders::site.checkout.Order creation error Please try later'),
                    'error',
                    20
                );
        }

        // Clear cart
        Cart::clear();
        $this->checkoutStorage->clear();

        // Redirect to payment link or thanks page
        $paymentDriver = $payment->makeDriver($order);
        if ($paymentDriver) {
            $paymentUrl = $paymentDriver->redirectUrl(
                route('thanks-page', $order->id),
                route('payment-callback', [$order->id, $payment->driver])
            );

            if ($paymentUrl) {
                return JsResponse::make()->redirect($paymentUrl);
            }
        }

        return JsResponse::make()->redirect(route('thanks-page', $order->id));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function cartIsEmpty()
    {
        $this->setSeo(__('cms-orders::site.cart.Cart is empty'));

        return view('cms-orders::site.checkout.cart-is-empty');
    }

    /**
     * @param  Request  $request
     * @return JsResponse|null
     */
    private function tryRegister(Request $request)
    {
        // Validate email && phone
        $validator = \Validator::make(
            $request->all(),
            ['email' => 'nullable|unique:users', 'phone' => 'unique:users'],
            [
                'email.unique' => __('cms-orders::site.checkout.User with provided email already exists'),
                'phone.unique' => __('cms-orders::site.checkout.User with provided phone already exists'),
            ],
            [
                'email' => __('cms-orders::site.checkout.Email'),
                'phone' => __('cms-orders::site.checkout.Phone'),
            ]
        );

        if ($validator->fails()) {
            $title = $request->get('email')
                ? __('cms-orders::site.checkout.User with provided email or phone already exists')
                : __('cms-orders::site.checkout.User with provided phone already exists');

            $message = NotifyMessage::warning($title, 20)
                ->set('confirmButtonText', __('cms-orders::site.checkout.Auth'))
                ->set('showConfirmButton', true);

            return JsResponse::make()
                ->reset(false)
                ->notification($message, 'warning', 20);
        }

        $passwordMinLength = config('cms.users.users.password_min_length');
        $password = mt_rand('1' . str_repeat(0, $passwordMinLength - 1), str_repeat('9', $passwordMinLength));
        event(new AutoRegistered($user = $this->createUser($request->all(), $password), $password));

        Auth::guard()->login($user);

        return null;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @param $password
     * @return User
     */
    protected function createUser(array $data, $password)
    {
        $user = User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'registered_through' => User::emailOrPhone($data['email']),
            'active' => true,
            'password' => Hash::make($password),
        ]);

        $user->markEmailAsVerified();

        return $user;
    }

    /**
     * @param  string|null  $title
     */
    private function setSeo($title = null)
    {
        $title = $title ?: __('cms-orders::site.checkout.Checkout');

        $this->seo()
            ->setTitle($title)
            ->setDescription($title)
            ->setH1($title)
            ->metatags()
            ->setKeywords($title);

        $this->addBreadcrumb($title);
    }

    /**
     * Validate contact details step
     * @return \Illuminate\Http\RedirectResponse|null
     */
    private function validateStep1()
    {
        $request = new Step1Request();

        $validator = \Validator::make(
            $this->checkoutStorage->only(self::FIRST_STEP_FIELDS),
            $request->rules(),
            $request->messages(),
            $request->attributes()
        );

        if ($validator->passes()) {
            return null;
        }

        NotifyMessage::make()
            ->type('error')
            ->asToast()
            ->set('html', collect($validator->errors()->messages())->flatten()->implode('<br>'))
            ->flash();

        return redirect()->route('checkout.step1');
    }
}
