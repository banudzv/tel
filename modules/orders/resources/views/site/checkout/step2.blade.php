@extends('cms-ui::layouts.main')

@php
    /**
     * @var $previousData array
     * @var $stepData array
     * @var $deliveryTypes array
     * @var $groupedDeliveries array|\WezomCms\Orders\Models\Delivery[][]
     * @var $payments \Illuminate\Database\Eloquent\Collection|\WezomCms\Orders\Models\Payment[]
     */
@endphp

@section('content')
    <div class="container">
        <h1>{{ SEO::getH1() }}</h1>
        <div>
            <div>@lang('cms-orders::site.Шаг первый')</div>
            <div>
                <div>@lang('cms-orders::site.Контактные данные')</div>
                <span>1. {{ array_get($previousData, 'name') . ' ' . array_get($previousData, 'surname') . ', ' . array_get($previousData, 'phone') }}</span>
                <a href="{{ route('checkout.step1') }}">@lang('cms-orders::site.Редактировать')</a>
            </div>

            <div>@lang('cms-orders::site.Шаг второй')</div>
            <div>
                <div>@lang('cms-orders::site.Доставка и оплата')</div>
                <form class="js-import" method="post" action="{{ route('checkout.save-step2') }}">
                    <div>
                        <div>@lang('cms-orders::site.Способ доставки')</div>
                        @if($deliveryTypes)
                            <ul class="js-import" data-service-order>
                                @foreach($deliveryTypes as $key => $deliveryName)
                                    <li>
                                        <label>
                                            <input type="radio" name="delivery" value="{{ $key }}"
                                                   data-service-input="{{ $key }}"
                                                   {{ array_get($stepData, 'delivery') === $key ? 'checked' : '' }}
                                                   required="required"
                                                   data-msg="@lang('cms-orders::site.Выберите один из пунктов')!">
                                            <span>{{ $deliveryName }}</span>
                                        </label>
                                        @if(!empty($groupedDeliveries[$key]))
                                            <div data-service-block="{{ $key }}" hidden>
                                                <select class="js-import"
                                                        name="delivery_id"
                                                        required="required"
                                                        disabled="disabled"
                                                        data-delivery-select>
                                                    @foreach($groupedDeliveries[$key] as $deliveryVariant)
                                                        <option value="{{ $deliveryVariant->id }}"
                                                                data-available-payments="{{ $deliveryVariant->payments->pluck('id')->toJson() }}"
                                                                data-type="{{ $deliveryVariant->type }}"
                                                                data-driver="{{ $deliveryVariant->driver }}"
                                                            {{ array_get($stepData, 'delivery_id') == $deliveryVariant->id ? 'selected' : '' }}>
                                                            {{ $deliveryVariant->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @foreach($groupedDeliveries[$key] as $deliveryVariant)
                                                    @php
                                                        /** @var \WezomCms\Orders\Contracts\DeliveryDriverInterface $driver */
                                                        $driver = $deliveryVariant->makeDriver();
                                                    @endphp
                                                    @if($driver)
                                                        <div data-delivery-selection="{{ $deliveryVariant->id }}"
                                                             data-driver="{{ $deliveryVariant->driver }}"
                                                             hidden>
                                                            {!! $driver->renderFormInputs($stepData, $key) !!}
                                                        </div>
                                                    @else
                                                        @include('cms-orders::site.delivery.address')
                                                    @endif
                                                @endforeach
                                            </div>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <div>
                        <div>@lang('cms-orders::site.Способ оплаты')</div>
                        @if($payments->isNotEmpty())
                            <ul>
                                @foreach($payments as $payment)
                                    <li>
                                        <label>
                                            <input type="radio"
                                                   {{ $payment->driver === 'cash' ? 'disabled' : 'name=payment_id' }}
                                                   value="{{ $payment->id }}"
                                                   data-driver="{{ $payment->driver }}"
                                                   required="required"
                                                   data-msg="@lang('cms-orders::site.Выберите один из пунктов')!">
                                            <span>{{ $payment->name }}</span>
                                        </label>
                                        @if($payment->driver === 'cash')
                                            <div>@lang('cms-orders::site.Данный способ не доступен, если в заказе присутствуют отрезные товары!')</div>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                        <div>
                            {!! nl2br(e(settings('payments.payment.payment'))) !!}
                        </div>
                    </div>
                    <div>
                        <div>
                            <textarea name="comment" inputmode="text"
                                      spellcheck="true"
                                      placeholder="@lang('cms-orders::site.Добавить пожелание или коментарий к заказу')"
                            >{{ array_get($stepData, 'recipient.comment') }}</textarea>
                        </div>
                        <div>
                            <label>
                                <input type="checkbox" name="dont_call_back" value="1">
                                <span>@lang('cms-orders::site.Не надо перезванивать')</span>
                            </label>
                        </div>
                    </div>
                    <div>
                        * @lang('cms-orders::site.Стоимость доставки будет рассчитываться в зависимости от объема посылки и точки доставки')
                    </div>
                    <div>
                        <button type="submit">@lang('cms-orders::site.Заказ подтверждаю')</button>
                        @if(\WezomCms\Core\Foundation\Helpers::providerLoaded('WezomCms\PrivacyPolicy\PrivacyPolicyServiceProvider'))
                            <div>
                                @lang('cms-orders::site.Подтверждая заказ, Вы принимаете'),
                                <br/>
                                <a href="{{ route('privacy-policy') }}" target="_blank">@lang('cms-privacy-policy::site.условия пользователя')</a>
                            </div>
                        @endif
                    </div>
                </form>
            </div>
        </div>
        <div data-cart-display="compact" data-cart-order="true">@lang('cms-orders::site.Загрузка')...</div>
    </div>
@endsection
