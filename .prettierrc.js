/**
 * @fileOverview Конфигурация Prettier
 * Форматирует JS, JSON, CSS и Sass файлы
 * @see https://prettier.io
 */

module.exports = {
	arrowParens: 'always',
	bracketSpacing: true,
	jsxBracketSameLine: false,
	printWidth: 90,
	semi: true,
	singleQuote: true,
	trailingComma: 'none',
	tabWidth: 4,
	useTabs: true
};
